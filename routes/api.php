<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/





Route::post('/login', 'API\UserController@login');
Route::post('/register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('/details', 'API\UserController@details');
    Route::post('/details-email', 'API\UserController@detailsEmail');
    Route::get('/test', function(){
        echo "test 123";
    });

    Route::get('/usermanagement-unitkerja-eselon/{eselon}','UsermanagementController@apiGetUnitKerjaByEselon');

    Route::get('/usermanagement-unitkerja','UsermanagementController@apiGetUnitKerja');
    Route::get('/usermanagement-unitkerja/{kode_unit_kerja}','UsermanagementController@apiGetUnitKerja');
    Route::post('/usermanagement-unitkerja','UsermanagementController@apiPostUnitKerja');
    Route::put('/usermanagement-unitkerja','UsermanagementController@apiPutUnitKerja');


});

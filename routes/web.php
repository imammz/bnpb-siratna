<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */



Auth::routes();

Route::get('/cekpdf','HomeController@testpdf');
Route::get('/cekpdfwatermark','HomeController@testpdf2');
Route::get('/suratmasuk-detailCode/{id}', 'SuratMasukController@detailQRCode');


Route::get('/cek-sess', 'ConfigController@cekSession');




Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/to/{url?}/{p1?}/{p2?}/{p3?}', 'HomeController@to');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/home-page', 'HomeController@home')->name('homePage');

    Route::get('/kpa-suratmasuk', 'KpaController@suratmasuk');
    Route::get('/kpa-suratmasuk-tambah', 'KpaController@suratmasukTambah');

    Route::get('/kpa-disposisi', 'KpaController@disposisi');
    Route::get('/kpa-disposisi-tambah', 'KpaController@disposisiTambah');
    Route::get('/kpa-disposisi-detil', 'KpaController@disposisiDetil');

    Route::get('/suratmasuk-tambah', 'SuratMasukController@tambah');
    Route::get('/suratmasuk-daftar/{flag?}', 'SuratMasukController@daftar');
    Route::get('/suratmasuk-daftar-proses/{id}', 'SuratMasukController@suratProses');
    Route::get('/suratmasuk-disposisi/{id}', 'SuratMasukController@disposisi');
    Route::post('/suratmasuk-disposisi-proses', 'SuratMasukController@disposisiProses');
    Route::get('/suratmasuk-daftar-data/{flag?}', 'SuratMasukController@daftarData');
    Route::get('/suratmasuk-daftar-data-sudah/{flag?}', 'SuratMasukController@daftarDataSudah');

    Route::get('/suratmasuk-disposisi/{id}', 'SuratMasukController@disposisi');
    Route::get('/suratmasuk-diteruskan/{id}', 'SuratMasukController@diteruskan');
    Route::post('/suratmasuk-diteruskan', 'SuratMasukController@diteruskanProses');
    Route::get('/suratmasuk-catatan/{id}', 'SuratMasukController@catatan');
    Route::post('/suratmasuk-catatan', 'SuratMasukController@catatanProses');
    Route::get('/suratmasuk-catatan-selesai/{id}', 'SuratMasukController@catatanSelesai');
    Route::post('/suratmasuk-catatan-selesai/', 'SuratMasukController@catatanSelesaiProses');

    Route::get('/suratmasuk-marked-proses/{id}', 'SuratMasukController@markedProses');
    Route::get('/suratmasuk-marked-batal/{id}', 'SuratMasukController@markedBatal');
    Route::post('/suratmasuk-tambah-proses', 'SuratMasukController@tambahProses');

    Route::get('/suratmasuk-code/{id}', 'SuratMasukController@QRCode');

    Route::get('/suratmasuk-lembar-disposisi/{id}', 'SuratMasukController@pdfLembarDisposisi');
    Route::get('/suratmasuk-lembar-disposisi-manual/{id}', 'SuratMasukController@pdfLembarDisposisiManual');
    Route::get('/suratmasuk-lampiran-images/{id}', 'SuratMasukController@pdfFileLampiranImages');

    Route::get('/suratkeluar-tambah', 'SuratKeluarController@tambah');
    Route::post('/suratkeluar-tambah-jenis', 'SuratKeluarController@jenis');
    Route::get('/suratkeluar-tambah-jenis/{id}/{flag1?}/{flag2?}', 'SuratKeluarController@jenis');
 
    Route::post('/suratkeluar-tambah-proses', 'SuratKeluarController@tambahProses');
    Route::post('/suratkeluar-tambah-proses-002', 'SuratKeluarController@tambahProses002');

    Route::get('/suratkeluar-tambah-detil', 'SuratKeluarController@tambahDetil');
    Route::post('/suratkeluar', 'SuratKeluarController@store');
    Route::get('/suratkeluar-daftar', 'SuratKeluarController@daftar');
    Route::get('/suratkeluar-daftar-data', 'SuratKeluarController@daftarData');

    Route::get('/suratkeluar-templates-detil/{code}/{id}', 'SuratKeluarController@templatesDetil');
    Route::get('/suratkeluar-templates-pdf/{code}/{id}', 'SuratKeluarController@templatesPdf');


    Route::get('/agenda', 'AgendaController@index');
    Route::get('/rekapitulasi', 'RekapitulasiController@index');
    Route::get('/usermanagement', 'UsermanagementController@index');
    Route::get('/usermanagement-datapegawai', 'UsermanagementController@datapegawai');
    Route::get('/usermanagement-roleakses', 'UsermanagementController@roleakses');
    Route::get('/usermanagement-unitkerja', 'UsermanagementController@unitkerja');


    Route::get('/usermanagement/tambahuser/{kode}', 'UsermanagementController@tambahuser');
    Route::get('/usermanagement/edituser/{id}/{kode}/{ids?}', 'UsermanagementController@edituser');
    Route::post('/usermanagement-tambahuser/', 'UsermanagementController@tambahuserProses');
    Route::post('/usermanagement-edituser/', 'UsermanagementController@edituserProses');
  
    Route::get('/usermanagement/hapus-user/{id}/{id_unit_kerja}', 'UsermanagementController@hapusUserProses');
   
    Route::get('/usermanagement/hapus-unitkerja/{id_unit_kerja}', 'UsermanagementController@hapusUnitKerja');

    Route::get('/masterdata', 'MasterdataController@index');

});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    return "Cache is cleared";
});





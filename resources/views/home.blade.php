<div id="page">
<div class="row">
    <div class="col-md-8 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Daftar Agenda</h4>


                <div class="fc-external-events">
                    <div class="row">
                        <div class="col-md-7">

                            @foreach ($TopAgendaUser as $row)

                                <div class="fc-event">
                                    <p>{{ $row->nama_kegiatan }}</p>
                                    <p class="small-text">{{ \Carbon\Carbon::parse($row->tanggal_mulai)->format('d-M-Y') }}</p>
                                    <p class="text-muted mb-0">{{ $row->keterangan }}</p>
                                </div>

                            @endforeach
                        </div>
                        <div class="col-md-5">

                            <div class="row">
                                <div class="col-sm-12 grid-margin alert alert-fill-danger" role="alert">
                                    <a href="{{ url('to/suratmasuk-daftar') }}"  class="text-white">
                                        <i class="fa fa-file-archive-o"></i> <b>{{ $jml_belum_dibaca }}</b> Surat Masuk Belum Dibaca
                                    </a>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-sm-12 grid-margin">
                                    <div>
                                        <div>

                                            <div class="alert alert-fill-primary" role="alert">
                                                <i class="fa fa-bookmark"></i> <b>{{ $suratmasukMarked }}</b> Surat Masuk Ditandai
                                            </div>
                                            <div class="alert alert-fill-success" role="alert">
                                                <i class="fa fa-bookmark-o"></i> <b> {{ $suratkeluarMarked }}</b> Surat Keluar Ditandai
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div id="calendar" class="full-calendar"></div>
            </div>
        </div>
    </div>
    <div class="col-md-4">


        <div class="row">
            <div class="col-lg-12">



            </div>

        </div>
         <div class="row">
            <div class="col-sm-12 grid-margin">

                <div class="card bg-primary" style="text-align: center; padding-top: 15px;">
                    <h4 class="card-title text-light">SURAT MASUK TERBARU</h4>
                </div>
                <div class="card bg-linkedin">
                    <div class="card-body">

                        @foreach($suratmasuk_terbaru as $row)

                            <div class="d-flex border-bottom pb-3">
                                <div class="flex-grow-1 ml-3 text-light">
                                    <p>Dari : <span class="badge badge-primary">{{ $row->pengirim }}</span></p>
                                    <div class="d-flex ">
                                        <p class="mb-0"><a class="badge badge-success" href={{ url( 'to/suratmasuk-daftar-proses/'.$row->id) }}>No: {{ $row->pengirim_nomor_surat }}</a></p>
                                    </div>
                                    <div class="d-flex ">
                                                <span class="badge badge-info">
                                                    Tgl :
                                                        {{ \Carbon\Carbon::parse($row->tanggal_surat_masuk)->format('d-m-Y') }}
                                                   </span>
                                    </div>
                                </div>
                            </div>

                        @endforeach




                    </div>
                </div>

            </div>

        </div>


    </div>

</div>
</div>



<script>
    (function($) {
        'use strict';
        $(function() {
            if ($('#calendar').length) {
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,basicWeek,basicDay'
                    },
                    defaultDate: '{{date('Y-m-d')}}',
                    navLinks: true, // can click day/week names to navigate views
                    editable: false,
                    eventLimit: true, // allow "more" link when too many events
                    events: [

                            @if(count($agendaUser)>0)

                            @php $no=1;

                            @endphp
                            @foreach ($agendaUser as $row)
                        {
                            id: {{ $no }},
                            title: '{{ $row->agenda->nama_kegiatan }}',
                            @if(false)
                            url: '{{ url('agenda-detail/'.$row->agenda->id) }}',
                            @endif
                            start: '{{ \Carbon\Carbon::parse($row->agenda->tanggal_mulai)->format('Y-m-d') }}',
                            @if(isset($row->agenda->tanggal_akhir))
                            end: '{{ \Carbon\Carbon::parse($row->agenda->tanggal_akhir)->format('Y-m-d') }}'
                            @endif
                        },
                        @php $no++;
                        @endphp
                        @endforeach
                        @endif
                    ]
                })
            }
        });
    })(jQuery);
</script>



<script>


    $(function(){

        @if($jml_belum_dibaca>0)
        @foreach($belum_dibaca as $key=>$row)
        @php $no = $key+1; $time = 800*$no; @endphp

        setTimeout(function(){
            suratMasukToast_{{$no}}()
        },{{$time}});


        @endforeach
        @endif

    });

    (function($) {

        @if($jml_belum_dibaca>0)
                @foreach($belum_dibaca as $key=>$row)
                @php $no = $key+1; @endphp

            suratMasukToast_{{$no}} = function() {
            'use strict';
            resetToastPosition();
            $.toast({
                heading: 'Notifikasi Surat Masuk',
                text: '  <p><b><i>{{$row->perihal}}</i></b>  <br/><br/><b>Dari :  {{$row->pengirim}}</b></p>   ',
                showHideTransition: 'slide',
                icon: 'info',
                loaderBg: '#c3465f',
                position: 'top-right'
            })
        };

        @endforeach
                @endif




            resetToastPosition = function() {
            $('.jq-toast-wrap').removeClass('bottom-left bottom-right top-left top-right mid-center'); // to remove previous position class
            $(".jq-toast-wrap").css({
                "top": "",
                "left": "",
                "bottom": "",
                "right": ""
            }); //to remove previous position style
        }
    })(jQuery);



</script>

@include('layouts.js-page')

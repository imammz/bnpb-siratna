@extends('layouts.app')
    @section('content')
        <style>
            .select2-container{ width: 100% !important; }
        </style>



            @if($messages=Session::get('pesan'))
                <div class="alert alert-success alert-block pesan" style=" margin-top:  100px;">
                    <strong>{{ $messages }}</strong>
                </div>
                <div class="pesan-box" style=" margin-top:  100px; display: none"> </div>
                <div id="content" class="content-wrapper" style="padding-bottom: 5px; padding-top:  0px;">

                </div>
            @endif


            @if ($errors->any())
                <div class="alert alert-danger pesan" style=" margin-top:  100px;">
                    <strong>Proses Gagal</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="pesan-box" style=" margin-top:  100px; display: none"> </div>
                <div id="content" class="content-wrapper" style="padding-bottom: 5px; padding-top:  0px;">

                </div>
            @endif


            <div id="loader">
                <div  class="loader-demo-box" style="min-height: 800px; padding-top:  80px;">
                    <div class="dot-opacity-loader">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>

        @if (!$errors->any() and !$messages=Session::get('pesan'))
            <div class="pesan"> </div>
            <div class="pesan-box" style="display: none"> </div>
        <div id="content" class="content-wrapper" style="padding-bottom: 5px; padding-top:  80px;">

        </div>
        @endif






    @endsection

            @section('part-css')
                <style>
                    #page {
                        display:none;
                    }
                </style>
            @endsection

            @section('part-js')


                <script>
                    $(document).ready(function(){

                        setTimeout(function(){
                            $(".pesan").fadeOut(5000);
                        },3000);

                        setTimeout(function(){
                            $(".pesan-box").show();
                        },8000);




                    });

                    function goto(url) {

                        $.get("{{url('cek-sess/')}}", function(data, status){

                            if(data.login) {
                                $("#page").fadeOut(20);
                                $("#loader").fadeIn(20);
                                $("#content").load("{{url('')}}/"+url);
                            }
                            else {
                                showSessionLogin();

                                setTimeout(function(){
                                    $(location).attr('href','{{url("login")}}');
                                },2000);
                            }


                        });


                    }

                    showSessionLogin = function() {
                        'use strict';
                        resetToastPosition();
                        $.toast({
                            heading: 'SESI  LOGIN  HABIS',
                            text: ' Mohon Login Kembali <br/',
                            showHideTransition: 'slide',
                            icon: 'info',
                            loaderBg: '#ff333f',
                            position: 'top-center'
                        })
                    };


                    resetToastPosition = function() {
                        $('.jq-toast-wrap').removeClass('bottom-left bottom-right top-left top-right mid-center'); // to remove previous position class
                        $(".jq-toast-wrap").css({
                            "top": "",
                            "left": "",
                            "bottom": "",
                            "right": ""
                        }); //to remove previous position style
                    }

                </script>

@endsection

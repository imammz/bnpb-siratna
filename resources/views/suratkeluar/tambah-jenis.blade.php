
    <div class="row" style="margin-top:40px;">
        <div class="col-lg-12 grid-margin d-flex flex-column">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb bg-primary">
                        <li class="breadcrumb-item"><a href="{!! url('to/suratkeluar-daftar') !!}">Naskah Dinas Keluar</a></li>
                        <li class="breadcrumb-item"><b>Jenis </b></li>
                        <li class="breadcrumb-item active" aria-current="page"><b>{{$nama_jenis}}</b></li>
                    </ol>
                </div>

            </div>



            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5> Data Surat </h5>

                            @if($messages=Session::get('pesan'))
                                <div class="alert alert-success alert-block">
                                    <strong>{{ $messages }}</strong>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            {!! Form::open(['url' => 'suratkeluar-tambah-proses','class'=>'form-sample','method' => 'post','files' => true]); !!} {!! Form::token(); !!}
                            {!! Form::hidden('gen', $gen) !!}



                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group row">
                                        {!! Form::label('kepada', 'Kepada',['class'=>'col-sm-2 col-form-label']); !!}
                                        <div class="col-sm-10">
                                            {!! Form::select('kepada', $unitkerja, '', ['class'=>'select2 col-sm-10','placeholder'=>'Pilih Tujuan Surat']) !!}

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">



                                    {!! Form::hidden('pengirim',Auth::user()->pegawai->unit_kerja->nama_unit_kerja); !!}

                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            {!! Form::label('nosurat', 'Nomor Surat',['class'=>'col-sm-12 col-form-label']); !!}

                                        </div>
                                        <div class="col-sm-9">
                                            {!! Form::text('nosurat', $no_agenda,['class'=>'form-control','placeholder'=>'Nomor Surat','required'=>'required']); !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {!! Form::label('tgl_surat', 'Tanggal Surat',['class'=>'col-sm-3 col-form-label']); !!}
                                        <div class="col-sm-9">
                                            {!! Form::date('tgl_surat', date('Y-m-d'),['class'=>'form-control','placeholder'=>'Tanggal Surat','required'=>'required']); !!}
                                            <span class="badge badge-secondary"><i>(Bulan/Tanggal/Tahun)</i></span>
                                        </div>
                                    </div>

                                    {!! Form::hidden('tgl_diterima',date('Y-m-d')); !!}


                                    <div class="form-group row">
                                        {!! Form::label('perihal', 'Perihal',['class'=>'col-sm-3 col-form-label']); !!}
                                        <div class="col-sm-9">
                                            {!! Form::textarea('perihal', null,['class'=>'form-control']); !!}
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        {!! Form::label('klasifikasi', 'Sifat',['class'=>'col-sm-3 col-form-label']); !!}
                                        <div class="col-sm-9">
                                            {!! Form::select('klasifikasi', $klasifikasi, '', ['class'=>'select2 col-sm-10','placeholder'=>'pilih jenis sifat dokumen']
                                            ) !!}
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        {!! Form::label('retensi', 'Retensi',['class'=>'col-sm-3 col-form-label']); !!}
                                        <div class="col-sm-9">
                                            {!! Form::select('retensi', $jra, '', ['class'=>'select2 col-sm-10','placeholder'=>'Pilih Retensi'] ) !!}
                                        </div>

                                    </div>


                                </div>

                            </div>
                            <hr/>
                            <h5> Badan Surat </h5>
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            {!! Form::label('yth', 'Yth',['class'=>'col-sm-12 col-form-label']); !!}

                                        </div>
                                        <div class="col-sm-9">
                                            {!! Form::text('yth', '',['class'=>'form-control','placeholder'=>'Yang Terhormat ....','required'=>'required']); !!}
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            {!! Form::label('dari', 'Dari',['class'=>'col-sm-12 col-form-label']); !!}

                                        </div>
                                        <div class="col-sm-9">
                                            {!! Form::text('dari', '',['class'=>'form-control','placeholder'=>'Dari ....','required'=>'required']); !!}
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <h5>PENANDATANGAN</h5>
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            {!! Form::label('penandatangan_nama', 'Nama',['class'=>'col-sm-12 col-form-label']); !!}

                                        </div>
                                        <div class="col-sm-9">
                                            {!! Form::text('penandatangan_nama', $ttd_nama,['class'=>'form-control','required'=>'required']); !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            {!! Form::label('penandatangan_nip', 'NIP',['class'=>'col-sm-12 col-form-label']); !!}

                                        </div>
                                        <div class="col-sm-9">
                                            {!! Form::text('penandatangan_nip', '',['class'=>'form-control','placeholder'=>'NIP ....','required'=>'required']); !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            {!! Form::label('penandatangan_jabatan', 'Jabatan',['class'=>'col-sm-12 col-form-label']); !!}

                                        </div>
                                        <div class="col-sm-9">
                                            {!! Form::text('penandatangan_jabatan', $ttd_jabatan,['class'=>'form-control','required'=>'required']); !!}
                                        </div>
                                    </div>
                                </div>



                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">

                                        <h5>ISI SURAT</h5>



                                        <div class="col-sm-12">


                                        <textarea  name="content_surat"  class="description" style="width: 100%; height: 500px;">

                                            {{$body}}

                                        </textarea>


                                        <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
                                        <script>
                                            tinymce.init({
                                                selector:'textarea.description',
                                                width: 850,
                                                height: 400
                                            });
                                        </script>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">

                                        <div class="col-sm-12">
                                            <span class="badge badge-warning">Tembusan Kepada</span> <br/> <br/>
                                            {!! Form::select('tembusan_kepada[]', $unitkerja_teruskan, '', ['class'=>'select2 col-sm-10','multiple'=>'multiple']) !!}


                                        </div>


                                    </div>

                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12" style="text-align: right">

                                    <a class="btn btn-sm btn-dark" href="{{ url('to/suratmasuk-daftar') }}">
                                        <i class="fa fa-close"></i> Batal
                                    </a>
                                    <button class="btn btn-sm btn-warning" type="reset">
                                        <i class="fa fa-refresh"></i> Reset
                                    </button>
                                    <button class="btn btn-sm btn-facebook" type="submit">
                                        <i class="fa fa-check-circle"></i> Simpan
                                    </button>


                                </div>
                            </div>
                            {!! Form::close(); !!}


                        </div>
                    </div>
                </div>



            </div>
        </div>






            <script>


                $(function(){

                    $('.select2').select2();

                    $(".btn-success").click(function(){
                        var html = $(".clone").html();
                        $(".increment").after(html);
                    });

                    $("body").on("click",".btn-danger",function(){
                        $(this).parents(".control-group").remove();
                    });


                    $(".btn-diteruskan-add").click(function(){
                        var html = $(".clone2").html();
                        $(".increment2").after(html);
                    });

                    $("body").on("click",".btn-diteruskan-del",function(){
                        $(this).parents(".control-group").remove();
                    });



                });

                (function($) {

                    showInfoToast = function() {
                        'use strict';
                        resetToastPosition();
                        $.toast({
                            heading: 'peringatan',
                            text: '  1. Agar dijaga kerahasiaan surat ini; <br/>',
                            showHideTransition: 'slide',
                            icon: 'info',
                            loaderBg: '#46c35f',
                            position: 'top-right'
                        })
                    };

                    showInfoToast2 = function() {
                        'use strict';
                        resetToastPosition();
                        $.toast({
                            heading: 'peringatan',
                            text: '  2. Dilarang memisahkan sehelaipun surat atau dokumen dari berkasi ini. <br/>',
                            showHideTransition: 'slide',
                            icon: 'info',
                            loaderBg: '#46c35f',
                            position: 'top-right'
                        })
                    };



                    resetToastPosition = function() {
                        $('.jq-toast-wrap').removeClass('bottom-left bottom-right top-left top-right mid-center'); // to remove previous position class
                        $(".jq-toast-wrap").css({
                            "top": "",
                            "left": "",
                            "bottom": "",
                            "right": ""
                        }); //to remove previous position style
                    }
                })(jQuery);
            </script>



    @include('layouts.js-page')

<div id="page">
    <style>
        a {
            color: #0c0c0c;
        }
        a:hover {
            font-weight: bolder;
            text-decoration: none;
        }
    </style>

    <div class="modal fade" id="modal-1" tabindex="-1" role="dialog"
         aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div style="width: 100%; padding-bottom: 10px; padding-left: 10px; padding-right: 10px;">
                    <hr/>
                    <div class="row" >




                        <div class="col-md-6" style="text-align: left;">

                        </div>



                        <div class="col-md-6" style="text-align: right;" >

                            <a href="#" id="detil" style="display: none"
                               class="btn btn-success">
                                <i class="fa fa-list"></i> DETIL
                            </a>
                            <button type="button" class="btn btn-light" data-dismiss="modal">
                                TUTUP
                            </button>
                        </div>


                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="row" style="margin-top:40px;">
        <div class="col-lg-12 grid-margin d-flex flex-column">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb bg-primary">
                        <li class="breadcrumb-item active"><a href="{{ url('to/suratkeluar-daftar') }}"><b>Naskah Dinas Keluar</b></a></li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title"> Surat Keluar</h4>

                            <div class="row">
                                <div class="col-12">
                                        @if($messages=Session::get('pesan'))
                                        <div class="alert alert-success alert-block">
                                            <strong>{{ $messages }}</strong>
                                        </div>
                                    @endif
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                        <br/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <ul class="nav nav-tabs" role="tablist">

                                        <li class="nav-item">
                                            <a class="nav-link active show" id="home-tab" data-toggle="tab" href="#proses" role="tab" aria-controls="proses" aria-selected="true">
                                                <i class="mdi mdi-progress-check text-danger ml-2"></i>
                                               Daftar Data
                                            </a>
                                        </li>


                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane fade active show" id="proses" role="tabpanel" aria-labelledby="home-tab-vertical">


                                            <table class="table table-hover table-bordered" id="daftarsuratkeluar">
                                                <thead>
                                                <tr style="text-align:center">
                                                    <th width="6%">No</th>
                                                    <th width="20%">Jenis Surat</th>
                                                    <th width="20%">Nomor Surat</th>
                                                    <th>Perihal</th>
                                                    <th width="15%">Dibuat Oleh</th>
                                                </tr>
                                                </thead>
                                            </table>


                                            <div>
                                              
                                             

                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>




                        </div>
                    </div>
                </div>



            </div>
        </div>



</div>

            <script>
                $(function() {
                    $('#daftarsuratkeluar').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: '{!! url('suratkeluar-daftar-data/') !!}',
                        columns: [
                            {data:"DT_RowIndex",orderable:false,searchable:false},
                            { data: 'surat_keluar_template_nama', name: 'surat_keluar_template_nama' },
                            { data: 'nomor_surat_keluar', name: 'nomor_surat_keluar' },
                            { data: 'nama', name: 'nama' },
                            { data: 'created_by_nama', name: 'created_by_nama' },
                        ],
                        columnDefs: [
                            {
                                targets: 0,
                                className: 'dt-body-right'
                            },
                            {
                                targets: 1,
                                className: 'dt-body-left'
                            },
                            {
                                targets: 4,
                                className: 'dt-body-center'
                            },
                        ],
                    });



                });

            </script>


            <script>
                (function ($) {
                    'use strict';
                    $('#modal-1').on('show.bs.modal', function (event) {
                        var button = $(event.relatedTarget) // Button that triggered the modal
                        var recipient = button.data('whatever')
                        var jenis = button.data('jenis')

                        // Extract info from data-* attributes
                        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                        var modal = $(this)
                        modal.find('.modal-body').html('<iframe src="{{ url('suratkeluar-templates-pdf') }}/'+jenis+'/'+ recipient + '" width="100%" height="600px"  frameBorder="0" scrolling="auto">Browser not compatible. </iframe >')
                        modal.find('#detil').attr("href",'{{ url('suratkeluar-templates-detil') }}/'+jenis+'/'+ recipient + '')
                    })
                })(jQuery);
            </script>

@include('layouts.js-page')

<div id="page">
    <div class="row" style="margin-top:40px;">
        <div class="col-lg-12 grid-margin d-flex flex-column">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb bg-primary">
                        <li class="breadcrumb-item"><a href="{{ url('to/suratkeluar-daftar') }}">Naskah Dinas Keluar</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><b>Jenis Naskah Dinas Keluar</b></li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="panel panel-info panel-square">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                    </h3>
                                </div>

                                <div class="panel-body">
                                    @if($messages=Session::get('pesan'))
                                        <div class="alert alert-success alert-block">
                                            <strong>{{ $messages }}</strong>
                                        </div>
                                    @endif
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    {!! Form::label('ttd', 'Penandatangan',['class'=>'col-sm-3 col-form-label']); !!}
                                                    <div class="col-sm-9">
                                                        {!! Form::select('ttd', $unitkerja, '', ['class'=>'select2 col-sm-10 form-control','selected'=>'selected','width'=>'100%','id'=>'ttd']) !!}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group row">
                                                    {!! Form::label('kode_klas', 'Kode Klasifikasi',['class'=>'col-sm-3 col-form-label']); !!}
                                                    <div class="col-sm-9">
                                                        {!! Form::select('kode_klas', $jra, '', ['class'=>'select2 col-sm-10','id'=>'kode_klas'] ) !!}
                                                    </div>

                                                </div>

                                            </div>



                                            </div>
                                        </div>

                                    <div class="row">
                                        <div class="col-md-12" style="text-align: right">




                                            <div class="row portfolio-grid">
                                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                                                    <h5 class="text-facebook"> PILIH JENIS SURAT KELUAR</h5>

                                                </div>

                                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                                                    <figure class="effect-text-in">
                                                        <a href="#" onclick="goto('suratkeluar-tambah-jenis/001/'+document.getElementById('ttd').value+'/'+document.getElementById('kode_klas').value)" >
                                                        <img src="{{asset('ui/images/docs/1.png')}}" width="300" height="300" alt="image"/>
                                                        <figcaption>
                                                            <h4>NOTA DINAS</h4>
                                                            <p>Klik Untuk Membuat Nota Dinas</p>
                                                        </figcaption>
                                                        </a>
                                                    </figure>
                                                </div>

                                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                                                    <figure class="effect-text-in">
                                                        <a href="#" onclick="goto('suratkeluar-tambah-jenis/002/'+document.getElementById('ttd').value+'/'+document.getElementById('kode_klas').value)">
                                                        <img src="{{asset('ui/images/docs/2.png')}}" width="300" height="300" alt="image"/>
                                                        <figcaption>
                                                            <h4 class="text-primary">UNDANGAN </h4>
                                                            <p>Klik Untuk Membuat Undangan </p>
                                                        </figcaption>
                                                        </a>
                                                    </figure>
                                                </div>

                                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                                                    <figure class="effect-text-in">
                                                        <a href="#" onclick="goto('suratkeluar-tambah-jenis/003/'+document.getElementById('ttd').value+'/'+document.getElementById('kode_klas').value)">
                                                        <img src="{{asset('ui/images/docs/3.png')}}" width="300" height="300" alt="image"/>
                                                        <figcaption>
                                                            <h4>SURAT TUGAS</h4>
                                                            <p>Klik Untuk Membuat Surat Tugas</p>
                                                        </figcaption>
                                                        </a>
                                                    </figure>
                                                </div>


                                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                                                    <figure class="effect-text-in">
                                                        <a href="#" onclick="goto('suratkeluar-tambah-jenis/002/'+document.getElementById('ttd').value+'/'+document.getElementById('kode_klas').value)">
                                                        <img src="{{asset('ui/images/docs/2.png')}}" width="300" height="300" alt="image"/>
                                                        <figcaption>
                                                            <h4 class="text-primary">SURAT KELUAR </h4>
                                                            <p>Klik Untuk Membuat Undangan </p>
                                                        </figcaption>
                                                        </a>
                                                    </figure>
                                                </div>

                                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                                                    <figure class="effect-text-in">
                                                        <a href="#" onclick="goto('suratkeluar-tambah-jenis/003/'+document.getElementById('ttd').value+'/'+document.getElementById('kode_klas').value)">
                                                        <img src="{{asset('ui/images/docs/3.png')}}" width="300" height="300" alt="image"/>
                                                        <figcaption>
                                                            <h4>MEMORANDUM</h4>
                                                            <p>Klik Untuk Membuat Surat Tugas</p>
                                                        </figcaption>
                                                        </a>
                                                    </figure>
                                                </div>

                                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                                                    <figure class="effect-text-in">
                                                        <a href="#" onclick="goto('suratkeluar-tambah-jenis/002/'+document.getElementById('ttd').value+'/'+document.getElementById('kode_klas').value)">
                                                        <img src="{{asset('ui/images/docs/2.png')}}" width="300" height="300" alt="image"/>
                                                        <figcaption>
                                                            <h4 class="text-primary">PENGUMUMAN </h4>
                                                            <p>Klik Untuk Membuat Undangan </p>
                                                        </figcaption>
                                                        </a>
                                                    </figure>
                                                </div>

                                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                                                    <figure class="effect-text-in">
                                                        <a href="#" onclick="goto('suratkeluar-tambah-jenis/003/'+document.getElementById('ttd').value+'/'+document.getElementById('kode_klas').value)">
                                                        <img src="{{asset('ui/images/docs/3.png')}}" width="300" height="300" alt="image"/>
                                                        <figcaption>
                                                            <h4>LAPORAN</h4>
                                                            <p>Klik Untuk Membuat Surat Tugas</p>
                                                        </figcaption>
                                                        </a>
                                                    </figure>
                                                </div>



                                            </div>

                                        </div>
                                    </div>


                                </div><!-- /.panel-body -->
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

            <script>

                $(function() {

                    $('.select2').select2();

                });


            </script>


@include('layouts.js-page')

<style>
    body {
        font-family: Arial;

    }
</style>

<title>{{$surat->nama}}</title>
<body>
<table width="100%">
    <tr>
         <td style="text-align: center;">
                <h2>BADAN NASIONAL PENANGGULANGAN BENCANA</h2>
         </td>
    </tr>

</table>


<span style="text-align: center">
    <h3>NOTA DINAS
     <h5>Nomor : {{$surat->nomor_surat_keluar}}</h5>
    </h3>


</span>
<table width="100%">
    <tr> <td width="20%">Yth </td> <td width="3%"> : </td> <td> {{$surat->notaDinas->yth}} </td> </tr>
    <tr> <td width="20%">Dari </td> <td width="3%"> : </td> <td> {{$surat->notaDinas->dari}} </td> </tr>
    <tr> <td width="20%">Hal </td> <td width="3%"> : </td> <td> {{$surat->notaDinas->prihal}} </td> </tr>
    <tr> <td width="20%">Tanggal </td> <td width="3%"> : </td> <td>  {{   Carbon\Carbon::parse($surat->notaDinas->tanggal_surat)->format('d-F-Y') }} </td> </tr>
</table>
<p>
    {!! $surat->notaDinas->isi  !!}
</p>
<table width="100%">
<tr>
    <td width="70%"></td>
    <td style="text-align: center">

        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        {{$ttd->nama}}
        <br/>
    </td>
</tr>
    <tr>
        <td width="70%">
            <b>TEMBUSAN : </b>
            @php
                $no = 1;
            @endphp
                @foreach($tembusan as $row)
                <br/>       
                <span> {{ $no }}. {{$row}} </span>

                        @php
                        $no++;    
                        @endphp
                @endforeach
           
        </td>
         <td> </td>

    </tr>
</table>
</body>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('ui/vendors/iconfonts/mdi/font/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('ui/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('ui/vendors/css/vendor.bundle.addons.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('ui/css/vertical-layout-light/style.css')}}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{asset('ui/images/favicon.png')}}" />
</head>

<body class="sidebar-fixed">
<div class="container-scroller">

    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
            <form method="POST" action="{{ url('usermanagement-edituser') }}">
                @csrf

            <div class="row flex-grow">
                <div class="col-lg-12 d-flex">

                    <div class="col-md-12" style="text-align: center">
                        <h2  class="font-weight-light">EDIT DATA PENGGUNA  </h2>
                        <h3  class="font-weight-light text-primary">UNIT KERJA SAAT INI: {{strtoupper($unit->nama_unit_kerja)}} </h3>
                        <hr/>

                        @if($message=Session::get('pesan'))
                            <div class="alert alert-success alert-block"> 
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <br/>
                    </div>

                </div>

                <div class="col-lg-12 d-flex">

                    <div class="auth-form-transparent text-left p-3">



                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>

                            <div class="col-md-8">
                                <input id="nama" type="text" class="form-control-lg  form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama" value="{{$pegawai->user->nama}}">


                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                            <div class="col-md-8">
                                <input id="email" type="text" class="form-control-lg  form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$pegawai->user->email}}">


                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nip" class="col-md-4 col-form-label text-md-right">{{ __('NIP') }}</label>

                            <div class="col-md-8">
                                <input id="nip" type="text" class="form-control-lg  form-control{{ $errors->has('nip') ? ' is-invalid' : '' }}" name="nip" value="{{$pegawai->nip}}">


                            </div>
                        </div>

                        <div class="form-group row">
                            {!! Form::label('id_unit_kerja', 'Pindah Unit Kerja',['class'=>'col-sm-4 col-form-label text-md-right']); !!}
                            <div class="col-sm-8">
                                {!! Form::select('id_unit_kerja', $select_unit, $pegawai->id_unit_kerja, ['class'=>'select2 col-sm-10 form-control','width'=>'100%','placeholder'=>'Pilih Unit Kerja Baru']) !!}
                            </div>
                        </div>


                        <input type="hidden" name="id_user" value="{{$pegawai->id_user}}"/>
                        <input type="hidden" name="id_pegawai" value="{{$pegawai->id}}"/>

                    <div class="form-group row">
                        <label for="tanggal_lahir" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Lahir') }}</label>

                        <div class="col-md-8">
                            <input id="tanggal_lahir" type="date" class="form-control-lg  form-control{{ $errors->has('tanggal_lahir') ? ' is-invalid' : '' }}" name="tanggal_lahir" value="{{ \Carbon\Carbon::parse($pegawai->tanggal_lahir)->format('Y-m-d')  }}">


                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tempat_lahir" class="col-md-4 col-form-label text-md-right">{{ __('Tempat Lahir') }}</label>

                        <div class="col-md-8">
                            <input id="tempat_lahir" type="text" class="form-control-lg  form-control{{ $errors->has('tempat_lahir') ? ' is-invalid' : '' }}" name="tempat_lahir" value="{{ $pegawai->tempat_lahir }}">


                        </div>
                    </div>






                    </div>







                </div>
                <div class="col-lg-12 d-flex">

                        <div class="col-md-6" style="text-align: center">
                            <a href="{{ url('/usermanagement/hapus-user/'.$pegawai->id_user.'/'.$pegawai->id_unit_kerja) }}" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Data Pengguna?')" class="btn btn-lg btn-danger">
                                {{ __('HAPUS DATA') }}
                            </a>
                        </div>
                        <div class="col-md-6" style="text-align: center">
                          
                            <button type="submit" class="btn btn-lg btn-primary">
                                {{ __('PROSES EDIT') }}
                            </button>
                        </div>

                </div>



            </div>
            </form>

        </div>
        <!-- content-wrapper ends -->


      </div>

    </div>
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="{{asset('ui/vendors/js/vendor.bundle.base.js')}}"></script>
<script src="{{asset('ui/vendors/js/vendor.bundle.addons.js')}}"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="{{asset('ui/js/off-canvas.js')}}"></script>
<script src="{{asset('ui/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('ui/js/template.js')}}"></script>
<script src="{{asset('ui/js/settings.js')}}"></script>
<script src="{{asset('ui/js/todolist.js')}}"></script>
<!-- endinject -->

<script>
    $(function() {

        $('.select2').select2();



        @if($edit==1)
            parent.location.reload();
        @endif

    }
    );

</script>
</body>
</html>

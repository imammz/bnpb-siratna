<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('ui/vendors/iconfonts/mdi/font/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('ui/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('ui/vendors/css/vendor.bundle.addons.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('ui/css/vertical-layout-light/style.css')}}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{asset('ui/images/favicon.png')}}" />
</head>

<body class="sidebar-fixed">
<div class="container-scroller">

    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
            <form method="POST" action="{{ url('usermanagement-tambahuser') }}">
                @csrf

            <div class="row flex-grow">
                <div class="col-lg-12 d-flex">

                    <div class="col-md-12" style="text-align: center">
                        <h2  class="font-weight-light">TAMBAH DATA PENGGUNA  </h2>
                        <h3  class="font-weight-light text-primary">UNIT KERJA : {{strtoupper($unit->nama_unit_kerja)}} </h3>
                        <hr/>

                        @if($messages=Session::get('pesan'))
                            <div class="alert alert-success alert-block">
                                <strong>{{ $messages }}</strong>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <br/>
                    </div>

                </div>
                <div class="col-lg-6 d-flex">
                    <div class="auth-form-transparent text-left p-3">




                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama Lengkap') }}</label>

                                <div class="col-md-8">
                                    <input id="name" type="text" class="form-control-lg  form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('NIP') }}</label>

                                <div class="col-md-8">
                                    <input id="username" type="username" class="form-control-lg  form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required>

                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-8">
                                    <input id="email" type="email" class="form-control-lg  form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-8">
                                    <input id="password" type="password" class="form-control-lg  form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <div class="col-md-8">
                                    <input id="password-confirm" type="password" class="form-control-lg  form-control" name="password_confirmation" required>
                                </div>
                            </div>




                    </div>
                </div>
                <div class="col-lg-6 d-flex">

                    <div class="auth-form-transparent text-left p-3">
                        <div class="form-group row">
                            <label for="sta_level" class="col-md-4 col-form-label text-md-right">{{ __('Jabatan') }}</label>

                            <div class="col-md-8">
                                <select id="sta_level" type="text" class="form-control-lg  form-control" name="sta_level" value="{{ old('sta_level') }}" required>
                                    <option value="1"> Kepala {{$unit->nama_unit_kerja}} </option>
                                    <option value="2" selected> Staff </option>
                                </select>
                            </div>
                        </div>

                    <div class="form-group row">
                        <label for="id_role" class="col-md-4 col-form-label text-md-right">{{ __('Role Akses') }}</label>

                        <div class="col-md-8">
                            <select id="id_role" type="text" class="form-control-lg  form-control" name="id_role" value="{{ old('id_role') }}" required>
                            @foreach($role as $row)

                                    @if($row->id == 5)
                                        <option selected value="{{$row->id}}"> {{$row->nama_role}} </option>
                                    @else
                                        <option value="{{$row->id}}"> {{$row->nama_role}} </option>
                                    @endif

                            @endforeach
                            </select>
                            <input type="hidden" name="id_unit_kerja" value="{{$id_unit_kerja}}"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="tanggal_lahir" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Lahir') }}</label>

                        <div class="col-md-8">
                            <input id="tanggal_lahir" type="date" class="form-control-lg  form-control{{ $errors->has('tanggal_lahir') ? ' is-invalid' : '' }}" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}">


                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tempat_lahir" class="col-md-4 col-form-label text-md-right">{{ __('Tempat Lahir') }}</label>

                        <div class="col-md-8">
                            <input id="tempat_lahir" type="text" class="form-control-lg  form-control{{ $errors->has('tempat_lahir') ? ' is-invalid' : '' }}" name="tempat_lahir" value="{{ old('tempat_lahir') }}">


                        </div>
                    </div>






                    </div>







                </div>
                <div class="col-lg-12 d-flex">

                        <div class="col-md-12" style="text-align: center">
                            <button type="submit" class="btn btn-lg btn-primary">
                                {{ __('Daftar') }}
                            </button>
                        </div>

                </div>



            </div>
            </form>

        </div>
        <!-- content-wrapper ends -->


      </div>

    </div>
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="{{asset('ui/vendors/js/vendor.bundle.base.js')}}"></script>
<script src="{{asset('ui/vendors/js/vendor.bundle.addons.js')}}"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="{{asset('ui/js/off-canvas.js')}}"></script>
<script src="{{asset('ui/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('ui/js/template.js')}}"></script>
<script src="{{asset('ui/js/settings.js')}}"></script>
<script src="{{asset('ui/js/todolist.js')}}"></script>
<!-- endinject -->
</body>
</html>

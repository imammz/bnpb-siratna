<div id="page">

<div class="row" style="margin-top:40px;">
    <div class="col-lg-12 grid-margin d-flex flex-column">
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb bg-primary">
                    <li class="breadcrumb-item active"><a href="{{ url('suratmasuk-daftar') }}">
                        <b>Manajemen User Dan Role Akses</b></a></li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"> Silahkan lakukan pengaturan <b>pegawai dan struktur organisasi</b>,
                            <b>role akses</b>,
                            <b>dan unit kerja.</b> </h4>

                        <div class="row">
                            <div class="col-md-4 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <div class="text-danger mb-4">
                                            <a href="{{url('to/usermanagement-datapegawai')}}">
                                            <i class="mdi mdi-account mdi-36px"></i>
                                            <p class="font-weight-medium mt-2">Data Pegawai </p>
                                            </a>
                                        </div>
                                        <h1 class="font-weight-light">{{$jmlPegawai}}</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <div class="text-info mb-4">
                                            <a href="{{url('to/usermanagement-roleakses')}}">
                                            <i class="mdi mdi-worker mdi-36px"></i>
                                            <p class="font-weight-medium mt-2">Role Akses</p>
                                            </a>
                                        </div>
                                        <h1 class="font-weight-light">{{$jmlRole}}</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <div class="text-success mb-4">
                                            <a href="{{url('to/usermanagement-unitkerja')}}">
                                            <i class="mdi mdi-chart-arc mdi-36px"></i>
                                            <p class="font-weight-medium mt-2">Unit Kerja</p>
                                            </a>
                                        </div>
                                        <h1 class="font-weight-light">{{$jmlUnitKerja}}</h1>
                                    </div>
                                </div>
                            </div>

                        </div>



                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="alert alert-primary" role="alert">
                    Klik Tombol <b><i class="mdi mdi-plus"></i></b>   untuk tambah data pengguna di Unit Kerja bersangkutan
                </div>


                <ul class="file-tree">
                    @foreach($ka as $row1)
                        <li><span href="#">::{{$row1->nama_unit_kerja}}::
                                <ul>
                                    @foreach($row1->kepala as $row1a)
                                        <li>=> <button type="button" href="#" class="btn btn-inverse-dark btn-rounded btn-icon" data-toggle="modal" data-target="#modal-url" data-whatever="{{url('usermanagement/edituser/'.$row1a->user->id.'/'.$row1->kode)}}"><i class="mdi mdi-pencil"></i></button> {{$row1a->user->nama}}</li>
                                    @endforeach
                                    @foreach($row1->child as $row2)
                                        <li>   <a  onclick="return confirm('Apakah Anda Yakin Akan Menghapus Unit Kerja Ini ?')" class="btn btn-inverse-danger btn-rounded btn-icon"  href="{{url('usermanagement/hapus-unitkerja/'.$row2->kode)}}"><i class="fa fa-trash-o"></i></a>  || <span href="#"> ::{{$row2->nama_unit_kerja}}::   ({{count($row2->child)}}) <button type="button" href="#" class="btn btn-inverse-success btn-rounded btn-icon" data-toggle="modal" data-target="#modal-url" data-whatever="{{url('usermanagement/tambahuser/'.$row2->kode)}}"><i class="mdi mdi-plus"></i></button>
                                            <ul>
                                            @foreach($row2->kepala as $row2a)
                                                <li> => <button type="button" href="#" class="btn btn-inverse-dark btn-rounded btn-icon" data-toggle="modal" data-target="#modal-url" data-whatever="{{url('usermanagement/edituser/'.$row2a->user->id.'/'.$row2->kode)}}"><i class="mdi mdi-pencil"></i></button> {{$row2a->user->nama}}</li>
                                            @endforeach
                                            @foreach($row2->staff as $row2a)
                                                <li>[Staff] <button type="button" href="#" class="btn btn-inverse-dark btn-rounded btn-icon" data-toggle="modal" data-target="#modal-url" data-whatever="{{url('usermanagement/edituser/'.$row2a->user->id.'/'.$row2->kode)}}"><i class="mdi mdi-pencil"></i></button>  {{$row2a->user->nama}}</li>
                                            @endforeach

                                                @foreach($row2->child as $row3)
                                                    <li><a  onclick="return confirm('Apakah Anda Yakin Akan Menghapus Unit Kerja Ini ?')" class="btn btn-inverse-danger btn-rounded btn-icon"  href="{{url('usermanagement/hapus-unitkerja/'.$row3->kode)}}"><i class="fa fa-trash-o"></i></a><span href="#">::{{$row3->nama_unit_kerja}}:: ({{count($row3->child)}}) <button type="button" href="#" class="btn btn-inverse-success btn-rounded btn-icon" data-toggle="modal" data-target="#modal-url" data-whatever="{{url('usermanagement/tambahuser/'.$row3->kode)}}"><i class="mdi mdi-plus"></i></button>
                                                            <ul>
                                                                @foreach($row3->kepala as $row3a)
                                                                    <li> => <button type="button" href="#" class="btn btn-inverse-dark btn-rounded btn-icon" data-toggle="modal" data-target="#modal-url" data-whatever="{{url('usermanagement/edituser/'.$row3a->user->id.'/'.$row3->kode)}}"><i class="mdi mdi-pencil"></i></button> {{$row3a->user->nama}}</li>
                                                                @endforeach
                                                                @foreach($row3->staff as $row3a)
                                                                    <li>[Staff] <button type="button" href="#" class="btn btn-inverse-dark btn-rounded btn-icon" data-toggle="modal" data-target="#modal-url" data-whatever="{{url('usermanagement/edituser/'.$row3a->user->id.'/'.$row3->kode)}}"><i class="mdi mdi-pencil"></i></button>  {{$row3a->user->nama}}</li>
                                                                @endforeach

                                                                    @foreach($row3->child as $row4)
                                                                        <li><a  onclick="return confirm('Apakah Anda Yakin Akan Menghapus Unit Kerja Ini ?')" class="btn btn-inverse-danger btn-rounded btn-icon"  href="{{url('usermanagement/hapus-unitkerja/'.$row4->kode)}}"><i class="fa fa-trash-o"></i></a><span href="#">::{{$row4->nama_unit_kerja}}:: ({{count($row4->child)}}) <button type="button" href="#" class="btn btn-inverse-success btn-rounded btn-icon" data-toggle="modal" data-target="#modal-url" data-whatever="{{url('usermanagement/tambahuser/'.$row4->kode)}}"><i class="mdi mdi-plus"></i></button>
                                                                                <ul>
                                                                                    @foreach($row4->kepala as $row4a)
                                                                                        <li> => <button type="button" href="#" class="btn btn-inverse-dark btn-rounded btn-icon" data-toggle="modal" data-target="#modal-url" data-whatever="{{url('usermanagement/edituser/'.$row4a->user->id.'/'.$row4->kode)}}"><i class="mdi mdi-pencil"></i></button> {{$row4a->user->nama}}</li>
                                                                                    @endforeach
                                                                                    @foreach($row4->staff as $row4a)
                                                                                        <li>[Staff] <button type="button" href="#" class="btn btn-inverse-dark btn-rounded btn-icon" data-toggle="modal" data-target="#modal-url" data-whatever="{{url('usermanagement/edituser/'.$row4a->user->id.'/'.$row4->kode)}}"><i class="mdi mdi-pencil"></i></button>  {{$row4a->user->nama}}</li>
                                                                                    @endforeach

                                                                                        @foreach($row4->child as $row5)
                                                                                            <li><a  onclick="return confirm('Apakah Anda Yakin Akan Menghapus Unit Kerja Ini ?')" class="btn btn-inverse-danger btn-rounded btn-icon"  href="{{url('usermanagement/hapus-unitkerja/'.$row5->kode)}}"><i class="fa fa-trash-o"></i></a><span href="#">::{{$row5->nama_unit_kerja}}:: ({{count($row5->child)}}) <button type="button" href="#" class="btn btn-inverse-success btn-rounded btn-icon" data-toggle="modal" data-target="#modal-url" data-whatever="{{url('usermanagement/tambahuser/'.$row5->kode)}}"><i class="mdi mdi-plus"></i></button>
                                                                                                    <ul>
                                                                                                        @foreach($row5->kepala as $row5a)
                                                                                                            <li> => <button type="button" href="#" class="btn btn-inverse-dark btn-rounded btn-icon" data-toggle="modal" data-target="#modal-url" data-whatever="{{url('usermanagement/edituser/'.$row5a->user->id.'/'.$row5->kode)}}"><i class="mdi mdi-pencil"></i></button> {{$row5a->user->nama}}</li>
                                                                                                        @endforeach
                                                                                                        @foreach($row5->staff as $row5a)
                                                                                                            <li>[Staff] <button type="button" href="#" class="btn btn-inverse-dark btn-rounded btn-icon" data-toggle="modal" data-target="#modal-url" data-whatever="{{url('usermanagement/edituser/'.$row5a->user->id.'/'.$row5->kode)}}"><i class="mdi mdi-pencil"></i></button>  {{$row5a->user->nama}}</li>
                                                                                                        @endforeach



                                                                                                    </ul>
                                                                                                </span></li>


                                                                                        @endforeach


                                                                                </ul>
                                                                            </span></li>


                                                                    @endforeach



                                                            </ul>
                                                        </span></li>


                                                @endforeach


                                            </ul>
                                            </span></li>


                                     @endforeach
                                </ul>
                            </span>
                        </li>


                    @endforeach

                </ul>



            </div>

        </div>
    </div>
</div>


        <style>
            body{font-family:'Roboto',Arial, Helvetica, sans-serif; background-color:#fafafa;}
            .container { margin:150px auto; max-width:728px;}
        </style>
        <link href="{{url('tree-structure/css/file-explore.css')}}" rel="stylesheet" type="text/css">


        <script src="{{url('tree-structure/js/file-explore.js')}}"></script>

        <script>
            $(function(){
                $(".file-tree").filetree();
            });
        </script>



@include('layouts.js-page')

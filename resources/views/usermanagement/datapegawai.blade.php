<div id="page">



    <div class="row" style="margin-top:40px;">
        <div class="col-lg-12 grid-margin d-flex flex-column">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb bg-primary">
                        <li class="breadcrumb-item active"><a href="{{ url('to/suratmasuk-daftar') }}">
                                <b>Data Pegawai</b></a></li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title"> Silahkan Lakukan Pengaturan Data Pegawai</h4>

                            <div class="row">
                                <div class="col-md-12 grid-margin stretch-card">
                                    <div class="card">
                                        <h5 class="text-center"> Form Tambah Data Pegawai</h5>
                                        <div class="card-body">

                                            {!! Form::open(['url' => 'suratmasuk-tambah-proses','class'=>'form-sample','method' => 'post','files' => true]); !!} {!! Form::token(); !!}

                                            <div class="row">
                                                <div class="col-md-6">

                                                    <div class="form-group row">
                                                        {!! Form::label('nip', 'NIP / Username',['class'=>'col-sm-3 col-form-label']); !!}
                                                        <div class="col-sm-9">

                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        {!! Form::label('password', 'Password',['class'=>'col-sm-3 col-form-label']); !!}
                                                        <div class="col-sm-9">

                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        {!! Form::label('nama', 'Nama',['class'=>'col-sm-3 col-form-label']); !!}
                                                        <div class="col-sm-9">

                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        {!! Form::label('id_role', 'Rule Akses',['class'=>'col-sm-3 col-form-label']); !!}
                                                        <div class="col-sm-9">

                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="col-md-6">

                                                    <div class="form-group row">
                                                        {!! Form::label('tanggal_lahir', 'Tanggal Lahir',['class'=>'col-sm-3 col-form-label']); !!}
                                                        <div class="col-sm-9">

                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        {!! Form::label('tempat_lahir', 'Tempat Lahir',['class'=>'col-sm-3 col-form-label']); !!}
                                                        <div class="col-sm-9">

                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        {!! Form::label('id_unit_kerja', 'Unit Kerja',['class'=>'col-sm-3 col-form-label']); !!}
                                                        <div class="col-sm-9">

                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        {!! Form::label('sta_level', 'Jabatan Pada Unit Kerja',['class'=>'col-sm-3 col-form-label']); !!}
                                                        <div class="col-sm-9">

                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-12 text-center">
                                                        <button class="btn btn-success">
                                                            <i class="fa fa-save"></i> SIMPAN
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>


                                            {!! Form::close() !!}


                                        </div>
                                    </div>
                                </div>


                            </div>


                            <div class="row">
                                <div class="col-md-12 grid-margin stretch-card">
                                    <div class="card">
                                        <h5 class="text-center"> Tabel Data Pegawai</h5>

                                        <div class="card-body">

                                            <table width="100%" class="table table-hover table-bordered"
                                                   id="daftarSuratMasukSudah">
                                                <thead>
                                                <tr style="text-align:center">
                                                    <th width="6%">No</th>
                                                    <th width="20%">Pengirim</th>
                                                    <th width="20%">Nomor Surat</th>
                                                    <th>Perihal</th>
                                                    <th width="15%">Tanggal Surat Masuk</th>
                                                    <th width="10%">Ditandai</th>
                                                </tr>
                                                </thead>
                                            </table>


                                        </div>
                                    </div>
                                </div>


                            </div>


                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
            <script>
                $('#daftarSuratMasukSudah').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: '{!! url('suratmasuk-daftar-data-sudah/') !!}',
                    columns: [
                        {data: "DT_RowIndex", orderable: false, searchable: false},
                        {data: 'pengirim', name: 'pengirim'},
                        {data: 'pengirim_nomor_surat', name: 'pengirim_nomor_surat'},
                        {data: 'perihal', name: 'perihal'},
                        {data: 'tgl_surat', name: 'tgl_surat'},
                        {data: 'marked', name: 'marked'},
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            className: 'dt-body-right'
                        },
                        {
                            targets: 1,
                            className: 'dt-body-left'
                        },
                        {
                            targets: 4,
                            className: 'dt-body-center'
                        },
                        {
                            targets: 5,
                            className: 'dt-body-center'
                        },
                    ],
                });

            </script>

@extends('layouts.app')

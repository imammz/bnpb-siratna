<div id="page">
<div class="row" style="margin-top:40px;">
    <div class="col-lg-12 grid-margin d-flex flex-column">
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb bg-primary">
                    <li class="breadcrumb-item"><a href="{{ url('to/suratmasuk-daftar') }}">KPA Disposisi</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><b>Tambah</b></li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Tambah KPA Disposisi</h4>

                        <div class="panel panel-square panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">KPA Disposisi.</h3>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" method="POST" autocomplete="OFF" enctype="multipart/form-data">

                                    <div class="row">


                                        <div class="col-md-6">

                                            <legend style="padding-bottom:10px;font-size:16px;">Form Utama</legend>

                                            <div class="form-group ">
                                                <label class="col-md-4 control-label">Nomor Agenda</label>
                                                <div class="col-md-8" style="padding-right:15px;">
                                                    <div class="input-group">
                                                        <input type="text" value="" class="form-control input-sm" placeholder="Auto Generate" readonly="">
                                                        <span class="input-group-addon"><i class="fa fa-cog fa-fw"></i></span>
                                                    </div>
                                                                                            </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="col-md-4 control-label">Kepada</label>
                                                <div class="col-md-8" style="padding-right:15px;">
                                                    <select class="select-chosen form-control input-sm kepada" name="kepada" data-placeholder="Pilih Kepada" style="display: none;">
                                                        <option></option>
                                                                                                        <option value="000001">Kepala Badan Nasional Penanggulangan Bencana</option>
                                                                                                        <option value="000003">SEKRETARIS UTAMA</option>
                                                                                                        <option value="00000F">Biro Umum</option>
                                                                                                    </select><div class="chosen-container chosen-container-single" style="width: NaN;" title=""><a class="chosen-single chosen-default" tabindex="-1"><span>Pilih Kepada</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off"></div><ul class="chosen-results"></ul></div></div>
                                                                                            </div>
                                            </div>



                                            <div class="form-group ">
                                                <label class="col-md-4 control-label">Perihal</label>
                                                <div class="col-md-8" style="padding-right:15px;">
                                                    <textarea class="form-control input-sm" rows="3" name="perihal" placeholder=""></textarea>
                                                                                            </div>
                                            </div>

                                            <hr>
                                            <div class="form-group ">
                                                <label class="col-md-4 control-label">Diteruskan kepada:</label>
                                                <div class="col-md-8" style="padding-right:15px;">
                                                    <select class="select-chosen form-control input-sm arahan" name="arahan" data-placeholder="Diteruskan Kepada" style="display: none;">
                                                        <option></option>
                                                                                                        <option value="00000F">Biro Umum</option>
                                                                                                        <option value="00000U">Sekretaris Biro Umum</option>
                                                                                                        <option value="000012">admin</option>
                                                                                                        <option value="00002Z">Bagian Tata Usaha</option>
                                                                                                        <option value="000030">Bagian Kepegawaian</option>
                                                                                                        <option value="000031">Bagian Rumah Tangga</option>
                                                                                                    </select><div class="chosen-container chosen-container-single" style="width: NaN;" title=""><a class="chosen-single chosen-default" tabindex="-1"><span>Diteruskan Kepada</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off"></div><ul class="chosen-results"></ul></div></div>
                                                                                            </div>
                                            </div>



                                        </div>

                                        <div class="col-md-6">

                                            <legend style="padding-bottom:10px;font-size:16px;">Lampiran</legend>

                                            <div class="row" style="margin-bottom:15px;">
                                                <span class="btn-add-lampiran pull-right btn btn-warning btn-xs"><i class="fa fa-plus"></i> Tambah lampiran</span>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="tabel-group-lampiran  table table-th-block table-default table-striped table-bordered responsive">
                                                        <tbody><tr class="group-lampiran">
                                                            <td class="text-center">
                                                                <input type="file" accept=".jpg, .png, .jpeg, .pdf" name="lampiran[]" class="">
                                                            </td>
                                                            <td class="text-center" style="width:10%;">
                                                                <span class="pull-right btn-reset-lampiran btn btn-info btn-xs"><i class="fa fa-refresh"></i></span>
                                                                <span style="display:none;" class="pull-right btn-remove-lampiran btn btn-danger btn-xs"><i class="fa fa-remove"></i></span>
                                                            </td>
                                                        </tr>
                                                </tbody></table>
                                            </div>


                                        </div>


                                    </div>

                                    <hr>

                                    <div class="row">
                                        <div class="col-md-12">

                                            <h4>Pilih Surat Masuk</h4>
                                            <div class="table-responsive first-step" id="pagination">
                                                <table class="table table-th-block table-default table-striped table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="6" style="padding:5px;">
                                                                <div class="row">
                                                                    <div class="col-md-3 col-xs-3">
                                                                        <input type="text" placeholder="Nomor Surat / Unit Kerja" class="col-md-3 form-control input-sm filter">
                                                                    </div>
                                                                    <div class="col-md-3 col-xs-3">
                                                                        <button type="button" class="btn btn-danger btn-sm" id="clear">Clear</button>
                                                                    </div>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th class="text-center" style="width:120px">Tanggal</th>
                                                            <th class="text-center" style="width:170px;">No. Surat</th>
                                                            <th class="text-center" style="width:170px;">Unit Kerja</th>
                                                            <th class="text-center">Perihal</th>
                                                            <th class="text-center" style="width:120px;">Nominal</th>
                                                            <th class="text-center" style="width:120px;">Select All &nbsp; <input type="checkbox" id="all"> </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                                                                <tr style="display: table-row;">
                                                        <td class="text-center">
                                                            <div>
                                                                <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Surat" data-placement="right">09 Jan 2018</span>
                                                            </div>
                                                            <div>
                                                                <span class="label label-danger square" data-toggle="tooltip" data-original-title="Tgl. Diterima" data-placement="right">10 Jan 2018</span>
                                                            </div>
                                                        </td>
                                                        <td class="text-center data-search"> <span data-unit-kerja="Biro Umum" class="text-danger bold">2242425</span> </td>
                                                        <td class="text-center"> Biro Umum </td>
                                                        <td class="text-left"> Testing </td>
                                                        <td class="text-right"> <span class="text-danger bold">2.000.000</span> </td>
                                                        <td class="text-center"><input type="checkbox" name="to[]" class="to" value="00000E"></td>
                                                    </tr>


                                                    </tbody>
                                                </table>

                                                <div class="text-right">
                                                    <ul class="my-navigation pagination">
                                                        <li class="simple-pagination-first"><a href="#" class="simple-pagination-navigation-first simple-pagination-navigation-disabled" data-simple-pagination-page-number="1">&lt;&lt;</a></li>
                                                        <li class="simple-pagination-previous"><a href="#" class="simple-pagination-navigation-previous simple-pagination-navigation-disabled" data-simple-pagination-page-number="1">&lt;</a></li>
                                                        <li class="simple-pagination-page-numbers"><a href="#" class="simple-pagination-navigation-page simple-pagination-navigation-disabled" data-simple-pagination-page-number="1">1</a></li>
                                                        <li class="simple-pagination-next"><a href="#" class="simple-pagination-navigation-next simple-pagination-navigation-disabled" data-simple-pagination-page-number="1">&gt;</a></li>
                                                        <li class="simple-pagination-last"><a href="#" class="simple-pagination-navigation-last simple-pagination-navigation-disabled" data-simple-pagination-page-number="1">&gt;&gt;</a></li>
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>




                                    <hr style="margin:10px 0;">
                                    <div class="form-group">
                                        <div class="col-md-offset-8 col-md-4" style="padding-right:15px;">
                                            <div class="row" style="margin-left:-10px;">
                                                <div class="col-xs-6" style="padding-left:10px;">
                                                    <a href="http://e-office.bnpb.go.id/siratna/eoffice/kpa-disposisi" class="btn btn-default btn-block btn-square">
                                                        <i class="fa fa-undo"></i> Batal
                                                    </a>
                                                </div>
                                                <div class="col-xs-6" style="padding-left:10px;">
                                                    <button class="btn btn-primary btn-block btn-square" type="submit" name="process" value="Add">
                                                        <i class="fa fa-check"></i> Simpan Data
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div><!-- /.panel-body -->
                        </div>

                    </div>
                </div>
            </div>



        </div>
    </div>

</div>
@include('layouts.js-page')

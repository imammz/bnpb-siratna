<div id="page">

<div class="row" style="margin-top:40px;">
    <div class="col-lg-12 grid-margin d-flex flex-column">
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb bg-primary">
                    <li class="breadcrumb-item"><a href="{{ url('to/suratmasuk-daftar') }}">KPA Disposisi</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><b>Detil</b></li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Data Detil</h4>

                        <div class="row">
                            <div class="col-lg-8">
                                  <div class="panel panel-primary panel-square">
                                    <header class="panel-heading">
                                        <h3 class="panel-title">General</h3>
                                    </header>

                                    <div class="panel-body profile-activity">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover table-bordered responsive">
                                                <tbody>
                                                    <tr>
                                                        <td style="width:50%"><b class="text-info">Kepada</b></td>
                                                        <td class="text-right"><b class="text-info">Sekretariat Utama</b></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:50%"><b class="text-info">No. Agenda</b></td>
                                                        <td class="text-right"><b class="text-info">KPA/UM/2018/0001</b></td>
                                                    </tr>

                                                </tbody>
                                            </table>

                                        </div>

                                         <div class="table-responsive">
                                            <table class="table table-striped table-hover table-bordered responsive">
                                                <tbody>
                                                    <tr>
                                                        <td style="width:50%"><b class="">Tgl. Input</b></td>
                                                        <td class="text-right"><b class="">10 Jan 2018</b></td>
                                                    </tr>

                                                </tbody>
                                            </table>

                                        </div>


                                    </div>
                                </div>

                                  <div class="panel panel-default panel-square">
                                    <header class="panel-heading">
                                        <h3 class="panel-title">Perihal</h3>
                                    </header>
                                    <div class="panel-body profile-activity">
                                        <div class="table-responsive">
                                            <figure>Testing</figure>
                                        </div>
                                    </div>

                                </div>

                                <section class="panel panel-default panel-square">
                                    <header class="panel-heading">
                                        <div class="right-content">
                                            <button type="button" class="btn btn-warning square btn-xs" id="add-surat-masuk"><i class="fa fa-plus"></i> <span class="hidden-xs">Tambah</span> Surat Masuk</button>
                                        </div>
                                        <h3 class="panel-title">Daftar Surat Masuk</h3>
                                    </header>
                                    <div class="table-responsive">
                                        <table class="table table-th-block  table-striped table-bordered responsive">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" style="width:30px">#</th>
                                                    <th class="text-center" style="width:120px">Tanggal</th>
                                                    <th class="text-center" style="width:120px">Nomor Surat</th>
                                                    <th class="text-center" style="width:120px">Unit Kerja</th>
                                                    <th class="text-center">Perihal</th>
                                                    <th class="text-center" style="width:100px;">Nominal</th>
                                                    <th class="text-center" style="width:50px">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                                                    <tr>
                                                    <td class="text-center">1</td>
                                                    <td class="text-center">
                                                        <div>
                                                            <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Surat" data-placement="right">09 Jan 2018</span>
                                                        </div>
                                                        <div>
                                                            <span class="label label-danger square" data-toggle="tooltip" data-original-title="Tgl. Diterima" data-placement="right">10 Jan 2018</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"> <span class="text-danger bold">24242135</span> </td>
                                                    <td class="text-center"> Biro Umum </td>
                                                    <td class="text-left"> Testing </td>
                                                    <td class="text-right"> <span class="text-danger bold">1.200.000</span> </td>
                                                    <td class="text-center">
                                                        <form class="form-horizontal" method="POST" autocomplete="OFF" enctype="multipart/form-data">

                                                        <input type="hidden" name="kpa_disposisi_detail_id" value="00MDAwMDBE">
                                                        <button onclick="return confirm('Anda yakin akan menghapus Surat Masuk dengan nomor surat: 24242135 dari daftar disposisi ?');" class="btn btn-danger btn-xs" type="submit" name="process" value="RemoveSuratMasuk">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                                                </tbody>
                                        </table>
                                    </div>

                                    <div style="display:none;" id="html-add-surat-masuk">

                                        <hr style="margin-bottom:0px;">
                                        <header class="panel-heading">
                                            <h3 class="panel-title">Tambah Surat Masuk</h3>
                                        </header>
                                        <form class="form-horizontal" method="POST" autocomplete="OFF" enctype="multipart/form-data">

                                        <div class="table-responsive first-step" id="pagination">
                                            <table class="table table-th-block table-default table-striped table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th colspan="6" style="padding:5px;">
                                                            <div class="row">
                                                                <div class="col-md-3 col-xs-3">
                                                                    <input type="text" placeholder="Nomor Surat / Unit Kerja" class="col-md-3 form-control input-sm filter">
                                                                </div>
                                                                <div class="col-md-3 col-xs-3">
                                                                    <button type="button" class="btn btn-danger btn-sm" id="clear">Clear</button>
                                                                </div>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-center" style="width:120px">Tanggal</th>
                                                        <th class="text-center" style="width:170px;">No. Surat</th>
                                                        <th class="text-center" style="width:170px;">Unit Kerja</th>
                                                        <th class="text-center">Perihal</th>
                                                        <th class="text-center" style="width:120px;">Nominal</th>
                                                        <th class="text-center" style="width:120px;">Select All &nbsp; <input type="checkbox" id="all"> </th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                                                        <tr style="display: table-row;">
                                                    <td class="text-center">
                                                        <div>
                                                            <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Surat" data-placement="right">09 Jan 2018</span>
                                                        </div>
                                                        <div>
                                                            <span class="label label-danger square" data-toggle="tooltip" data-original-title="Tgl. Diterima" data-placement="right">10 Jan 2018</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center data-search"> <span data-unit-kerja="Biro Umum" class="text-danger bold">2242425</span> </td>
                                                    <td class="text-center"> Biro Umum </td>
                                                    <td class="text-left"> Testing </td>
                                                    <td class="text-right"> <span class="text-danger bold">2.000.000</span> </td>
                                                    <td class="text-center"><input type="checkbox" name="to[]" class="to" value="00000E"></td>
                                                </tr>


                                                </tbody>
                                            </table>

                                            <div class="text-right">
                                                <ul class="my-navigation pagination">
                                                    <li class="simple-pagination-first"><a href="#" class="simple-pagination-navigation-first simple-pagination-navigation-disabled" data-simple-pagination-page-number="1">&lt;&lt;</a></li>
                                                    <li class="simple-pagination-previous"><a href="#" class="simple-pagination-navigation-previous simple-pagination-navigation-disabled" data-simple-pagination-page-number="1">&lt;</a></li>
                                                    <li class="simple-pagination-page-numbers"><a href="#" class="simple-pagination-navigation-page simple-pagination-navigation-disabled" data-simple-pagination-page-number="1">1</a></li>
                                                    <li class="simple-pagination-next"><a href="#" class="simple-pagination-navigation-next simple-pagination-navigation-disabled" data-simple-pagination-page-number="1">&gt;</a></li>
                                                    <li class="simple-pagination-last"><a href="#" class="simple-pagination-navigation-last simple-pagination-navigation-disabled" data-simple-pagination-page-number="1">&gt;&gt;</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <hr style="margin:10px 0;">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-offset-8 col-md-4" style="padding-right:20px;">
                                                    <div class="row" style="margin-left:-10px;">
                                                        <div class="col-xs-6" style="padding-left:30px;">
                                                            <button type="button" class="btn btn-default btn-block btn-square btn-sm" id="cancel-surat-masuk">Batal</button>
                                                        </div>
                                                        <div class="col-xs-6" style="padding-left:10px;">
                                                            <button class="btn btn-primary btn-block btn-square btn-sm" type="submit" name="process" value="AddSuratMasuk">
                                                            Tambah
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        </form>
                                    </div>
                                </section>

                                <section class="panel panel-default panel-square">
                                    <header class="panel-heading">
                                        <h3 class="panel-title">Lampiran File</h3>
                                    </header>
                                    <div class="table-responsive">
                                        <table class="table table-th-block table-default table-striped table-bordered responsive">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" style="width:7%">#</th>
                                                    <th class="text-center">Nama File</th>
                                                    <th class="text-center" style="width:100px;">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                                                    <tr><td colspan="3" class="text-center" style="height:70px;">Belum ada data.</td></tr>

                                                                                </tbody>
                                        </table>
                                    </div>
                                </section>

                                <section class="panel panel-default panel-square">
                                    <header class="panel-heading">
                                        <h3 class="panel-title">Instruksi / Catatan</h3>
                                    </header>
                                    <div class="table-responsive">
                                        <table class="table table-th-block table-default table-striped table-bordered responsive">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" style="width:12%">Tanggal</th>
                                                    <th class="text-center" style="width:25%">Pengirim</th>
                                                    <th class="text-center" style="width:30%">Kepada</th>
                                                    <th class="text-center">Catatan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                                                                                        <tr><td colspan="6" class="text-center" style="height:70px;">Belum ada data.</td></tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </section>

                            </div>
                            <div class="col-lg-4">


                                <div class="panel panel-default panel-square">
                                    <header class="panel-heading">
                                        <h3 class="panel-title">Created by:</h3>
                                    </header>
                                    <div class="table-responsive">
                                        <table class="table table-th-block table-hover table-bordered responsive">
                                            <tbody><tr>
                                                <td class="text-center" style="width: 150px;"> <div> <span style="width:100%" class="label label-info square" data-toggle="tooltip" data-original-title="Tgl. Create" data-placement="right"> 10-01-2018 14:51 </span></div> </td>
                                                <td class="text-right"><strong class="text-primary">TU Persuratan Biro Umum</strong><br></td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                </div>

                                <div class="panel panel-default panel-square">
                                    <header class="panel-heading">
                                        <h3 class="panel-title"> Diteruskan Kepada:</h3>
                                    </header>
                                    <div class="table-responsive">
                                        <table class="table table-th-block table-hover table-bordered responsive">

                                                                                <tbody><tr>
                                                <td class="text-center" style="width: 30px;">1</td>
                                                <td class="text-center" style="width: 120px;">
                                                    <div><span style="width:100%" class="label label-info square"> 10-01-2018 14:51 </span></div>
                                                    <div><span style="width:100%" class="label label-warning square"> waiting </span></div>
                                                </td>
                                                <td class="text-right"><strong class="text-primary">Sekretaris Biro Umum</strong><br></td>
                                            </tr>



                                        </tbody></table>
                                    </div>
                                </div>



                                <div class="panel panel-default panel-square">
                                    <header class="panel-heading">
                                        <h3 class="panel-title">Disposisi:</h3>
                                    </header>
                                    <div class="table-responsive">
                                        <table class="table table-th-block table-hover table-bordered responsive">
                                                                        <tbody><tr><td colspan="3" class="text-center" style="height:50px;">Belum ada data.</td></tr>


                                        </tbody></table>
                                    </div>
                                </div>



                                <div class="panel panel-danger panel-square panel-block-color">
                                    <header class="panel-heading">
                                        <h3 class="panel-title">Preview / Download</h3>
                                    </header>
                                    <div class="panel-body bg-danger profile-activity">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <a class="btn btn-danger btn-sm btn-block view-pdf" href="http://e-office.bnpb.go.id/siratna/eoffice/kpa-disposisi/generate?file=evcHJldmlld3x8a3BhfHwwMDAwMEU=">PREVIEW</a>
                                            </div>
                                            <div class="col-xs-6">
                                                <a class="btn btn-danger btn-sm btn-block" href="http://e-office.bnpb.go.id/siratna/eoffice/kpa-disposisi/generate?file=wnZG93bmxvYWR8fGtwYXx8MDAwMDBF" target="_blank">DOWNLOAD</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>


                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

<style>
    h3 {
        margin-top: 20px;
        padding-top: 20px;
    }
</style>


@include('layouts.js-page')


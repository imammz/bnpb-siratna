<div id="page">

<div class="row" style="margin-top:40px;">
    <div class="col-lg-12 grid-margin d-flex flex-column">
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb bg-primary">
                    <li class="breadcrumb-item"><a href="{{ url('kpa-suratmasuk') }}">KPA</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><b>Disposisi</b></li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">KPA Disposisi</h4>

                        <div class="panel panel-info panel-square">
                            <div class="panel-heading">
                                <div class="right-content">
                                    <a href="{{ url('to/kpa-disposisi-tambah') }}" class="btn btn-warning btn-square btn-xs">
                                        <i class="fa fa-plus"></i> <span class="hidden-xs">Tambah</span> Disposisi
                                    </a>
                                </div>
                                <hr/>
                            </div>
                            <div class="panel-body">
                                <form action="" method="get">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label><span class="label label-primary square">Keyword</span> <span class="label label-warning square">Pencarian</span></label>
                                                <input type="text" class="form-control input-sm" value="" placeholder="Nomor Surat / Unit Kerja / Perihal " name="keyword">
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="hidden-xs hidden-sm">&nbsp;</label>
                                                <button class="btn btn-sm btn-danger btn-square btn-block" type="submit"><i class="fa fa-search"></i> Cari</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="table-responsive">
                                <table class="table  table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width:50px">#</th>
                                            <th class="text-center" style="width:150px">Tanggal</th>
                                            <th class="text-center" style="width:250px">Kepada</th>
                                            <th class="text-center" style="width:200px">No. Agenda</th>
                                            <th class="text-center">Perihal</th>
                                            <th class="text-center" style="width:150px">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                                                    <tr>
                                            <td class="text-center">1</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Create" data-placement="right">10 Jan 2018 14:51</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class=" bold">Sekretariat Utama</span> </td>
                                            <td class="text-center"> <span class=" bold">KPA/UM/2018/0001</span> </td>
                                            <td class="text-left"> Testing </td>
                                            <td class="text-center">
                                                <a class="btn btn-sm btn-inverse" href="{{ url('kpa-disposisi-detil') }}?id=00MDAwMDBF"><i class="fa fa-folder-open"></i></a>

                                                                                        <a class="btn btn-sm btn-warning" href="http://e-office.bnpb.go.id/siratna/eoffice/kpa-disposisi/edit?id=00MDAwMDBF"><i class="fa fa-edit"></i></a>
                                                                                    </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">2</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Create" data-placement="right">28 Des 2017 00:23</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class=" bold">Sekretariat Utama</span> </td>
                                            <td class="text-center"> <span class=" bold">KPA/UM/2017/0012</span> </td>
                                            <td class="text-left"> Testing 2 </td>
                                            <td class="text-center">
                                                <a class="btn btn-sm btn-inverse" href="{{ url('kpa-disposisi-detil') }}?id=00MDAwMDBD"><i class="fa fa-folder-open"></i></a>

                                                                                        <a class="btn btn-sm btn-warning" href="http://e-office.bnpb.go.id/siratna/eoffice/kpa-disposisi/edit?id=00MDAwMDBD"><i class="fa fa-edit"></i></a>
                                                                                    </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">3</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Create" data-placement="right">27 Des 2017 01:27</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class=" bold">Sekretariat Utama</span> </td>
                                            <td class="text-center"> <span class=" bold">KPA/UM/2017/0011</span> </td>
                                            <td class="text-left"> Testing 2 </td>
                                            <td class="text-center">
                                                <a class="btn btn-sm btn-inverse" href="{{ url('kpa-disposisi-detil') }}?id=00MDAwMDBC"><i class="fa fa-folder-open"></i></a>

                                                                                        <a class="btn btn-sm btn-warning" href="http://e-office.bnpb.go.id/siratna/eoffice/kpa-disposisi/edit?id=00MDAwMDBC"><i class="fa fa-edit"></i></a>
                                                                                    </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">4</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Create" data-placement="right">27 Des 2017 00:10</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class=" bold">Sekretariat Utama</span> </td>
                                            <td class="text-center"> <span class=" bold">KPA/UM/2017/0010</span> </td>
                                            <td class="text-left"> Testing </td>
                                            <td class="text-center">
                                                <a class="btn btn-sm btn-inverse" href="{{ url('kpa-disposisi-detil') }}?id=00MDAwMDBB"><i class="fa fa-folder-open"></i></a>

                                                                                        <a class="btn btn-sm btn-warning" href="http://e-office.bnpb.go.id/siratna/eoffice/kpa-disposisi/edit?id=00MDAwMDBB"><i class="fa fa-edit"></i></a>
                                                                                    </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">5</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Create" data-placement="right">26 Des 2017 19:37</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class=" bold">KEPALA BNPB</span> </td>
                                            <td class="text-center"> <span class=" bold">KPA/UM/2017/0009</span> </td>
                                            <td class="text-left"> Testing sajas </td>
                                            <td class="text-center">
                                                <a class="btn btn-sm btn-inverse" href="{{ url('kpa-disposisi-detil') }}?id=00MDAwMDA5"><i class="fa fa-folder-open"></i></a>

                                                                                        <a class="btn btn-sm btn-warning" href="http://e-office.bnpb.go.id/siratna/eoffice/kpa-disposisi/edit?id=00MDAwMDA5"><i class="fa fa-edit"></i></a>
                                                                                    </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">6</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Create" data-placement="right">03 Des 2017 01:12</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class=" bold">Sekretaris Utama</span> </td>
                                            <td class="text-center"> <span class=" bold">KPA/UM/2017/0008</span> </td>
                                            <td class="text-left"> masuk </td>
                                            <td class="text-center">
                                                <a class="btn btn-sm btn-inverse" href="{{ url('kpa-disposisi-detil') }}?id=00MDAwMDA4"><i class="fa fa-folder-open"></i></a>

                                                                                        <a class="btn btn-sm btn-warning" href="http://e-office.bnpb.go.id/siratna/eoffice/kpa-disposisi/edit?id=00MDAwMDA4"><i class="fa fa-edit"></i></a>
                                                                                    </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">7</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Create" data-placement="right">03 Des 2017 01:10</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class=" bold">Sekretaris Utama</span> </td>
                                            <td class="text-center"> <span class=" bold">KPA/UM/2017/0007</span> </td>
                                            <td class="text-left"> Testing lagi </td>
                                            <td class="text-center">
                                                <a class="btn btn-sm btn-inverse" href="{{ url('kpa-disposisi-detil') }}?id=00MDAwMDA3"><i class="fa fa-folder-open"></i></a>

                                                                                        <a class="btn btn-sm btn-warning" href="http://e-office.bnpb.go.id/siratna/eoffice/kpa-disposisi/edit?id=00MDAwMDA3"><i class="fa fa-edit"></i></a>
                                                                                    </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">8</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Create" data-placement="right">03 Des 2017 01:08</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class=" bold">KEPALA BNPB</span> </td>
                                            <td class="text-center"> <span class=" bold">KPA/UM/2017/0005</span> </td>
                                            <td class="text-left"> Tes </td>
                                            <td class="text-center">
                                                <a class="btn btn-sm btn-inverse" href="{{ url('kpa-disposisi-detil') }}?id=00MDAwMDA1"><i class="fa fa-folder-open"></i></a>

                                                                                        <a class="btn btn-sm btn-warning" href="http://e-office.bnpb.go.id/siratna/eoffice/kpa-disposisi/edit?id=00MDAwMDA1"><i class="fa fa-edit"></i></a>
                                                                                    </td>
                                        </tr>
                                                                </tbody>
                                </table>
                            </div>


                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-6" style="padding-left: 15px;">
                                        Total Data : <strong>8</strong>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="text-right">
                                                                            </div>
                                    </div>
                                </div>
                            </div><!-- /.panel-body -->
                        </div>



                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

@include('layouts.js-page')

<div id="page">
<div class="row" style="margin-top:40px;">
    <div class="col-lg-12 grid-margin d-flex flex-column">
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb bg-primary">
                    <li class="breadcrumb-item active" aria-current="page"><a href="{{ url('to/kpa-suratmasuk') }}">
                        <b>KPA</b></a></li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">KPA Surat Masuk</h4>
                        <div class="panel panel-info panel-square">
                            <div class="panel-body">
                                <form action="" method="get">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label><span class="label label-primary square">Keyword</span> <span class="label label-warning square">Pencarian</span></label>
                                                <input type="text" class="form-control input-sm" value="" placeholder="Nomor Surat / Unit Kerja / Perihal " name="keyword">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label><span class="label label-primary square">Status</span></label>
                                                <select name="tipe" class="form-control input-sm">
                                                    <option value="" selected="selected">Semua</option>
                                                    <option value="available">Available</option>
                                                    <option value="used">Used</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="hidden-xs hidden-sm">&nbsp;</label>
                                                <button class="btn btn-sm btn-danger btn-square btn-block" type="submit"><i class="fa fa-search"></i> Cari</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div><div class="panel-heading">
                                <div class="right-content">
                                                                <a href="{{ url('to/kpa-suratmasuk-tambah') }}" class="btn btn-warning btn-square btn-xs">
                                        <i class="fa fa-plus"></i> <span class="hidden-xs">Tambah</span> Surat Masuk
                                    </a>
                                                            </div>

                            </div>
                            <div class="row">
                                <hr/>
                            </div>


                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width:50px">#</th>
                                            <th class="text-center" style="width:120px">Tanggal</th>
                                            <th class="text-center" style="width:150px">Nomor Surat</th>
                                            <th class="text-center" style="width:150px">Unit Kerja</th>
                                            <th class="text-center">Perihal</th>
                                            <th class="text-center" style="width:130px;">Nominal</th>
                                            <th class="text-center" style="width:100px">Status</th>
                                            <th class="text-center" style="width:120px">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                                                    <tr>
                                            <td class="text-center">1</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Surat" data-placement="right">09 Jan 2018</span>
                                                </div>
                                                <div>
                                                    <span class="label label-danger square" data-toggle="tooltip" data-original-title="Tgl. Diterima" data-placement="right">10 Jan 2018</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class="text-danger bold">2242425</span> </td>
                                            <td class="text-left"> Biro Umum </td>
                                            <td class="text-left"> Testing </td>
                                            <td class="text-right"> <span class="text-danger bold">2.000.000</span> </td>
                                            <td class="text-center"> <label style="width:100%" class="label label-primary square">available</label>  </td>
                                            <td class="text-center">
                                                                                                                                <a class="btn btn-sm btn-warning" href="http://e-office.bnpb.go.id/siratna/eoffice/kpa-surat-masuk/edit?id=00MDAwMDBF"><i class="fa fa-edit"></i></a>
                                                                                                                                <a class="btn btn-danger btn-sm del" href="javascript:void(0)" data-url="http://e-office.bnpb.go.id/siratna/eoffice/kpa-surat-masuk/delete" data-value="00000E" data-text="Anda yakin akan menghapus Surat Masuk dengan nomor surat: 2242425 ?"><i class="fa fa-trash"></i></a>
                                                                                                                            </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">2</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Surat" data-placement="right">09 Jan 2018</span>
                                                </div>
                                                <div>
                                                    <span class="label label-danger square" data-toggle="tooltip" data-original-title="Tgl. Diterima" data-placement="right">10 Jan 2018</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class="text-danger bold">24242135</span> </td>
                                            <td class="text-left"> Biro Umum </td>
                                            <td class="text-left"> Testing </td>
                                            <td class="text-right"> <span class="text-danger bold">1.200.000</span> </td>
                                            <td class="text-center"> <label style="width:100%" class="label label-danger square">used</label>  </td>
                                            <td class="text-center">
                                                                                        <span data-toggle="tooltip" data-original-title="Sudah dimasukan ke dalam disposisi" data-placement="left">
                                                <a class="btn btn-sm btn-default" href="#" disabled=""><i class="fa fa-info"></i></a>
                                                </span>
                                                                                    </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">3</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Surat" data-placement="right">20 Des 2017</span>
                                                </div>
                                                <div>
                                                    <span class="label label-danger square" data-toggle="tooltip" data-original-title="Tgl. Diterima" data-placement="right">25 Des 2017</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class="text-danger bold">12345678901</span> </td>
                                            <td class="text-left"> Biro Umum </td>
                                            <td class="text-left"> rwerwefsdgsga </td>
                                            <td class="text-right"> <span class="text-danger bold">1.200.000</span> </td>
                                            <td class="text-center"> <label style="width:100%" class="label label-danger square">used</label>  </td>
                                            <td class="text-center">
                                                                                        <span data-toggle="tooltip" data-original-title="Sudah dimasukan ke dalam disposisi" data-placement="left">
                                                <a class="btn btn-sm btn-default" href="#" disabled=""><i class="fa fa-info"></i></a>
                                                </span>
                                                                                    </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">4</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Surat" data-placement="right">20 Des 2017</span>
                                                </div>
                                                <div>
                                                    <span class="label label-danger square" data-toggle="tooltip" data-original-title="Tgl. Diterima" data-placement="right">18 Des 2017</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class="text-danger bold">1234567890</span> </td>
                                            <td class="text-left"> Biro Umum </td>
                                            <td class="text-left"> Testing saja lagi </td>
                                            <td class="text-right"> <span class="text-danger bold">123.000.000</span> </td>
                                            <td class="text-center"> <label style="width:100%" class="label label-danger square">used</label>  </td>
                                            <td class="text-center">
                                                                                        <span data-toggle="tooltip" data-original-title="Sudah dimasukan ke dalam disposisi" data-placement="left">
                                                <a class="btn btn-sm btn-default" href="#" disabled=""><i class="fa fa-info"></i></a>
                                                </span>
                                                                                    </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">5</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Surat" data-placement="right">04 Des 2017</span>
                                                </div>
                                                <div>
                                                    <span class="label label-danger square" data-toggle="tooltip" data-original-title="Tgl. Diterima" data-placement="right">05 Des 2017</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class="text-danger bold">12345676T6</span> </td>
                                            <td class="text-left"> Biro Umum </td>
                                            <td class="text-left"> Testing </td>
                                            <td class="text-right"> <span class="text-danger bold">1.000.000</span> </td>
                                            <td class="text-center"> <label style="width:100%" class="label label-danger square">used</label>  </td>
                                            <td class="text-center">
                                                                                        <span data-toggle="tooltip" data-original-title="Sudah dimasukan ke dalam disposisi" data-placement="left">
                                                <a class="btn btn-sm btn-default" href="#" disabled=""><i class="fa fa-info"></i></a>
                                                </span>
                                                                                    </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">6</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Surat" data-placement="right">01 Des 2017</span>
                                                </div>
                                                <div>
                                                    <span class="label label-danger square" data-toggle="tooltip" data-original-title="Tgl. Diterima" data-placement="right">01 Des 2017</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class="text-danger bold">1213232424</span> </td>
                                            <td class="text-left"> Biro Umum </td>
                                            <td class="text-left"> Testikng </td>
                                            <td class="text-right"> <span class="text-danger bold">2.500.000</span> </td>
                                            <td class="text-center"> <label style="width:100%" class="label label-danger square">used</label>  </td>
                                            <td class="text-center">
                                                                                        <span data-toggle="tooltip" data-original-title="Sudah dimasukan ke dalam disposisi" data-placement="left">
                                                <a class="btn btn-sm btn-default" href="#" disabled=""><i class="fa fa-info"></i></a>
                                                </span>
                                                                                    </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">7</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Surat" data-placement="right">15 Nov 2017</span>
                                                </div>
                                                <div>
                                                    <span class="label label-danger square" data-toggle="tooltip" data-original-title="Tgl. Diterima" data-placement="right">22 Nov 2017</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class="text-danger bold">212/32123/1</span> </td>
                                            <td class="text-left"> Biro Umum </td>
                                            <td class="text-left"> Testing saja </td>
                                            <td class="text-right"> <span class="text-danger bold">12.000.000</span> </td>
                                            <td class="text-center"> <label style="width:100%" class="label label-danger square">used</label>  </td>
                                            <td class="text-center">
                                                                                        <span data-toggle="tooltip" data-original-title="Sudah dimasukan ke dalam disposisi" data-placement="left">
                                                <a class="btn btn-sm btn-default" href="#" disabled=""><i class="fa fa-info"></i></a>
                                                </span>
                                                                                    </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">8</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Surat" data-placement="right">08 Nov 2017</span>
                                                </div>
                                                <div>
                                                    <span class="label label-danger square" data-toggle="tooltip" data-original-title="Tgl. Diterima" data-placement="right">22 Nov 2017</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class="text-danger bold">23232.121213</span> </td>
                                            <td class="text-left"> TU Persuratan Biro Umum </td>
                                            <td class="text-left"> Testing lagi1 </td>
                                            <td class="text-right"> <span class="text-danger bold">15.000.001</span> </td>
                                            <td class="text-center"> <label style="width:100%" class="label label-danger square">used</label>  </td>
                                            <td class="text-center">
                                                                                        <span data-toggle="tooltip" data-original-title="Sudah dimasukan ke dalam disposisi" data-placement="left">
                                                <a class="btn btn-sm btn-default" href="#" disabled=""><i class="fa fa-info"></i></a>
                                                </span>
                                                                                    </td>
                                        </tr>
                                                                    <tr>
                                            <td class="text-center">9</td>
                                            <td class="text-center">
                                                <div>
                                                    <span class="label label-primary square" data-toggle="tooltip" data-original-title="Tgl. Surat" data-placement="right">14 Nov 2017</span>
                                                </div>
                                                <div>
                                                    <span class="label label-danger square" data-toggle="tooltip" data-original-title="Tgl. Diterima" data-placement="right">21 Nov 2017</span>
                                                </div>
                                            </td>
                                            <td class="text-center"> <span class="text-danger bold">232/334/2018</span> </td>
                                            <td class="text-left"> Biro Umum </td>
                                            <td class="text-left"> Testing Testing Testing Testing Testing Testing Testing Testing Testing </td>
                                            <td class="text-right"> <span class="text-danger bold">150.000.000</span> </td>
                                            <td class="text-center"> <label style="width:100%" class="label label-danger square">used</label>  </td>
                                            <td class="text-center">
                                                                                        <span data-toggle="tooltip" data-original-title="Sudah dimasukan ke dalam disposisi" data-placement="left">
                                                <a class="btn btn-sm btn-default" href="#" disabled=""><i class="fa fa-info"></i></a>
                                                </span>
                                                                                    </td>
                                        </tr>
                                                                </tbody>
                                </table>
                            </div>


                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-6" style="padding-left: 15px;">
                                        Total Data : <strong>9</strong>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="text-right">
                                                                            </div>
                                    </div>
                                </div>
                            </div><!-- /.panel-body -->
                        </div>




                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

@include('layouts.js-page')

<div id="page">

<div class="row" style="margin-top:40px;">
    <div class="col-lg-12 grid-margin d-flex flex-column">
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb bg-primary">
                    <li class="breadcrumb-item"><a href="{{ url('to/suratmasuk-daftar') }}">KPA</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><b>Tambah</b></li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Tambah Data Surat Masuk KPA</h4>


                        <div class="panel panel-info panel-square">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    Tambah KPA Surat Masuk
                                </h3>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" method="POST" action="" autocomplete="OFF">
                                    <div class="row">
                                        <div class="col-md-6">


                                            <div class="form-group ">
                                                <label class="col-md-4 control-label">No. Surat</label>
                                                <div class="col-md-8" style="padding-right:15px;">
                                                    <input type="text" value="" class="form-control input-sm" name="nomor_surat" placeholder="">
                                                                                            </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="col-md-4 control-label">Tgl. Surat </label>
                                                <div class="col-md-8" style="padding-right:15px;">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm hasDatepicker" value="" name="tanggal_surat" id="dp1544063780296">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="col-md-4 control-label">Tgl. Diterima </label>
                                                <div class="col-md-8" style="padding-right:15px;">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm hasDatepicker" value="" name="tanggal_diterima" id="dp1544063780297">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="col-md-4 control-label">Perihal</label>
                                                <div class="col-md-8" style="padding-right:15px;">
                                                    <textarea class="form-control input-sm" rows="3" name="perihal" placeholder=""></textarea>
                                                                                            </div>
                                            </div>




                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group ">
                                                <label class="col-md-4 control-label">Nominal</label>
                                                <div class="col-md-8" style="padding-right:15px;">
                                                    <div class="input-group">
                                                        <input type="text" name="nominal" value="" class="form-control input-sm" placeholder="Masukan nominal">
                                                        <span class="input-group-addon"><i class="fa fa-money fa-fw"></i></span>
                                                    </div>
                                                                                            </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="col-md-4 control-label">Unit Kerja</label>
                                                <div class="col-md-8" style="padding-right:15px;">
                                                        <select class="select-chosen form-control input-sm referal" name="unit_kerja" data-placeholder="Pilih Unit Kerja" style="display: none;">
                                                                                                                <option value="00000F">Biro Umum</option>
                                                                                                                <option value="00000S">TU Persuratan Biro Umum</option>
                                                                                                            </select><div class="chosen-container chosen-container-single" style="width: NaN;" title=""><a class="chosen-single" tabindex="-1"><span>Biro Umum</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off"></div><ul class="chosen-results"></ul></div></div>
                                                                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr style="margin:10px 0;">
                                    <div class="form-group">
                                        <div class="col-md-offset-8 col-md-4" style="padding-right:15px;">
                                            <div class="row" style="margin-left:-10px;">
                                                <div class="col-xs-6" style="padding-left:10px;">
                                                    <a href="http://e-office.bnpb.go.id/siratna/eoffice/kpa-surat-masuk" class="btn btn-default btn-block btn-square">
                                                        <i class="fa fa-undo"></i> Batal
                                                    </a>
                                                </div>
                                                <div class="col-xs-6" style="padding-left:10px;">
                                                    <button class="btn btn-primary btn-block btn-square" type="submit" name="process" value="Add">
                                                        <i class="fa fa-check"></i> Simpan Data
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div><!-- /.panel-body -->
                        </div>

                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

@include('layouts.js-page')

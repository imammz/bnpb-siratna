<style>
    .table-border, .table-border.td, .table-border.th, .table-border.tr {
        border: 1px solid black;
    }
    .center {
        text-align: center;
    }

    .border  {
        border-collapse: collapse;
        border: 1px solid black;
    }
    .row-border {
        border-collapse: collapse;
        border: 1px solid black;
        border-top: none;

    }

    .row-border-non  {
        border: 1px solid black;
        border-top: none;

    }

    .column-border  {
        border-collapse: collapse;
        border: 1px solid black; border-top:none; border-bottom: none; border-right: none;"
    }

    body {
        font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
        font-size: smaller;
    }

    td {
        padding-left: 2px;
    }

    .text-center {
        text-align: center;
    }

</style>

<body>

<table width="100%" border="0" style="margin-bottom: -20px;">
    <tr>
        <td width="60%" style="text-align: left; vertical-align: top;">
            <img src="{{ asset('qrcode/'.$suratmasuk->id.'.png') }}" width="90"/>
        </td>
        <td width="40%" style="text-align: left; vertical-align: top;">

            <table class="table-border" width="100%" style="text-align: center;"  >
                <tr><td> KLASIFIKASI </td></tr>
                <tr style="background-color: #a90707; color: white;"><td>  {{ $suratmasuk->klasifikasi }} </td></tr>
            </table>
        </td>
    </tr>
</table>
<div class="center" style="margin-top: 1px;">
    <h3> BADAN NASIONAL PENANGGULANGAN BENCANA <br/>
        LEMBAR DISPOSISI
    </h3>
</div>

<table class="border"   width="100%">
    <tr>
        <td width="20%" style="padding-left: 10px">
            Peringatan
        </td>
        <td width="1%"> : </td>
        <td style="font-size: xx-small;"> <ol>
                <li> <i> Agar dijaga kerahasiaan surat ini; </i> </li>
                <li> <i> Dilarang memisahkan sehelaipun surat atau dokumen dari berkas ini. </i> </li>
                <li> <i> Jika sudah selesai diproses, agar segera dikembalikan ke Bagian Tata Usaha dan Kepegawaian. </i> </li>
            </ol> </td>
    </tr>
    </table>
    <table class="row-border"   width="100%">
    <tr>
        <td width="20%" style="padding-left: 10px">
            Instruksi
        </td>
        <td width="1%"> : </td>
        <td style="font-size: xx-small;"> <ol>
                <li> <i> Instruksi dan arahan dalam lembar Disposisi ini harus diselenggarakan sesuai dengan Peraturan Perundang-undangan </i> </li>
                <li> <i> Para pejabat - Staf yang menyelenggarakan Disposisi ini harus juga memperhatikan azas-azas efisien dan efektif. </i> </li>
                <li> <i> Surat usulan yang diajukan kepada kepala dan Pimpinan harus sudah mempertimbangkan butir 1 dan 2 diatas. </i> </li>
            </ol> </td>
    </tr>
    </table>
    <table class="row-border"   width="100%">
        <tr>
            <td width="20%" style="padding-left: 10px">
                Pengirim
            </td>
            <td width="1%"> : </td>
            <td> {{$suratmasuk->pengirim}} </td>
        </tr>
    </table>
    <table class="row-border"   width="100%">
        <tr>
            <td width="20%" style="padding-left: 10px">
                Nomor Surat
            </td>
            <td width="1%"> : </td>
            <td width="29%"> {{ $suratmasuk->pengirim_nomor_surat }} </td>
            <td width="20%" class="column-border" style="padding-left: 10px">
                Tgl. Diterima
            </td>
            <td width="1%"> : </td>
            <td width="29%"> {{ \Carbon\Carbon::parse($suratmasuk->tanggal_diterima)->format('d-m-Y') }} </td>
        </tr>
    </table>
    <table class="row-border"   width="100%">
        <tr>
            <td width="20%" style="padding-left: 10px">
               Tgl. Surat
            </td>
            <td width="1%"> : </td>
            <td width="29%"> {{ \Carbon\Carbon::parse($suratmasuk->tanggal_surat_masuk)->format('d-m-Y') }} </td>
            <td width="20%" class="column-border" style="padding-left: 10px">
                Nomor Agenda
            </td>
            <td width="1%"> : </td>
            <td width="29%"> {{ $suratmasuk->nomor_agenda }} </td>
        </tr>
    </table>
    
    <table class="row-border"   width="100%">
        <tr>
            <td width="20%" style="padding-left: 10px">
                Lampiran
            </td>
            <td width="1%"> : </td>
            <td width="29%"> {{ $suratmasuk->keterangan_lampiran }} </td>
            <td width="20%" class="column-border" style="padding-left: 10px">
                Retro
            </td>
            <td width="1%"> : </td>
            <td width="29%"> </td>
        </tr>
    </table>

<table class="row-border-non"  width="100%" style="height: 50px; vertical-align: top;">
    <tr>
        <td width="20%" style="vertical-align: top;">
            Perihal
        </td>
        <td width="1%" style="vertical-align: top;"> : </td>
        <td style="vertical-align: top;">  {{ $suratmasuk->perihal }} </td>
    </tr>
</table>

<table class="border"   width="100%" style="border-collapse: collapse; font-size: smaller">
    <tr>
        <td >

            <table width="100%" style="height: 25px">
                <tr>
                    <td colspan="3"> <b>Diteruskan Kepada : </b> </td>

                    <td colspan="3"> <b>Arah Lanjut : </b> </td>

                </tr>
                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Sestama </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Diterima /Disetujui </td>

                </tr>
                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Irtama </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Diproses Sesuai Ketentuan Perundangan </td>

                </tr>

                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Deputi Bidang Sistem dan Strategi </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Proses Lebih Lanjut / Didukung / Dibantu </td>

                </tr>

                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Deputi Bidang Pencegahan </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Bicarakan Dengan Saya </td>

                </tr>

                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Deputi Bidang Penanganan Darurat </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Saya Hadir, Siapkan Bahan</td>

                </tr>

                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Deputi Bidang Rehabilitasi dan Rekonstruksi </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Wakili dan Laporkan                    </td>

                </tr>

                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Deputi Bidang Logistik dan Peralatan </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Buat Tanggapan dan Saran </td>

                </tr>

                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Kapusdatin dan Komunikasi Bencana </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Pelajari / Kaji dan laporkan ke Saya </td>

                </tr>

                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Kapusdiklat PB </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Edarkan ke Unit Terkait </td>

                </tr>

                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Karo Perencanaan </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Agendakan Waktunya </td>

                </tr>

                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Karo Keuangan </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Ikuti Perkembangannya </td>

                </tr>

                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Karo Hukum, Organisasi, Ks dan Humas </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Untuk Menjadi Perhatian </td>

                </tr>

                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Karo SDM dan Umum </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Pantau </td>

                </tr>


                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Kapusdalops </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Ingatkan Saya </td>

                </tr>

                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Yth. Tenaga Ahli </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Ditolak </td>

                </tr>
                <tr>
                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%">  </td>

                    <td width="2%"> </td>
                    <td width="4%" style="border: 1px solid black;">
                    </td>
                    <td width="44%"> Simpan / Arsipkan </td>

                </tr>
             
            </table>
        </td>
    </tr>
</table>
<table class="row-border" width="100%" style="text-align: center; border-collapse: collapse;">
    <tr><td> CATATAN / DISPOSISI KEPALA </td></tr>
</table>
<table class="row-border" width="100%" style="text-align: center; border-collapse: collapse;">
    <tr><td style="height: 130px;">  </td></tr>
</table>
<table class="row-border" width="100%" style="text-align: center; border-collapse: collapse;">
    <tr><td> Disposisi / Catatan </td>
    </tr>
</table>
<table border="1" class="row-border" width="100%" style="text-align: center; border-collapse: collapse;">
    <tr><td style="height: 175px; vertical-align: top; font-size: smaller" width="50%">  
        <b><u>SESTAMA / DEPUTI</u></b>   
    </td><td style="height: 175px; vertical-align: top; font-size: smaller" width="50%">  
        <b><u>KEPALA BIRO / DIREKTUR</u></b>   
        </td></tr>
</table>

</body>

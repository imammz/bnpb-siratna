<style>
    .table-border, .table-border.td, .table-border.th, .table-border.tr {
        border: 1px solid black;
    }
    .center {
        text-align: center;
    }

    .border  {
        border-collapse: collapse;
        border: 1px solid black;
    }
    .row-border {
        border-collapse: collapse;
        border: 1px solid black;
        border-top: none;

    }

    .row-border-non  {
        border: 1px solid black;
        border-top: none;

    }

  .column-border  {
      border-collapse: collapse;
      border: 1px solid black; border-top:none; border-bottom: none; border-right: none;"
    }

    body {
        font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
        font-size: smaller;
    }

    td {
        padding-left: 2px;
    }

    .text-center {
        text-align: center;
    }

</style>

<body>

<table width="100%" border="0" style="margin-bottom: -20px;">
    <tr>
        <td width="60%" style="text-align: left; vertical-align: top;">
            <img src="{{ asset('qrcode/'.$suratmasuk->id.'.png') }}" width="90"/>
        </td>
        <td width="40%" style="text-align: left; vertical-align: top;">

                <table class="table-border" width="100%" style="text-align: center;"  >
                    <tr><td> KLASIFIKASI </td></tr>
                    <tr style="background-color: #a90707; color: white;"><td>  {{ $suratmasuk->klasifikasi }} </td></tr>
                </table>
        </td>
    </tr>
</table>
<div class="center" style="margin-top: 1px;">
    <h3> BADAN NASIONAL PENANGGULANGAN BENCANA <br/>
        LEMBAR DISPOSISI
    </h3>
 </div>

<table class="border"   width="100%">
<tr>
    <td width="20%" style="padding-left: 10px">
        Peringatan
    </td>
    <td width="1%"> : </td>
    <td style="font-size: xx-small;"> <ol>
            <li> <i> Agar dijaga kerahasiaan surat ini; </i> </li>
            <li> <i> Dilarang memisahkan sehelaipun surat atau dokumen dari berkas ini. </i> </li>
            <li> <i> Jika sudah selesai diproses, agar segera dikembalikan ke Bagian Tata Usaha dan Kepegawaian. </i> </li>
        </ol> </td>
</tr>
</table>
<table class="row-border"   width="100%">
<tr>
    <td width="20%" style="padding-left: 10px">
        Instruksi
    </td>
    <td width="1%"> : </td>
    <td style="font-size: xx-small;"> <ol>
            <li> <i> Instruksi dan arahan dalam lembar Disposisi ini harus diselenggarakan sesuai dengan Peraturan Perundang-undangan </i> </li>
            <li> <i> Para pejabat - Staf yang menyelenggarakan Disposisi ini harus juga memperhatikan azas-azas efisien dan efektif. </i> </li>
            <li> <i> Surat usulan yang diajukan kepada kepala dan Pimpinan harus sudah mempertimbangkan butir 1 dan 2 diatas. </i> </li>
        </ol> </td>
</tr>
</table>
<table class="row-border"   width="100%">
    <tr>
        <td width="20%" style="padding-left: 10px">
            Pengirim
        </td>
        <td width="1%"> : </td>
        <td> {{$suratmasuk->pengirim}} </td>
    </tr>
</table>
<table class="row-border"   width="100%">
    <tr>
        <td width="20%" style="padding-left: 10px">
            Nomor Surat
        </td>
        <td width="1%"> : </td>
        <td width="29%"> {{ $suratmasuk->pengirim_nomor_surat }} </td>
        <td width="20%" class="column-border" style="padding-left: 10px">
            Tgl. Diterima
        </td>
        <td width="1%"> : </td>
        <td width="29%"> {{ \Carbon\Carbon::parse($suratmasuk->tanggal_diterima)->format('d-m-Y') }} </td>
    </tr>
</table>
<table class="row-border"   width="100%">
    <tr>
        <td width="20%" style="padding-left: 10px">
           Tgl. Surat
        </td>
        <td width="1%"> : </td>
        <td width="29%"> {{ \Carbon\Carbon::parse($suratmasuk->tanggal_surat_masuk)->format('d-m-Y') }} </td>
        <td width="20%" class="column-border" style="padding-left: 10px">
            Nomor Agenda
        </td>
        <td width="1%"> : </td>
        <td width="29%"> {{ $suratmasuk->nomor_agenda }} </td>
    </tr>
</table>

<table class="row-border"   width="100%">
    <tr>
        <td width="20%" style="padding-left: 10px">
            Lampiran
        </td>
        <td width="1%"> : </td>
        <td width="29%"> {{ $suratmasuk->keterangan_lampiran }} </td>
        <td width="20%" class="column-border" style="padding-left: 10px">
            Retro
        </td>
        <td width="1%"> : </td>
        <td width="29%"> </td>
    </tr>
</table>

<table class="row-border-non"  width="100%" style="height: 100px; vertical-align: top;">
    <tr>
        <td width="20%" style="vertical-align: top; padding-left: 10px;">
            Perihal
        </td>
        <td width="1%" style="vertical-align: top;"> : </td>
        <td style="vertical-align: top;">  {{ $suratmasuk->perihal }} </td>
    </tr>
</table>

<br/>

<table class="border"   width="100%">
    <tr>
        <td >
          <b>  Catatan / Intruksi </b>
        </td>
    </tr>
</table>
<table class="border" border="1" width="100%" >

    <tr style="text-align: center">
        <th> # </th><th> Tanggal </th> <th> Dari </th> <th> Kepada </th><th> Pesan</th>
    </tr>

    @if(count($suratmasuk->logDisposisi) > 0 )
        @php
            $no =1;
        @endphp
        @foreach($suratmasuk->logDisposisi as $row)
            @php
                $data = json_decode($row->data); $data->catatan = str_replace('&lt;br/&gt;','<br/>',$data->catatan);
                $data->kepada = str_replace('&lt;br/&gt;','<br/>',$data->kepada);
            @endphp
            <tr>
                <td>{{ $no }}</td>
                <td> {{ \Carbon\Carbon::parse($row->created_at)->format('d-m-Y') }}
                </td>
                <td>
                                                                                <span class="badge badge-success">
                                                                                              {{ $row->dari_nama_unit_kerja }} </span>
                    <br/>
                    <span
                        class="badge badge-secondary">
                                                                                          {{ $row->created_by_nama }}
                                                                                          </span>
                </td>
                <td>
                    {!! str_replace('&lt;br/&gt;','<br/>',$data->kepada) !!}
                </td>
                <td>
                    {!! str_replace('&lt;br/&gt;','<br/>',$data->catatan) !!}

                </td>
            </tr>

            @php $no++;
            @endphp @endforeach @else

        <tr>
            <td colspan="5" class="text-center"
                style="height:70px;">Belum ada data.
            </td>
        </tr>

    @endif

</table>
</body>

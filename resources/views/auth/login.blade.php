<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('ui/vendors/iconfonts/mdi/font/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('ui/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('ui/vendors/css/vendor.bundle.addons.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('ui/css/vertical-layout-light/style.css')}}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{asset('ui/images/favicon.png')}}" />
    <style>
        input[type=checkbox]
        {
          /* Double-sized Checkboxes */
          -ms-transform: scale(2); /* IE */
          -moz-transform: scale(2); /* FF */
          -webkit-transform: scale(2); /* Safari and Chrome */
          -o-transform: scale(2); /* Opera */
          transform: scale(2);
          padding: 10px;
        }
    </style>
</head>

<body class="sidebar-fixed">
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
                <div class="row flex-grow">
                    <div class="col-lg-6 d-flex align-items-center justify-content-center">
                        <div class="auth-form-transparent text-left p-3">
                            <div class="brand-logo" style="text-align:center;">
                                <img src="{{asset('ui/images/logo.png')}}" alt="logo">
                            </div>
                            <h6 style="text-align:center;">APLIKASI SISTEM PERSURATAN DAN NASKAH DINAS (SIRATNA)</h4>
                                <h2 style="text-align:center;" class="font-weight-light">LOGIN</h2>
                                <form class="pt-3" action="{{ route('login') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInputEmail">NIP atau Email</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend bg-transparent">
                                                <span class="input-group-text bg-transparent border-right-0">
                        <i class="mdi mdi-account-outline text-primary"></i>
                      </span>
                                            </div>
                                            <input id="identity" type="identity" class="form-control" name="identity" value="{{ old('identity') }}" autofocus>                                            @if ($errors->has('identity'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('identity') }}</strong>
                                    </span> @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword">Password</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend bg-transparent">
                                                <span class="input-group-text bg-transparent border-right-0">
                        <i class="mdi mdi-lock-outline text-primary"></i>
                      </span>
                                            </div>
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-lg border-left-0"
                                                id="exampleInputPassword" name="password" required>                                            @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span> @endif
                                        </div>
                                    </div>
                                    <div class="my-2 d-flex justify-content-between align-items-center">
                                        <div class="form-check">
                                            <label class="form-check-label text-muted">
                      <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        Keep me signed in
                    </label>
                                        </div>
                                        <a href="{{ route('password.request') }}" class="auth-link text-black">Lupa password?</a>
                                    </div>
                                    <div class="my-3">
                                        <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                    </button>
                                    </div>

                                    <div class="text-center mt-4 font-weight-light">
                                        <a href="{{ route('register') }}" class="text-primary">Register</a>
                                    </div>
                                </form>
                        </div>
                    </div>
                    <div class="col-lg-6 login-half-bg d-flex flex-row">
                        <p class="text-black font-weight-medium text-center flex-grow align-self-end">Copyright &copy; 2019 Badan Nasional Penanggulangan Bencana.</p>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('ui/vendors/js/vendor.bundle.base.js')}}"></script>
    <script src="{{asset('ui/vendors/js/vendor.bundle.addons.js')}}"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="{{asset('ui/js/off-canvas.js')}}"></script>
    <script src="{{asset('ui/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('ui/js/template.js')}}"></script>
    <script src="{{asset('ui/js/settings.js')}}"></script>
    <script src="{{asset('ui/js/todolist.js')}}"></script>
    <!-- endinject -->
</body>

</html>

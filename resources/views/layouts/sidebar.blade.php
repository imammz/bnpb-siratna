<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="#" style="bottom-margin: 30px;">
          <i class="mdi mdi-view-quilt menu-icon"></i>
          <span class="menu-title">--</span>

        </a>
      </li>

      @php
      $menu = Auth::user()->role->menus()->where('id_parent_menu',0)->orderBy('urut')->get();
      @endphp

      @foreach ($menu as $row)
      @php
          $sub = substr($row->link,0,1);
      @endphp
      <li class="nav-item">
            @php
              $link = url("to/$row->link");
            @endphp

            @if($sub=='#')
              @php
                $link = $row->link;
              @endphp
            @endif

            <a class="nav-link"  href="{{ $link }}"
                    @if($sub=='#')
                aria-expanded="false" aria-controls="{{ str_replace('#','',$row->link) }}" data-toggle="collapse"
                @endif
                 >
              <i class="mdi {{ $row->icon }} menu-icon"></i>
              <span class="menu-title">{{ $row->nama_menu }}</span>
              @if($sub=='#')
              <i class="menu-arrow"></i>
              @endif
            </a>
            @php
            $submenu = Auth::user()->role->menus()->where('id_parent_menu',$row->id)->orderBy('urut')->get();
            @endphp
            @if($sub=='#')
                 <div class="collapse" id="{{ str_replace('#','',$row->link) }}">
                    <ul class="nav flex-column sub-menu">
                         @foreach($submenu as $row_sub)
                      <li class="nav-item"> <a class="nav-link" href="{{ url("to/$row_sub->link") }}">{{ $row_sub->nama_menu }}</a></li>
                         @endforeach
                    </ul>
                  </div>
            @endif
        </li>

      @endforeach










    </ul>
  </nav>

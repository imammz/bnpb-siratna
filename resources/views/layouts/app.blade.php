<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>:: Applikasi SIRATNA ::</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('ui/vendors/iconfonts/mdi/font/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('ui/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{ asset('ui/vendors/css/vendor.bundle.addons.css') }}">
    <link rel="stylesheet" href="{{asset('ui/vendors/iconfonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('lib/datatables.css') }}">
    <!-- <link rel="stylesheet" href="iziModal/css/iziModal.min.css"> -->


    <link href="{{ asset('lib/select2.min.css') }}" rel="stylesheet" />




    <!-- endinject -->
    <!-- plugin css for this page -->
    @include('layouts.css') @yield('part-css')
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('ui/css/vertical-layout-light/style.css')}}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{asset('ui/images/favicon.png')}}" />
    

    <style>
        input[type=checkbox]
        {
          /* Double-sized Checkboxes */
          -ms-transform: scale(2); /* IE */
          -moz-transform: scale(2); /* FF */
          -webkit-transform: scale(2); /* Safari and Chrome */
          -o-transform: scale(2); /* Opera */
          transform: scale(2);
          padding: 10px;
        }
    </style>
   
</head>

<body class="sidebar-fixed sidebar-light sidebar-icon-only">
    <div class="container-scroller">
    @include('layouts.navbar')


        <!-- partial -->
    @include('layouts.sidebar')
        <!-- partial:../../partials/_sidebar.html -->


        <div class="modal fade" id="modal-url" tabindex="-1" role="dialog"
             aria-labelledby="ModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div style="width: 100%; padding-bottom: 10px; padding-left: 10px; padding-right: 10px;">
                        <hr/>
                        <div class="row" >

                            <div class="col-md-6" style="text-align: left;">
                            </div>
                            <div class="col-md-6" style="text-align: right;" >
                                <button type="button" class="btn btn-primary" data-dismiss="modal">
                                    TUTUP
                                </button>
                            </div>


                        </div>

                    </div>

                </div>
            </div>
        </div>

        <!-- partial -->
        <div class="main-panel">

            @yield('content')


            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2018 <a href="#"> | BNPB -  Badan Nasional Penanggulangan Bencana | </a></span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
    </div>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5dbc00e7e4c2fa4b6bd97c6e/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->

    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('ui/vendors/js/vendor.bundle.base.js')}}"></script>
    <script src="{{asset('ui/vendors/js/vendor.bundle.addons.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    @include('layouts.js')
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{asset('ui/js/off-canvas.js')}}"></script>
    <script src="{{asset('ui/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('ui/js/template.js')}}"></script>
    <script src="{{asset('ui/js/settings.js')}}"></script>
    <script src="{{asset('ui/js/todolist.js')}}"></script>
    <script src="{{ asset('ui/js/tooltips.js') }}"></script>
    <script src="{{ asset('ui/js/popover.js') }}"></script>
    <script src="{{ asset('ui/js/editorDemo.js') }}"></script>
    <!--
    <script src="{{ asset('ui/js/iCheck.js') }}"></script>
    -->

    <!-- DataTables -->
    <script src="{{ asset('lib/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('lib/select2.min.js') }}"></script>


    <!-- <script src="iziModal/js/iziModal.min.js" type="text/javascript"></script> -->



    <script>
        (function ($) {
            'use strict';
            $('#modal-url').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.modal-body').html('<iframe src="'+recipient+'" width="100%" height="600px"  frameBorder="0" scrolling="auto">Browser not compatible. </iframe >')
            })
        })(jQuery);


        @if(isset($url))
                $("#content").load("{{url($url)}}");
        @else
             $("#content").load("{{url('home-page')}}");
        @endif


    </script>



    <!-- endinject -->
    <!-- Custom js for this page-->
    @yield('part-js')
    <!-- End custom js for this page-->


    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
    <script>
        tinymce.init({
            selector:'textarea.description',
            width: 900,
            height: 300
        });
    </script>

</body>

</html>

<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="navbar-menu-wrapper d-flex align-items-stretch justify-content-between">
        <ul class="navbar-nav mr-lg-2 d-none d-lg-flex">
          <li class="nav-item nav-toggler-item">
            <button class="navbar-toggler align-self-center" type="button" data-toggle="minimize">
              <span class="mdi mdi-menu"></span>
            </button>
          </li>
          <li class="nav-item nav-search d-none d-lg-flex">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id="search">
                  <i class="mdi mdi-magnify"></i>
                </span>
              </div>
              <input type="text" class="form-control" placeholder="Cari Dokumen Surat" aria-label="search" aria-describedby="search">
            </div>
          </li>
           <li class="nav-item nav-settings d-none d-lg-flex">



          </li>
        </ul>
        <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">

        </div>
        <ul class="navbar-nav navbar-nav-right">

          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="mdi mdi-bell-outline mx-0"></i>
              <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              <a class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left">:: NOTIFIKASI ::
                </p>
              </a>

             @if(session('jml_surat_masuk') > 0)
              <a class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left">Anda Punya {{session('jml_surat_masuk')}} Notifikasi Surat Masuk Baru
                </p>
              </a>
              @endif

                 @if(session('jml_surat_masuk') > 0)
              @foreach(session('notif_surat_masuk') as $row)

              <div class="dropdown-divider"></div>
              <div class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-primary">
                    <i class="mdi mdi-file-document mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">

                  <h6 class="preview-subject font-weight-medium"><a href="{{url('to/suratmasuk-daftar-proses/'.$row->id)}}">{{$row->perihal}}</a></h6>
                  <p class="font-weight-light small-text mb-0" style="font-weight: bolder; color: #1b4b72;">
                    Dari : {{$row->pengirim}}  <br/>
                    Tanggal :  {{ \Carbon\Carbon::parse($row->tanggal_surat_masuk)->format('d-m-Y') }} <br/>
                    Nomor :  {{ $row->pengirim_nomor_surat }}   <br/>

                  </p>

                </div>

              </div>
              @endforeach
                     @endif

            </div>
          </li>
          <li class="nav-item nav-profile dropdown">
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
              <img src="{{asset('ui/images/favicon.png')}}" alt="profile"/>
              <span class="nav-profile-name">{{Auth::user()->nama}} ({{Auth::user()->pegawai->unit_kerja->nama_unit_kerja}}) </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
              <a class="dropdown-item">
                <i class="mdi mdi-settings text-primary"></i>
                Settings
              </a>
              <div class="dropdown-divider"></div>

               <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        <i class="mdi mdi-logout text-primary"></i>
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </div>
          </li>

          <li class="nav-item nav-toggler-item-right d-lg-none">
            <button class="navbar-toggler align-self-center" type="button" data-toggle="offcanvas">
              <span class="mdi mdi-menu"></span>
            </button>
          </li>


        </ul>
      </div>
    </nav>

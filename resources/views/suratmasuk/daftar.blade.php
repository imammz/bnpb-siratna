
<div id="page">


    <style>
        a {
            color: #0c0c0c;
        }

        a:hover {
            font-weight: bolder;
            text-decoration: none;
        }
    </style>
    <div class="row" style="margin-top:40px;">
        <div class="col-lg-12 grid-margin d-flex flex-column">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb bg-primary">
                        <li class="breadcrumb-item active"><a href="{{url('to/suratmasuk-daftar')}}" ><b>Surat
                                    Masuk</b></a></li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">


                            <div class="modal fade" id="modal-1" tabindex="-1" role="dialog"
                                 aria-labelledby="ModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                        </div>
                                        <div style="width: 100%; padding-bottom: 10px; padding-left: 10px; padding-right: 10px;">
                                            <hr/>
                                            <div class="row" >

                                                    <div class="col-md-6" style="text-align: left;">

                                                        @if(Auth::user()->id_role==3 and Auth::user()->pegawai->unit_kerja->eselon != 4)
                                                            <span data-toggle="tooltip" data-placement="left"
                                                                  title="Disposisikan Kepada Pejabat atau Staff Dibawah Anda">
                                    <a href="#" id="disposisi"
                                       class="btn btn-success">
                                                <i class="fa fa-share-square-o"></i> Disposisikan
                                        </a>
                                    </span>
                                                        @endif

                                                        @if(Auth::user()->id_role==3 and Auth::user()->pegawai->id_unit_kerja != '000001')
                                                            <span data-toggle="tooltip" data-placement="left"
                                                                  title="Diteruskan Kepada Pejabat Lain">
                                    <a href="#" id="diteruskan"
                                       class="btn btn-info">
                                                <i class="fa fa-share-square"></i> Diteruskan
                                        </a>
                                    </span>
                                                        @endif

                                                        @if(Auth::user()->pegawai->id_unit_kerja != '000001')
                                                            <span data-toggle="tooltip" data-placement="left"
                                                                  title="Beri Catatan & Laporan Terhadap Hasil Tindak Lanjut Dari Surat Ini">
                                    <a href="#" id="catatan"
                                       class="btn btn-facebook">
                                                <i class="fa fa-edit"></i> Beri Catatan & Laporan
                                        </a>
                                    </span>
                                                        @endif


                                                    </div>



                                                <div class="col-md-6" style="text-align: right;" >

                                                    <a href="#" id="detil"
                                                       class="btn btn-success">
                                                        <i class="fa fa-book"></i> BACA SURAT
                                                    </a>
                                                    <button type="button" class="btn btn-light" data-dismiss="modal">
                                                        TUTUP
                                                    </button>
                                                </div>


                                            </div>

                                        </div>

                                </div>
                            </div>
                            </div>


                            <h4 class="card-title">Data Surat Masuk</h4>


                            <div class="row">
                                <div class="col-12">
                                    <ul class="nav nav-tabs" role="tablist">

                                        <li class="nav-item">
                                            <a class="nav-link active show" id="home-tab" data-toggle="tab"
                                               href="#proses" role="tab" aria-controls="proses"
                                               aria-selected="true">
                                                <i class="mdi mdi-progress-check text-danger ml-2"></i>
                                                Sedang Proses
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#sudah"
                                               role="tab" aria-controls="sudah" aria-selected="false">
                                                <i class="mdi mdi-check-circle text-info ml-2"></i>
                                                Sudah Diproses
                                            </a>
                                        </li>

                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane fade active show" id="proses" role="tabpanel"
                                             aria-labelledby="home-tab-vertical">
                                            <div style="text-align: right">

                                                @if($flag=='sudah-dibaca')
                                                    <a href="{{url('to/suratmasuk-daftar')}}" >
                                                        <span class="badge badge-pill badge-primary"> Kembali Ke-Daftar Surat Masuk </span>
                                                    </a> @else
                                                    <a href="{{url('to/suratmasuk-daftar/sudah-dibaca')}}" >
                                                        <span class="badge badge-pill badge-primary"> Sudah Dibaca : {{ $jml_dibaca }} </span>
                                                    </a> @endif

                                                @if($flag=='belum-dibaca')
                                                    <a href="{{url('to/suratmasuk-daftar')}}" >
                                                        <span class="badge badge-pill badge-info">  Kembali Ke-Daftar Surat Masuk  </span>
                                                    </a> @else
                                                    <a href="{{url('to/suratmasuk-daftar/belum-dibaca')}}" >
                                                        <span class="badge badge-pill badge-info"> Belum Dibaca : {{ $jml_belum_dibaca }} </span>
                                                    </a> @endif

                                                @if($flag=='tandai')
                                                    <a href="{{url('to/suratmasuk-daftar')}}" > <span
                                                                class="badge badge-pill badge-danger">  Kembali Ke-Daftar Surat Masuk  </span>
                                                    </a>
                                                @else
                                                    <a href="{{url('to/suratmasuk-daftar/tandai')}}" > <span
                                                                class="badge badge-pill badge-danger">  Ditandai : {{ $jml_ditandai }}  </span>
                                                    </a>
                                                @endif

                                            </div>
                                            <br/>
                                            <table class="table table-hover table-bordered" id="daftarSuratMasuk">
                                                <thead>
                                                <tr style="text-align:center">
                                                    <th width="6%">No</th>
                                                    <th width="20%">Pengirim</th>
                                                    <th width="20%">Nomor Surat</th>
                                                    <th>Perihal</th>
                                                    <th width="15%">Tanggal Surat Masuk</th>
                                                    <th width="15%">Dibaca</th>
                                                    <th width="10%">Ditandai</th>
                                                </tr>
                                                </thead>
                                            </table>

                                        </div>
                                        <div class="tab-pane fade" id="sudah" role="tabpanel"
                                             aria-labelledby="profile-tab-vertical">


                                            <table width="100%" class="table table-hover table-bordered"
                                                   id="daftarSuratMasukSudah">
                                                <thead>
                                                <tr style="text-align:center">
                                                    <th width="6%">No</th>
                                                    <th width="20%">Pengirim</th>
                                                    <th width="20%">Nomor Surat</th>
                                                    <th>Perihal</th>
                                                    <th width="15%">Tanggal Surat Masuk</th>
                                                    <th width="10%">Ditandai</th>
                                                </tr>
                                                </thead>
                                            </table>


                                        </div>

                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>


            </div>
        </div>


        <!-- Link to open the modal -->


        @if(Auth::user()->id_role == 2 or Auth::user()->id_role == 5)
            <div id="settings-trigger" data-toggle="tooltip" data-placement="left" title="Tambah Data Surat Masuk">
                <a href="{{url('to/suratmasuk-tambah')}}" >
                    <i class="mdi mdi-plus-circle" style="padding: 100% 0 100% 0"></i>
                </a>
            </div>
        @endif


</div>



            <script>


                $(function () {


                    $('#daftarSuratMasuk').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: '{!! url('suratmasuk-daftar-data/'.$flag) !!}',
                        columns: [
                            {data: "DT_RowIndex", orderable: false, searchable: false},
                            {data: 'pengirim', name: 'pengirim'},
                            {data: 'pengirim_nomor_surat', name: 'pengirim_nomor_surat'},
                            {data: 'perihal', name: 'perihal'},
                            {data: 'tgl_surat', name: 'tgl_surat'},
                            {data: 'dibaca', name: 'dibaca'},
                            {data: 'marked', name: 'marked'},
                        ],
                        columnDefs: [
                            {
                                targets: 0,
                                className: 'dt-body-right'
                            },
                            {
                                targets: 1,
                                className: 'dt-body-left'
                            },
                            {
                                targets: 4,
                                className: 'dt-body-center'
                            },
                            {
                                targets: 5,
                                className: 'dt-body-center'
                            },
                            {
                                targets: 6,
                                className: 'dt-body-center'
                            },
                        ],
                    });


                    $('#daftarSuratMasukSudah').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: '{!! url('suratmasuk-daftar-data-sudah/'.$flag) !!}',
                        columns: [
                            {data: "DT_RowIndex", orderable: false, searchable: false},
                            {data: 'pengirim', name: 'pengirim'},
                            {data: 'pengirim_nomor_surat', name: 'pengirim_nomor_surat'},
                            {data: 'perihal', name: 'perihal'},
                            {data: 'tgl_surat', name: 'tgl_surat'},
                            {data: 'marked', name: 'marked'},
                        ],
                        columnDefs: [
                            {
                                targets: 0,
                                className: 'dt-body-right'
                            },
                            {
                                targets: 1,
                                className: 'dt-body-left'
                            },
                            {
                                targets: 4,
                                className: 'dt-body-center'
                            },
                            {
                                targets: 5,
                                className: 'dt-body-center'
                            },
                        ],
                    });

                });


            </script>

            <script>
                (function ($) {
                    'use strict';
                    $('#modal-1').on('show.bs.modal', function (event) {
                        var button = $(event.relatedTarget) // Button that triggered the modal
                        var recipient = button.data('whatever') // Extract info from data-* attributes
                        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                        var modal = $(this)
                        modal.find('.modal-body').html('<iframe src="{{url('suratmasuk-lampiran-images')}}/' + recipient + '" width="100%" height="600px"  frameBorder="0" scrolling="auto">Browser not compatible. </iframe >')
                        modal.find('#disposisi').attr("href",'{{ url('to/suratmasuk-disposisi/') }}/' + recipient + '#disposisi')
                        modal.find('#diteruskan').attr("href",'{{ url('to/suratmasuk-diteruskan/') }}/' + recipient + '#diteruskan')
                        modal.find('#catatan').attr("href",'{{ url('to/suratmasuk-catatan/') }}/' + recipient + '#catatan')
                        modal.find('#detil').attr("href",'{{ url('to/suratmasuk-daftar-proses/') }}/' + recipient + '')
                    })
                })(jQuery);
            </script>

    @include('layouts.js-page')

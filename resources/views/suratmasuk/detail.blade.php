<div id="page">
    <div class="row" style="margin-top:40px;">
        <div class="col-lg-12 grid-margin d-flex flex-column">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb bg-primary">
                        <li class="breadcrumb-item"><a href="{{ url('to/suratmasuk-daftar') }}">Surat Masuk</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><b>Detail</b></li>
                    </ol>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">

                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" id="1-tab" data-toggle="tab" href="#1" role="tab"
                               aria-controls="1" aria-selected="true">
                                <i class="mdi mdi-progress-check text-danger ml-2"></i>
                                Informasi Umum
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="2a-tab" data-toggle="tab" href="#2a" role="tab" aria-controls="2a"
                               aria-selected="false">
                                <i class="mdi mdi-check-circle text-info ml-2"></i>
                                <i>Lampiran File</i> Surat
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="2-tab" data-toggle="tab" href="#2" role="tab" aria-controls="2"
                               aria-selected="false">
                                <i class="mdi mdi-monitor text-danger ml-2"></i>
                                Monitoring Disposisi
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="1" role="tabpanel" aria-labelledby="1-tab-vertical">
                            <div class="card">
                                <div class="card-body">
                                    <div class="panel-group" id="faq-accordion-1">

                                        <div>

                                            <div>
                                                <div>

                                                    <div class="row">
                                                        <div class="col-lg-8">
                                                            <div class="panel panel-primary panel-square"
                                                                 style="margin-bottom:15px;">


                                                                <div class="panel-body profile-activity">
                                                                    <div class="table-responsive">
                                                                        <table
                                                                            class="table table-striped table-hover table-bordered responsive">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td style="width:35%"><b
                                                                                        class="text-info">Kepada : </b>
                                                                                </td>
                                                                                <td class="text-left"><b
                                                                                        class="text-info">
                                                                                        @if(isset($suratmasuk->kepada_unitkerja_nama))
                                                                                            {{ $suratmasuk->kepada_unitkerja_nama }}
                                                                                        @endif
                                                                                    </b></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width:35%"><b
                                                                                        class="text-info">No. Agenda
                                                                                        : </b></td>
                                                                                <td class="text-left"><b
                                                                                        class="text-info">
                                                                                        {{ $suratmasuk->nomor_agenda }}
                                                                                    </b></td>
                                                                            </tr>

                                                                            </tbody>
                                                                        </table>

                                                                    </div>

                                                                    <div class="table-responsive">

                                                                        <table
                                                                            class="table table-striped table-hover table-bordered responsive">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td style="width:35%"><b class="">Pengirim
                                                                                        : </b></td>
                                                                                <td class="text-left"><b class="">
                                                                                        {{ $suratmasuk->pengirim }}
                                                                                    </b></td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td style="width:35%"><b class="">Tgl.
                                                                                        Surat : </b></td>
                                                                                <td class="text-left"><b class="">
                                                                                        {{ \Carbon\Carbon::parse($suratmasuk->tanggal_surat_masuk)->format('d-m-Y') }}
                                                                                    </b></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width:35%"><b class="">No.
                                                                                        Surat : </b></td>
                                                                                <td class="text-left"><b class="">
                                                                                        {{ $suratmasuk->pengirim_nomor_surat }}
                                                                                    </b></td>
                                                                            </tr>

                                                                            </tbody>
                                                                        </table>
                                                                    </div>

                                                                    <div class="table-responsive">

                                                                        <table
                                                                            class="table table-striped table-hover table-bordered responsive">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td style="width:35%"><b
                                                                                        class="text-primary">Tgl.
                                                                                        Diterima : </b></td>
                                                                                <td class="text-left"><b
                                                                                        class="text-primary">
                                                                                        {{ \Carbon\Carbon::parse($suratmasuk->tanggal_diterima)->format('d-m-Y') }}
                                                                                    </b></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width:35%"><b
                                                                                        class="text-primary">Tgl.
                                                                                        Pelaksanaan : </b></td>
                                                                                <td class="text-left"><b
                                                                                        class="text-primary">
                                                                                        {{ \Carbon\Carbon::parse($suratmasuk->tanggal_pelaksanaan)->format('d-m-Y') }}
                                                                                    </b></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width:35%"><b
                                                                                        class="text-primary">Perihal
                                                                                        : </b></td>
                                                                                <td class="text-left"><b
                                                                                        class="text-primary">
                                                                                        {{ $suratmasuk->perihal }}
                                                                                    </b></td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="col-lg-12">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="panel panel-default panel-square"
                                                                             style="margin-bottom:15px;">
                                                                            <header class="panel-heading">
                                                                                <h5 class="text-facebook">Klasifikasi
                                                                                    Surat
                                                                                    Masuk</h5>
                                                                            </header>
                                                                            <div class="panel-body profile-activity">
                                                                                <div class="table-responsive">
                                                                                    <table
                                                                                        class="table table-striped table-hover table-bordered responsive"
                                                                                        style="margin-bottom:0px;">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td><b
                                                                                                    class="">Klasifikasi : </b>
                                                                                            </td>
                                                                                            <td class="text-left"><b
                                                                                                    class="text-danger">{{ $suratmasuk->klasifikasi }}</b>
                                                                                            </td>
                                                                                            <td><b
                                                                                                    class="">Ket.
                                                                                                    Lampiran : </b></td>
                                                                                            <td class="text-left"><b
                                                                                                    class="text-primary">{{ $suratmasuk->keterangan_lampiran }}</b>
                                                                                            </td>
                                                                                        </tr>




                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>



                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="panel panel-info panel-square panel-block-color"
                                                                 style="margin-bottom:15px;">
                                                                <header class="panel-heading">
                                                                    <h5 class="text-facebook">QR Code</h5>
                                                                </header>
                                                                <div>
                                                                    <div class="row" style="text-align: center;">
                                                                        <a href="#" id="show">
                                                                        <img src="{{ asset('qrcode\\'.$suratmasuk->id.'.png') }}" width="30%"/>
                                                                        </a>

                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div
                                                                class="panel panel-danger panel-square panel-block-color"
                                                                style="margin-bottom:15px;">
                                                                <header class="panel-heading">
                                                                    <h5 class="text-facebook">Lembar Disposisi</h5>
                                                                </header>
                                                                <div>
                                                                    <div class="row">
                                                                        <div class="col-md-12"
                                                                             style="text-align: center">
                                                                            <a class="btn btn-sm btn-danger"
                                                                               href="#"  data-toggle="modal"
                                                                               data-target="#modal-url" data-whatever="{{url('suratmasuk-lembar-disposisi/'.$suratmasuk->id)}}"  > <i class="fa fa-file-pdf-o"></i>
                                                                                Tampilkan</a>

                                                                            <a class="btn btn-sm btn-warning"
                                                                               href="#"  data-toggle="modal"
                                                                               data-target="#modal-url" data-whatever="{{url('suratmasuk-lembar-disposisi-manual/'.$suratmasuk->id)}}"> <i class="fa fa-file-pdf-o"></i>
                                                                                Manual</a>
                                                                        </div>


                                                                    </div>

                                                                    <div class="row">

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default panel-square"
                                                                 style="margin-bottom:15px;">
                                                                <header class="panel-heading">
                                                                    <h5 class="text-facebook">Retensi</h5>
                                                                </header>
                                                                <div class="table-responsive">
                                                                    <table
                                                                        class="table table-th-block table-hover table-bordered responsive">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="text-left"
                                                                                style="width: 150px; padding-left:15px;">
                                                                                <div><b> Active</b></div>
                                                                            </td>
                                                                            <td class="text-left"><strong
                                                                                    class="text-primary">{{ $suratmasuk->retensi_active }}
                                                                                    tahun</strong><br></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="text-left"
                                                                                style="width: 150px; padding-left:15px;">
                                                                                <div><b> Inactive </b></div>
                                                                            </td>
                                                                            <td class="text-left"><strong
                                                                                    class="text-primary">{{ $suratmasuk->retensi_inactive }}
                                                                                    tahun</strong><br></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="text-left"
                                                                                style="width: 150px; padding-left:15px;">
                                                                                <div><b> Keterangan </b>
                                                                                </div>
                                                                            </td>
                                                                            <td class="text-left"><strong
                                                                                    class="text-primary">{{ $suratmasuk->retensi_keterangan }}</strong><br>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                            <div class="panel panel-default panel-square"
                                                                 style="margin-bottom:15px;">
                                                                <header class="panel-heading">
                                                                    <h5 class="text-facebook">Dibuat Oleh
                                                                        :</h5>
                                                                </header>
                                                                <div class="table-responsive">
                                                                    <table
                                                                        class="table table-th-block table-hover table-bordered responsive">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="text-center"
                                                                                style="width: 150px;">
                                                                                <div> <span
                                                                                        style="width:100%"
                                                                                        class="label label-info square"
                                                                                        data-toggle="tooltip"
                                                                                        data-original-title="Tgl. Create"
                                                                                        data-placement="right">  {{ \Carbon\Carbon::parse($suratmasuk->created_at)->format('d-m-Y H:i') }} </span>
                                                                                </div>
                                                                            </td>
                                                                            <td class="text-left"><strong
                                                                                    class="text-primary">{{ $suratmasuk->created_by_nama }}</strong><br>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="col-lg-12">

                                                            <section class="panel panel-default panel-square"
                                                                     style="margin-bottom:15px;">
                                                                <header class="panel-heading">
                                                                    <h5 class="text-facebook">Instruksi / Isi Disposisi : </h5>
                                                                </header>
                                                                <div class="table-responsive">
                                                                    <table
                                                                        class="table table-th-block table-default table-striped table-bordered responsive">
                                                                        <thead>
                                                                        <tr>
                                                                            <th class="text-center" style="width:7%">#
                                                                            </th>
                                                                            <th class="text-center" style="width:12%">
                                                                                Tanggal
                                                                            </th>
                                                                            <th class="text-center" style="width:8%">
                                                                                Jenis
                                                                            </th>
                                                                            <th class="text-center" style="width:25%">
                                                                                Dari
                                                                            </th>
                                                                            <th class="text-center" style="width:30%">
                                                                                Kepada
                                                                            </th>
                                                                            <th class="text-center">Catatan</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>

                                                                        @if(count($suratmasuk->logDisposisi) > 0 )
                                                                            @php
                                                                                $no =1;
                                                                            @endphp
                                                                            @foreach($suratmasuk->logDisposisi as $row)
                                                                                @php
                                                                                    $data = json_decode($row->data); $data->catatan = str_replace('&lt;br/&gt;','<br/>',$data->catatan);
                                                                                    $data->kepada = str_replace('&lt;br/&gt;','<br/>',$data->kepada);
                                                                                @endphp
                                                                                <tr>
                                                                                    <td>{{ $no }}</td>
                                                                                    <td> <span class="badge badge-primary">
                                                                                        {{ \Carbon\Carbon::parse($row->created_at)->format('d-m-Y') }}
                                                                                        </span> <br/>
                                                                                        <span class="badge badge-secondary">
                                                                                        Jam : {{ \Carbon\Carbon::parse($row->created_at)->format('H:i') }}
                                                                                        </span>
                                                                                    </td>
                                                                                    <td> {{strtoupper($row->tipe)}} </td>
                                                                                    <td>
                                                                                <span class="badge badge-success">
                                                                                              {{ $row->dari_nama_unit_kerja }} </span>
                                                                                        <br/>
                                                                                        <span
                                                                                            class="badge badge-secondary">
                                                                                          {{ $row->created_by_nama }}
                                                                                          </span>
                                                                                    </td>
                                                                                    <td>
                                                                                        {!! str_replace('&lt;br/&gt;','<br/>',$data->kepada) !!}
                                                                                    </td>
                                                                                    <td>
                                                                                        {!! str_replace('&lt;br/&gt;','<br/>',$data->catatan) !!}

                                                                                    </td>
                                                                                </tr>

                                                                                @php $no++;
                                                                                @endphp @endforeach @else

                                                                            <tr>
                                                                                <td colspan="5" class="text-center"
                                                                                    style="height:70px;">Belum ada data.
                                                                                </td>
                                                                            </tr>

                                                                        @endif

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </section>

                                                            <section>
                                                                @if($suratmasuk->status_progress != 'selesai')
                                                                    <div class="row">
                                                                        <div class="col-md-12 grid-margin stretch-card">
                                                                            <div class="card">
                                                                                <div class="card-body">
                                                                                    <div class="row">

                                                                                        <div class="col-md-12" style="text-align: center;">

                                                                                            @if(Auth::user()->id_role==3 and Auth::user()->pegawai->unit_kerja->eselon != 4)
                                                                                                <span data-toggle="tooltip" data-placement="left"
                                                                                                      title="Disposisikan Kepada Pejabat atau Staff Dibawah Anda">
                                    <a href="{{ url('to/suratmasuk-disposisi/'.$suratmasuk->id) }}#disposisi"
                                       class="btn btn-success">
                                                <i class="fa fa-share-square-o"></i> Disposisikan
                                        </a>
                                    </span>
                                                                                            @endif

                                                                                            @if(Auth::user()->id_role==3 and Auth::user()->pegawai->id_unit_kerja != '000001')
                                                                                                <span data-toggle="tooltip" data-placement="left"
                                                                                                      title="Diteruskan Kepada Pejabat Lain">
                                    <a href="{{ url('to/suratmasuk-diteruskan/'.$suratmasuk->id) }}#diteruskan" class="btn btn-info">
                                                <i class="fa fa-share-square"></i> Diteruskan
                                        </a>
                                    </span>
                                                                                            @endif

                                                                                            @if(Auth::user()->pegawai->id_unit_kerja != '000001')
                                                                                                <span data-toggle="tooltip" data-placement="left"
                                                                                                      title="Beri Catatan & Laporan Terhadap Hasil Tindak Lanjut Dari Surat Ini">
                                    <a href="{{ url('to/suratmasuk-catatan/'.$suratmasuk->id) }}#catatan" class="btn btn-facebook">
                                                <i class="fa fa-edit"></i> Beri Catatan & Laporan
                                        </a>
                                    </span>
                                                                                            @endif


                                                                                        </div>


                                                                                    </div>


                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </section>

                                                            <section class="panel panel-default panel-square"
                                                                     style="margin-bottom:15px;">
                                                                <header class="panel-heading">
                                                                    <h5 class="text-facebook">Catatan</h5>
                                                                </header>
                                                                <div class="table-responsive">
                                                                    <table
                                                                        class="table table-th-block table-default table-striped table-bordered responsive">
                                                                        <thead>
                                                                        <tr>
                                                                            <th class="text-center" style="width:7%">#
                                                                            </th>
                                                                            <th class="text-center" style="width:12%">
                                                                                Tanggal
                                                                            </th>
                                                                            <th class="text-center" style="width:25%">
                                                                                Dari
                                                                            </th>
                                                                            <th class="text-center">Catatan</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tbody>

                                                                        @if(count($suratmasuk->logCatatan) > 0 )
                                                                            @php
                                                                                $no =1;
                                                                            @endphp

                                                                            @foreach($suratmasuk->logCatatan as $row)
                                                                                @php $data = json_decode($row->data);
                                                                            $data->catatan = str_replace('&lt;br/&gt;','<br/>',$data->catatan);
                                                                                @endphp
                                                                                <tr>
                                                                                    <td>{{ $no }}</td>
                                                                                    <td>
                                                                                        <span class="badge badge-primary">
                                                                                        {{ \Carbon\Carbon::parse($row->created_at)->format('d-m-Y') }}
                                                                                        </span> <br/>
                                                                                        <span class="badge badge-secondary">
                                                                                        Jam : {{ \Carbon\Carbon::parse($row->created_at)->format('H:i') }}
                                                                                        </span>
                                                                                    </td>
                                                                                    <td>
                                                                                    <span class="badge badge-success">
                                                                                              {{ $row->dari_nama_unit_kerja }} </span>
                                                                                        <br/>
                                                                                        <span
                                                                                            class="badge badge-secondary">
                                                                                          {{ $row->created_by_nama }}
                                                                                          </span>
                                                                                    </td>

                                                                                    <td>
                                                                                        {!! str_replace('&lt;br/&gt;','<br/>',$data->catatan)
                                                                                        !!}

                                                                                    </td>
                                                                                </tr>

                                                                                @php
                                                                                    $no++;
                                                                                @endphp
                                                                            @endforeach

                                                                        @else

                                                                            <tr>
                                                                                <td colspan="5" class="text-center"
                                                                                    style="height:70px;">Belum ada data.
                                                                                </td>
                                                                            </tr>

                                                                        @endif

                                                                        </tbody>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </section>

                                                            <section class="panel panel-default panel-square"
                                                                     style="margin-bottom:15px;">
                                                                <header class="panel-heading">
                                                                    <h5 class="text-facebook">Lampiran Catatan</h5>
                                                                </header>
                                                                <div class="table-responsive">
                                                                    <table
                                                                        class="table table-th-block table-default table-striped table-bordered responsive">
                                                                        <thead>
                                                                        <tr>
                                                                            <th class="text-center" style="width:7%">#
                                                                            </th>
                                                                            <th class="text-center" style="width:12%">
                                                                                Tanggal
                                                                            </th>
                                                                            <th class="text-center" style="width:25%">
                                                                                Dari
                                                                            </th>
                                                                            <th class="text-center">File</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>

                                                                        @if(count($files_lampiran_catatan) > 0 )
                                                                            @php
                                                                                $no =1;
                                                                            @endphp

                                                                            @foreach($files_lampiran_catatan as $row)
                                                                                <tr>
                                                                                    <td>{{ $no }}</td>
                                                                                    <td>
                                                                                        <span class="badge badge-primary">
                                                                                        {{ \Carbon\Carbon::parse($row->created_at)->format('d-m-Y') }}
                                                                                        </span> <br/>
                                                                                         <span class="badge badge-secondary">
                                                                                        Jam : {{ \Carbon\Carbon::parse($row->created_at)->format('H:i') }}
                                                                                        </span>
                                                                                    </td>
                                                                                    <td>
                                                                                    <span class="badge badge-success">
                                                                                              {{ $row->byUsername->pegawai->unit_kerja->nama_unit_kerja }} </span>
                                                                                        <br/>
                                                                                        <span
                                                                                            class="badge badge-secondary">
                                                                                          {{ $row->byUsername->nama }}
                                                                                          </span>
                                                                                    </td>

                                                                                    <td style="text-align: center;">
                                                                                        <a class="btn btn-primary" target="_blank" href="{{ url('lampiran/suratmasuk/'.$row->nama_file) }}">
                                                                                            <i class="fa fa-download"> </i>
                                                                                        </a>

                                                                                    </td>
                                                                                </tr>

                                                                                @php
                                                                                    $no++;
                                                                                @endphp
                                                                            @endforeach

                                                                        @else

                                                                            <tr>
                                                                                <td colspan="5" class="text-center"
                                                                                    style="height:70px;">Belum ada data.
                                                                                </td>
                                                                            </tr>

                                                                        @endif

                                                                        </tbody>

                                                                    </table>
                                                                </div>
                                                            </section>


                                                        </div>

                                                    </div>


                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="2" role="tabpanel" aria-labelledby="profile-2-vertical">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="panel panel-default panel-square" style="margin-bottom:15px;">

                                                <div class="table-responsive">
                                                    <table
                                                        class="table table-th-block table-hover table-bordered responsive">


                                                        <tbody>
                                                        <tr>
                                                            <th><h5 class="text-facebook"> Diteruskan Kepada:</h5></th>
                                                            <th> Waktu</th>
                                                            <th> Tindak Lanjut</th>
                                                        </tr>
                                                        @foreach($suratmasuk->notif()->where('kategori','arahan')->orderby('kepada_id_unit_kerja')->orderby('created_at')->get() as $row)
                                                            <tr>
                                                                @php
                                                                    if(isset($row->kepadaUnitKerja->nama_unit_kerja )) {
                                                                        $kepadaunitkerja = $row->kepadaUnitKerja->nama_unit_kerja;
                                                                    }
                                                                    else {
                                                                        $kepadaunitkerja = $row->nama;
                                                                    }
                                                                    @endphp
                                                                <td class="text-left"><strong
                                                                        class="text-primary"> {{ $kepadaunitkerja }} </strong><br>
                                                                </td>
                                                                <td class="text-center" style="width: 120px;">
                                                                    <div><span style="width:100%"
                                                                               class="badge badge-warning">  {{ \Carbon\Carbon::parse($row->created_at)->format('d-m-Y H:i') }}</span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-center" style="width: 120px;">
                                                                    <div><span style="width:100%"
                                                                               class="badge badge-success"> {{ $row->status }} </span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach


                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-lg-6">

                                            <div class="panel panel-default panel-square" style="margin-bottom:15px;">

                                                <div class="table-responsive">
                                                    <table
                                                        class="table table-th-block table-hover table-bordered responsive">
                                                        <th><h5 class="text-facebook"> Disposisi Kepada:</h5></th>
                                                        <th> Waktu</th>
                                                        <th> Tindak Lanjut</th>

                                                        <tbody>
                                                        @foreach($suratmasuk->notif()->where('kategori','disposisi')->orderby('created_at')->get() as $row)

                                                            @php
                                                                if(isset($row->kepadaUnitKerja->nama_unit_kerja )) {
                                                                    $kepadaunitkerja = $row->kepadaUnitKerja->nama_unit_kerja;
                                                                }
                                                                else {
                                                                    $kepadaunitkerja = $row->nama;
                                                                }
                                                            @endphp

                                                            <tr>
                                                                <td class="text-left"><strong
                                                                        class="text-primary"> {{ $kepadaunitkerja }} </strong><br>
                                                                </td>
                                                                <td class="text-center" style="width: 120px;">
                                                                    <div><span style="width:100%"
                                                                               class="badge badge-warning">  {{ \Carbon\Carbon::parse($row->created_at)->format('d-m-Y H:i') }}</span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-center" style="width: 120px;">
                                                                    <div><span style="width:100%"
                                                                               class="badge badge-success"> {{ $row->status }} </span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach


                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                        <div class="tab-pane fade" id="2a" role="tabpanel" aria-labelledby="profile-2a-vertical">
                            <div class="card">
                                <div class="card-body">

                                    @if(count($files_pdf) > 0 )
                                        @foreach($suratmasuk->lampiran->where('kategori','utama') as $row)

                                        <iframe width="100%" height="800px" src="{{ url('lampiran/suratmasuk/'.$row->nama_file) }}">
                                        </iframe>

                                        @endforeach
                                    @endif

                                        @if(count($files_images) > 0 )

                                            <iframe width="100%" height="800px" src="{{ url('suratmasuk-lampiran-images/'.$suratmasuk->id) }}">
                                            </iframe>

                                        @endif

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


            @if($suratmasuk->status_progress != 'selesai')
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12" style="text-align:center;">

                                    @if(true)
                                        <span data-toggle="tooltip" data-placement="left"
                                              title="Klik Untuk Mengubah Satus Surat Ini Menjadi SELESAI">
                                            <a onclick="return confirm('Anda Yakin Akan Menyelesaikan Surat Masuk Ini..?')" href="{{ url('to/suratmasuk-catatan-selesai/'.$suratmasuk->id) }}"
                                               class="btn btn-youtube col-md-12">
                                                        <i class="fa fa-check"></i>:: END PROSES ::
                                            </a>
                                            </span>
                                    @endif

                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
            @endif

        </div>

        @if($suratmasuk->oleh_id_user==Auth::user()->id)
            <div id="settings-trigger" data-toggle="tooltip" data-placement="left" title="Edit Data Surat Masuk">
                <a href="{{ url('to/suratmasuk-edit/'.$suratmasuk->id) }}">
                    <i class="mdi mdi-pencil" style="padding: 100% 0 100% 0"></i>
                </a>
            </div>
        @endif





    </div>
</div>





            <script>
                (function($) {
                    'use strict';
                    $(function() {
                        $('#show').avgrund({
                            height: 1000,
                            holderClass: 'custom',
                            showClose: true,
                            showCloseText: 'x',
                            onBlurContainer: '.container-scroller',
                            template: '  <img src="{{ asset('qrcode/'.$suratmasuk->id.'.png') }}" width="100%" height="100%"/>'
                        });
                    })
                })(jQuery);

            </script>

@include('layouts.js-page')
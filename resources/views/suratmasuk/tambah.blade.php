
<style>
    .date {
        position: relative;
        width: 150px; height: 20px;
        color: white;
    }

    .date:before {
        position: absolute;
        top: 3px; left: 3px;
        content: attr(data-date);
        display: inline-block;
        color: black;
    }

    .date::-webkit-datetime-edit, .date::-webkit-inner-spin-button, .date::-webkit-clear-button {
        display: none;
    }

    .date::-webkit-calendar-picker-indicator {
        position: absolute;
        top: 3px;
        right: 0;
        color: black;
        opacity: 1;
    }
</style>
<div id="page">

<div class="row" style="margin-top:40px;">
    <div class="col-lg-12 grid-margin d-flex flex-column">
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb bg-primary">
                    <li class="breadcrumb-item"><a href="{!! url('suratmasuk-daftar') !!}">Surat Masuk</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><b>Tambah</b></li>
                </ol>
            </div>

        </div>



        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h5> Tambah Data Surat Masuk </h5>
@php
//print_r(Session::get('pesan'));
@endphp
                        @if($messages=Session::get('pesan'))
                        <div class="alert alert-success alert-block">
                            <strong>{{ $messages }}</strong>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        {!! Form::open(['url' => 'suratmasuk-tambah-proses','class'=>'form-sample','method' => 'post','files' => true]); !!} {!! Form::token(); !!}
                        {!! Form::hidden('gen', $gen) !!}

                        <div class="row">
                                    <div class="col-md-6">
                                        <h4>Form Utama</h4>
                                        <div class="form-group row">
                                            {!! Form::label('kepada', 'Kepada',['class'=>'col-sm-3 col-form-label']); !!}
                                            <div class="col-sm-9">
                                                {!! Form::select('kepada', $unitkerja, '', ['class'=>'select2 col-sm-10 form-control','width'=>'100%','placeholder'=>'Pilih Tujuan Surat']) !!}
                                                <sup class="text-danger">* wajib diisi</sup>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {!! Form::label('pengirim', 'Pengirim',['class'=>'col-sm-3 col-form-label']); !!}
                                            <div class="col-sm-9">
                                                {!! Form::text('pengirim', '',['class'=>'form-control','placeholder'=>'Pengirim']); !!}
                                                <sup class="text-danger">* wajib diisi</sup>
                                            </div>
                                        </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        {!! Form::label('nosurat', 'Nomor Surat',['class'=>'col-sm-12 col-form-label']); !!}

                                    </div>
                                    <div class="col-sm-9">
                                        {!! Form::text('nosurat', '',['class'=>'form-control','placeholder'=>'Nomor Surat','required'=>'required']); !!}
                                        <sup class="text-danger">* wajib diisi</sup>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('tgl_surat', 'Tanggal Surat',['class'=>'col-sm-3 col-form-label']); !!}
                                    <div class="col-sm-9">
                                        {!! Form::date('tgl_surat', date('Y-m-d'),['class'=>'date','data-date-format'=>'DD MMMM YYYY','placeholder'=>'Tanggal Surat','required'=>'required']); !!}
                                        <span class="badge badge-secondary"><i>(Tanggal/Bulan/Tahun)</i></span> <br/> <sup class="text-danger">* wajib diisi</sup>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('tgl_diterima', 'Tanggal Diterima',['class'=>'col-sm-3 col-form-label']); !!}
                                    <div class="col-sm-9">
                                        {!! Form::date('tgl_diterima', date('Y-m-d'),['class'=>'date','data-date-format'=>'DD MMMM YYYY','placeholder'=>'Tanggal Diterima','required'=>'required']); !!}
                                       <span class="badge badge-secondary"><i>(Tanggal/Bulan/Tahun)</i></span> <br/> <sup class="text-danger">* wajib diisi</sup>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('tgl_pelaksanaan', 'Tanggal Pelaksanaan',['class'=>'col-sm-3 col-form-label']); !!}
                                    <div class="col-sm-9">
                                        {!! Form::date('tgl_pelaksanaan', date('Y-m-d'),['class'=>'date','data-date-format'=>'DD MMMM YYYY','placeholder'=>'Tanggal Pelaksanaan']); !!}
                                       <span class="badge badge-secondary"><i>(Tanggal/Bulan/Tahun)</i></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('perihal', 'Perihal',['class'=>'col-sm-3 col-form-label','required'=>'required']); !!}
                                    <div class="col-sm-9">
                                        {!! Form::textarea('perihal', null,['class'=>'form-control']); !!}  <sup class="text-danger">* wajib diisi</sup>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">

                                <h4>Klasifikasi</h4>
                                <div class="form-group row">
                                    {!! Form::label('klasifikasi', 'Klasifikasi',['class'=>'col-sm-3 col-form-label']); !!}
                                    <div class="col-sm-9">
                                        {!! Form::select('klasifikasi', $klasifikasi, '', ['class'=>'select2 col-sm-10','placeholder'=>'pilih jenis klasifikasi']
                                        ) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('keterangan_lampiran', 'Keterangan Lampiran',['class'=>'col-sm-3 col-form-label']); !!}
                                    <div class="col-sm-9">
                                        {!! Form::select('keterangan_lampiran', $keteranggan_lampiran, '', ['class'=>'select2 col-sm-10','placeholder'=>'Pilih Keterangan
                                        Lampiran'] ) !!}
                                    </div>
                                </div>

                                <h4>Retensi</h4>
                                <div class="form-group row">
                                    {!! Form::label('retensi', 'Retensi',['class'=>'col-sm-3 col-form-label']); !!}
                                    <div class="col-sm-9">
                                        {!! Form::select('retensi', $jra, '', ['class'=>'select2 col-sm-10','placeholder'=>'Pilih Retensi'] ) !!}
                                    </div>

                                </div>


                            </div>

                        </div>
                        <hr/>
                        <div class="row">

                            <div class="col-md-6">
                                <h4>Surat Diteruskan Kepada </h4>
                                <div class="form-group row increment2">

                                    <div class="col-sm-12">
                                        {!! Form::select('diteruskan_kepada[]', $unitkerja_teruskan, '', ['class'=>'select2 col-sm-10 form-control','multiple'=>'multiple','required'=>'required']) !!}
                                        <sup class="text-danger">* wajib diisi (minimal sama dengan tujuan surat atau lebih dari satu)</sup>
                                    </div>



                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4>Lampiran Surat   </h4>

                                <div class="form-group row">
                                    <div class="input-group control-group increment">

                                        <div class="col-sm-9">
                                            <span class="badge badge-warning">Format: JPG, PNG, atau PDF</span> <br/> <sup class="text-danger">* Hanya Satu Jenis Format, Tidak Bisa Bercampur format JPG/PNG dengan PDF  </sup>  <br/>
                                            <input type="file" name="file_attachment[]" accept=".jpg, .png, .jpeg, .pdf" class="form-control">


                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group-btn">
                                                <br/>
                                                <button class="btn btn-xs btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                                            </div>
                                        </div>
                                        <div class="clone" style="display: none">
                                            <div class="control-group input-group" style="margin-top:10px">
                                                <div class="col-sm-9">
                                                    <input type="file" name="file_attachment[]" accept=".jpg, .png, .jpeg, .pdf" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="input-group-btn">
                                                        <button class="btn btn-xs btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="text-align: right">

                                <a class="btn btn-sm btn-dark" href="{{ url('to/suratmasuk-daftar') }}">
                                        <i class="fa fa-close"></i> Batal
                                </a>
                                <button class="btn btn-sm btn-warning" type="reset">
                                        <i class="fa fa-refresh"></i> Reset
                                </button>
                                <button class="btn btn-sm btn-facebook" type="submit">
                                        <i class="fa fa-check-circle"></i> Simpan
                                </button>


                            </div>
                        </div>
                        {!! Form::close(); !!}


                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
</div>

    <script>


        $(function(){

            $('.select2').select2();

            $(".btn-success").click(function(){
                var html = $(".clone").html();
                $(".increment").after(html);
            });

            $("body").on("click",".btn-danger",function(){
                $(this).parents(".control-group").remove();
            });


            $(".btn-diteruskan-add").click(function(){
                var html = $(".clone2").html();
                $(".increment2").after(html);
            });

            $("body").on("click",".btn-diteruskan-del",function(){
                $(this).parents(".control-group").remove();
            });

            setTimeout(function(){
                showInfoToast()
            },1000);


            setTimeout(function(){
                showInfoToast2()
            },4000);

         });

        (function($) {

            showInfoToast = function() {
              'use strict';
              resetToastPosition();
              $.toast({
                heading: 'peringatan',
                text: '  1. Agar dijaga kerahasiaan surat ini; <br/>',
                showHideTransition: 'slide',
                icon: 'info',
                loaderBg: '#46c35f',
                position: 'top-right'
              })
            };

            showInfoToast2 = function() {
                'use strict';
                resetToastPosition();
                $.toast({
                  heading: 'peringatan',
                  text: '  2. Dilarang memisahkan sehelaipun surat atau dokumen dari berkasi ini. <br/>',
                  showHideTransition: 'slide',
                  icon: 'info',
                  loaderBg: '#46c35f',
                  position: 'top-right'
                })
              };



            resetToastPosition = function() {
              $('.jq-toast-wrap').removeClass('bottom-left bottom-right top-left top-right mid-center'); // to remove previous position class
              $(".jq-toast-wrap").css({
                "top": "",
                "left": "",
                "bottom": "",
                "right": ""
              }); //to remove previous position style
            }
          })(jQuery);


          $(".date").on("change", function() {
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                .format( this.getAttribute("data-date-format") )
            )
        }).trigger("change");

    </script>

@include('layouts.js-page')

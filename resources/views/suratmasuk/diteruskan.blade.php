<div id="page">


    <div class="row" style="margin-top:40px;">
        <div class="col-lg-12 grid-margin d-flex flex-column">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb bg-primary">
                        <li class="breadcrumb-item"><a href="{{ url('to/suratmasuk-daftar') }}">Surat Masuk</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('to/suratmasuk-daftar-proses/'.$suratmasuk->id) }}">{{ $suratmasuk->perihal }} </a></li>
                        <li class="breadcrumb-item active" aria-current="page"><b>Diteruskan</b></li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Surat Masuk Diteruskan</h4>

                            {!! Form::open(['url' => 'suratmasuk-diteruskan','class'=>'form-sample','method' => 'post','files' => true]);
                            !!} {!! Form::token(); !!} {!! Form::hidden('disposisi_id', $disposisi_id ) !!}

                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="panel panel-primary panel-square"
                                         style="margin-bottom:15px;">


                                        <div class="panel-body profile-activity">
                                            <div class="table-responsive">
                                                <table
                                                    class="table table-striped table-hover table-bordered responsive">
                                                    <tbody>
                                                    <tr>
                                                        <td style="width:35%"><b
                                                                class="text-info">Kepada : </b>
                                                        </td>
                                                        <td class="text-left"><b
                                                                class="text-info">
                                                                @if(isset($suratmasuk->kepada_unitkerja_nama))
                                                                    {{ $suratmasuk->kepada_unitkerja_nama }}
                                                                @endif
                                                            </b></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:35%"><b
                                                                class="text-info">No. Agenda
                                                                : </b></td>
                                                        <td class="text-left"><b
                                                                class="text-info">
                                                                {{ $suratmasuk->nomor_agenda }}
                                                            </b></td>
                                                    </tr>

                                                    </tbody>
                                                </table>

                                            </div>

                                            <div class="table-responsive">

                                                <table
                                                    class="table table-striped table-hover table-bordered responsive">
                                                    <tbody>
                                                    <tr>
                                                        <td style="width:35%"><b class="">Pengirim
                                                                : </b></td>
                                                        <td class="text-left"><b class="">
                                                                {{ $suratmasuk->pengirim }}
                                                            </b></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:35%"><b class="">Tgl.
                                                                Surat : </b></td>
                                                        <td class="text-left"><b class="">
                                                                {{ \Carbon\Carbon::parse($suratmasuk->tanggal_surat_masuk)->format('d-m-Y') }}
                                                            </b></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:35%"><b class="">No.
                                                                Surat : </b></td>
                                                        <td class="text-left"><b class="">
                                                                {{ $suratmasuk->pengirim_nomor_surat }}
                                                            </b></td>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="table-responsive">

                                                <table
                                                    class="table table-striped table-hover table-bordered responsive">
                                                    <tbody>
                                                    <tr>
                                                        <td style="width:35%"><b
                                                                class="text-primary">Tgl.
                                                                Diterima : </b></td>
                                                        <td class="text-left"><b
                                                                class="text-primary">
                                                                {{ \Carbon\Carbon::parse($suratmasuk->tanggal_diterima)->format('d-m-Y') }}
                                                            </b></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:35%"><b
                                                                class="text-primary">Tgl.
                                                                Pelaksanaan : </b></td>
                                                        <td class="text-left"><b
                                                                class="text-primary">
                                                                {{ \Carbon\Carbon::parse($suratmasuk->tanggal_pelaksanaan)->format('d-m-Y') }}
                                                            </b></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:35%"><b
                                                                class="text-primary">Perihal
                                                                : </b></td>
                                                        <td class="text-left"><b
                                                                class="text-primary">
                                                                {{ $suratmasuk->perihal }}
                                                            </b></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="panel panel-default panel-square"
                                                     style="margin-bottom:15px;">
                                                    <header class="panel-heading">
                                                        <h5 class="text-facebook">Klasifikasi
                                                            Surat
                                                            Masuk</h5>
                                                    </header>
                                                    <div class="panel-body profile-activity">
                                                        <div class="table-responsive">
                                                            <table
                                                                class="table table-striped table-hover table-bordered responsive"
                                                                style="margin-bottom:0px;">
                                                                <tbody>
                                                                <tr>
                                                                    <td><b
                                                                            class="">Klasifikasi : </b>
                                                                    </td>
                                                                    <td class="text-left"><b
                                                                            class="text-danger">{{ $suratmasuk->klasifikasi }}</b>
                                                                    </td>
                                                                    <td><b
                                                                            class="">Ket.
                                                                            Lampiran : </b></td>
                                                                    <td class="text-left"><b
                                                                            class="text-primary">{{ $suratmasuk->keterangan_lampiran }}</b>
                                                                    </td>
                                                                </tr>


                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-4">
                                    <div class="panel panel-info panel-square panel-block-color"
                                         style="margin-bottom:15px;">
                                        <header class="panel-heading">
                                            <h5 class="text-facebook">QR Code</h5>
                                        </header>
                                        <div>
                                            <div class="row" style="text-align: center;">
                                                <a href="#" id="show">
                                                    <img src="{{ asset('qrcode\\'.$suratmasuk->id.'.png') }}"
                                                         width="30%"/>
                                                </a>

                                            </div>
                                        </div>
                                    </div>


                                    <div
                                        class="panel panel-danger panel-square panel-block-color"
                                        style="margin-bottom:15px;">
                                        <header class="panel-heading">
                                            <h5 class="text-facebook">Lembar Disposisi</h5>
                                        </header>
                                        <div>
                                            <div class="row">
                                                <div class="col-md-12"
                                                     style="text-align: center">
                                                    <a class="btn btn-sm btn-danger"
                                                       target="_blank"
                                                       href="{{url('to/suratmasuk-lembar-disposisi/'.$suratmasuk->id)}}"
                                                     ><i class="fa fa-file-pdf-o"></i>
                                                        Tampilkan</a>

                                                    <a class="btn btn-sm btn-warning"
                                                       target="_blank"
                                                       href="{{url('to/suratmasuk-lembar-disposisi-manual/'.$suratmasuk->id)}}"
                                                      ><i class="fa fa-file-pdf-o"></i>
                                                        Manual</a>
                                                </div>


                                            </div>

                                            <div class="row">

                                            </div>

                                        </div>
                                    </div>
                                    <div class="panel panel-default panel-square"
                                         style="margin-bottom:15px;">
                                        <header class="panel-heading">
                                            <h5 class="text-facebook">Retensi</h5>
                                        </header>
                                        <div class="table-responsive">
                                            <table
                                                class="table table-th-block table-hover table-bordered responsive">
                                                <tbody>
                                                <tr>
                                                    <td class="text-left"
                                                        style="width: 150px; padding-left:15px;">
                                                        <div><b> Active</b></div>
                                                    </td>
                                                    <td class="text-left"><strong
                                                            class="text-primary">{{ $suratmasuk->retensi_active }}
                                                            tahun</strong><br></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left"
                                                        style="width: 150px; padding-left:15px;">
                                                        <div><b> Inactive </b></div>
                                                    </td>
                                                    <td class="text-left"><strong
                                                            class="text-primary">{{ $suratmasuk->retensi_inactive }}
                                                            tahun</strong><br></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left"
                                                        style="width: 150px; padding-left:15px;">
                                                        <div><b> Keterangan </b>
                                                        </div>
                                                    </td>
                                                    <td class="text-left"><strong
                                                            class="text-primary">{{ $suratmasuk->retensi_keterangan }}</strong><br>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="panel panel-default panel-square"
                                         style="margin-bottom:15px;">
                                        <header class="panel-heading">
                                            <h5 class="text-facebook">Dibuat Oleh
                                                :</h5>
                                        </header>
                                        <div class="table-responsive">
                                            <table
                                                class="table table-th-block table-hover table-bordered responsive">
                                                <tbody>
                                                <tr>
                                                    <td class="text-center"
                                                        style="width: 150px;">
                                                        <div> <span
                                                                style="width:100%"
                                                                class="label label-info square"
                                                                data-toggle="tooltip"
                                                                data-original-title="Tgl. Create"
                                                                data-placement="right">  {{ \Carbon\Carbon::parse($suratmasuk->created_at)->format('d-m-Y H:i') }} </span>
                                                        </div>
                                                    </td>
                                                    <td class="text-left"><strong
                                                            class="text-primary">{{ $suratmasuk->created_by_nama }}</strong><br>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-lg-12">

                                    <section class="panel panel-default panel-square"
                                             style="margin-bottom:15px;">
                                        <header class="panel-heading">
                                            <h5 class="text-facebook">Instruksi : </h5>
                                        </header>
                                        <div class="table-responsive">
                                            <table
                                                class="table table-th-block table-default table-striped table-bordered responsive">
                                                <thead>
                                                <tr>
                                                    <th class="text-center" style="width:7%">#
                                                    </th>
                                                    <th class="text-center" style="width:12%">
                                                        Tanggal
                                                    </th>
                                                    <th class="text-center" style="width:25%">
                                                        Dari
                                                    </th>
                                                    <th class="text-center" style="width:30%">
                                                        Kepada
                                                    </th>
                                                    <th class="text-center">Catatan</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @if(count($suratmasuk->logDisposisi) > 0 )
                                                    @php
                                                        $no =1;
                                                    @endphp
                                                    @foreach($suratmasuk->logDisposisi as $row)
                                                        @php
                                                            $data = json_decode($row->data); $data->catatan = str_replace('&lt;br/&gt;','<br/>',$data->catatan);
                                                            $data->kepada = str_replace('&lt;br/&gt;','<br/>',$data->kepada);
                                                        @endphp
                                                        <tr>
                                                            <td>{{ $no }}</td>
                                                            <td> {{ \Carbon\Carbon::parse($row->created_at)->format('d-m-Y') }}
                                                            </td>
                                                            <td>
                                                                                <span class="badge badge-success">
                                                                                              {{ $row->dari_nama_unit_kerja }} </span>
                                                                <br/>
                                                                <span
                                                                    class="badge badge-secondary">
                                                                                          {{ $row->created_by_nama }}
                                                                                          </span>
                                                            </td>
                                                            <td>
                                                                {!! str_replace('&lt;br/&gt;','<br/>',$data->kepada) !!}
                                                            </td>
                                                            <td>
                                                                {!! str_replace('&lt;br/&gt;','<br/>',$data->catatan) !!}

                                                            </td>
                                                        </tr>

                                                        @php $no++;
                                                        @endphp @endforeach @else

                                                    <tr>
                                                        <td colspan="5" class="text-center"
                                                            style="height:70px;">Belum ada data.
                                                        </td>
                                                    </tr>

                                                @endif

                                                </tbody>
                                            </table>
                                        </div>
                                    </section>

                                    <section class="panel panel-default panel-square"
                                             style="margin-bottom:15px;">
                                        <header class="panel-heading">
                                            <h5 class="text-facebook">Catatan</h5>
                                        </header>
                                        <div class="table-responsive">
                                            <table
                                                class="table table-th-block table-default table-striped table-bordered responsive">
                                                <thead>
                                                <tr>
                                                    <th class="text-center" style="width:7%">#
                                                    </th>
                                                    <th class="text-center" style="width:12%">
                                                        Tanggal
                                                    </th>
                                                    <th class="text-center" style="width:25%">
                                                        Dari
                                                    </th>
                                                    <th class="text-center">Catatan</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tbody>

                                                @if(count($suratmasuk->logCatatan) > 0 )
                                                    @php
                                                        $no =1;
                                                    @endphp

                                                    @foreach($suratmasuk->logCatatan as $row)
                                                        @php $data = json_decode($row->data);
                                                                            $data->catatan = str_replace('&lt;br/&gt;','<br/>',$data->catatan);
                                                        @endphp
                                                        <tr>
                                                            <td>{{ $no }}</td>
                                                            <td> {{ \Carbon\Carbon::parse($row->created_at)->format('d-m-Y')
                                                                                    }}
                                                            </td>
                                                            <td>
                                                                                    <span class="badge badge-success">
                                                                                              {{ $row->dari_nama_unit_kerja }} </span>
                                                                <br/>
                                                                <span
                                                                    class="badge badge-secondary">
                                                                                          {{ $row->created_by_nama }}
                                                                                          </span>
                                                            </td>

                                                            <td>
                                                                {!! str_replace('&lt;br/&gt;','<br/>',$data->catatan)
                                                                !!}

                                                            </td>
                                                        </tr>

                                                        @php
                                                            $no++;
                                                        @endphp
                                                    @endforeach

                                                @else

                                                    <tr>
                                                        <td colspan="5" class="text-center"
                                                            style="height:70px;">Belum ada data.
                                                        </td>
                                                    </tr>

                                                @endif

                                                </tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </section>
                                </div>

                            </div>

                            <div id="diteruskan" style="padding-bottom: 30px"><br/></div>

                            <div style="background-color: #d7d4f0; padding-top: 20px; padding-bottom: 20px;"
                                 class="row">
                                <div class="col-md-12">
                                    <h4>Diteruskan</h4>

                                    <div class="form-group row">
                                        {!! Form::label('kepada', 'Kepada',['class'=>'col-sm-2 col-form-label']); !!}
                                        <div class="col-sm-10">
                                            {!! Form::select('kepada[]', $unitkerja_kepada, '', ['class'=>'select2 col-sm-10','multiple'=>'multiple','autofocus'=>'autofocus']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {!! Form::label('catatan', 'Catatan',['class'=>'col-sm-2 col-form-label']); !!}
                                        <div class="col-sm-10">
                                            {!! Form::textarea('catatan', null,['class'=>'form-control']); !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="text-align: left">
                                            <a class="btn btn-sm btn-dark"
                                               href="{{ url('to/suratmasuk-daftar-proses/'.$disposisi_id) }}">
                                                <i class="fa fa-close"></i> Batal
                                            </a>
                                            <button class="btn btn-sm btn-warning" type="reset">
                                                <i class="fa fa-refresh"></i> Reset
                                            </button>
                                        </div>
                                        <div class="col-md-6" style="text-align: right">
                                            <button class="btn btn-sm btn-facebook" type="submit">
                                                <i class="fa fa-check-circle"></i> Proses
                                            </button>


                                        </div>
                                    </div>


                                </div>


                            </div>
                            <hr/> {!! Form::close() !!}


                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
            <script>


                $(function () {

                    $('.select2').select2();

                });

            </script>
            @include('layouts.js-page')

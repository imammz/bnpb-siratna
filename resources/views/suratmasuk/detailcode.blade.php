<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('ui/vendors/iconfonts/mdi/font/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('ui/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('ui/vendors/css/vendor.bundle.addons.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('ui/css/vertical-layout-light/style.css')}}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{asset('ui/images/favicon.png')}}"/>
</head>

<body style="overflow: auto;">
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
            <div class="row flex-grow">
                <div class="col-lg-12 d-flex align-items-center justify-content-center">
                    <div class="auth-form-transparent text-left p-3">

                        <div class="form-group" style="margin-top: 20px;">
                            <div>
                                <img src="{{asset('ui/images/logo.png')}}" width="50" alt="logo">
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="exampleInputEmail">No. Surat : </label>
                            <div class="input-group">
                                <div class="input-group-prepend bg-transparent">
                                    <b class="text-primary">
                                        {{ $surat->pengirim_nomor_surat }}
                                    </b>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail">Perihal : </label>
                            <div class="input-group">
                                <div class="input-group-prepend bg-transparent">
                                    <b class="text-primary">
                                        {{ $surat->perihal }}
                                    </b>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail">Tanggal Surat : </label>
                            <div class="input-group">
                                <div class="input-group-prepend bg-transparent">
                                    <b class="text-primary">
                                        {{ \Carbon\Carbon::parse($surat->tanggal_surat_masuk)->format('d-m-Y') }}
                                    </b>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail">No. Surat : </label>
                            <div class="input-group">
                                <div class="input-group-prepend bg-transparent">
                                    <b class="text-primary">
                                        {{ $surat->pengirim_nomor_surat }}
                                    </b>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <section class="panel panel-default panel-square"
                                     style="margin-bottom:15px;">
                                <header class="panel-heading">
                                    <h5 class="text-facebook">Instruksi Disposisi : </h5>
                                </header>
                                <div class="table-responsive">
                                    <table
                                        class="table table-th-block table-default table-striped table-bordered responsive">
                                        <thead>
                                        <tr>
                                            <th class="text-center" style="width:7%">#
                                            </th>
                                            <th class="text-center" style="width:12%">
                                                Informasi
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if(count($surat->logDisposisi) > 0 )
                                            @php
                                                $no =1;
                                            @endphp
                                            @foreach($surat->logDisposisi as $row)
                                                @php
                                                    $data = json_decode($row->data); $data->catatan = str_replace('&lt;br/&gt;','<br/>',$data->catatan);
                                                    $data->kepada = str_replace('&lt;br/&gt;','<br/>',$data->kepada);
                                                @endphp
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <b> Tanggal : </b> <br/>
                                                        {{ \Carbon\Carbon::parse($row->created_at)->format('d-m-Y') }}
                                                        <hr/><b> Dari : </b> <br/>
                                                        <span class="badge badge-success">
                                                                                              {{ $row->dari_nama_unit_kerja }} </span>
                                                        <br/>
                                                        <span
                                                            class="badge badge-secondary">
                                                                                          {{ $row->created_by_nama }}
                                                                                          </span>
                                                        <hr/><b> Kepada : </b> <br/>
                                                        {!! str_replace('&lt;br/&gt;','<br/>',$data->kepada) !!}
                                                        <hr/><b> Catatan : </b> <br/>
                                                        {!! str_replace('&lt;br/&gt;','<br/>',$data->catatan) !!}

                                                    </td>
                                                </tr>

                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else

                                            <tr>
                                                <td colspan="2" class="text-center"
                                                    style="height:70px;">Belum ada data.
                                                </td>
                                            </tr>

                                        @endif

                                        </tbody>
                                    </table>
                                </div>
                            </section>
                            <section class="panel panel-default panel-square"
                                     style="margin-bottom:15px;">
                                <header class="panel-heading">
                                    <h5 class="text-facebook">Catatan</h5>
                                </header>
                                <div class="table-responsive">
                                    <table
                                        class="table table-th-block table-default table-striped table-bordered responsive">
                                        <thead>
                                        <tr>
                                            <th class="text-center" style="width:7%">#
                                            </th>
                                            <th class="text-center">Catatan</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tbody>

                                        @if(count($surat->logCatatan) > 0 )
                                            @php
                                                $no =1;
                                            @endphp

                                            @foreach($surat->logCatatan as $row)
                                                @php $data = json_decode($row->data);
                                                                            $data->catatan = str_replace('&lt;br/&gt;','<br/>',$data->catatan);
                                                @endphp
                                                <tr>
                                                    <td>{{ $no }}</td>

                                                    <td>

                                                        <hr/><b> Tanggal : </b> <br/>
                                                        {{ \Carbon\Carbon::parse($row->created_at)->format('d-m-Y') }}
                                                        <hr/><b> Dari : </b> <br/>
                                                        <span class="badge badge-success">  {{ $row->dari_nama_unit_kerja }} </span>
                                                        <br/>
                                                        <span
                                                            class="badge badge-secondary">
                                                                                          {{ $row->created_by_nama }}
                                                                                          </span>
                                                        <hr/><b> Catatan : </b> <br/>
                                                        {!! str_replace('&lt;br/&gt;','<br/>',$data->catatan)
                                                        !!}

                                                    </td>
                                                </tr>

                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach

                                        @else

                                            <tr>
                                                <td colspan="2" class="text-center"
                                                    style="height:70px;">Belum ada data.
                                                </td>
                                            </tr>

                                        @endif

                                        </tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </section>

                        </div>


                    </div>
                </div>

            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="{{asset('ui/vendors/js/vendor.bundle.base.js')}}"></script>
<script src="{{asset('ui/vendors/js/vendor.bundle.addons.js')}}"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="{{asset('ui/js/off-canvas.js')}}"></script>
<script src="{{asset('ui/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('ui/js/template.js')}}"></script>
<script src="{{asset('ui/js/settings.js')}}"></script>
<script src="{{asset('ui/js/todolist.js')}}"></script>
<!-- endinject -->
</body>

</html>

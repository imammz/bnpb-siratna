<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDisposisiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_disposisi', function(Blueprint $table)
		{
			$table->string('id')->primary();



			$table->string('disposisi_template_nama')->nullable();
			$table->string('disposisi_template_id')->nullable();
			$table->string('nama')->nullable();
			$table->string('pengirim')->nullable();
			$table->string('pengirim_nomor_surat')->nullable();
			$table->enum('status', array('active','inactive','done','created'))->nullable()->comment="'active','inactive','done','created'";
			$table->string('created_by_username')->nullable();
			$table->string('created_by_nama')->nullable();
			$table->string('kepada_divisi_id')->nullable();
			$table->string('kepada_divisi_nama')->nullable();
			$table->enum('status_progress', array('open','selesai','proses'))->nullable()->default('proses')->comment="'open','selesai','proses'";
			$table->dateTime('tanggal_pelaksanaan')->nullable();
			$table->integer('retensi_active')->nullable();
			$table->integer('retensi_inactive')->nullable();
			$table->string('retensi_keterangan')->nullable();
            $table->string('keterangan_lampiran')->nullable();
            $table->string('id_user')->nullable();
			$table->integer('id_struktur_organisasi')->nullable();
            $table->integer('id_unit_kerja')->nullable();
            $table->string('kepada_id_user')->nullable();
			$table->integer('kepada_id_struktur_organisasi')->nullable();
            $table->integer('kepada_id_unit_kerja')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_disposisi');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuratKeluarTemplate003Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_surat_keluar_template_003', function(Blueprint $table)
		{
			$table->string('id', 10)->primary();
			$table->string('surat_keluar_id');
			$table->string('surat_keluar_template_id');

			$table->string('nomor_surat')->nullable();
			$table->enum('keamanan', array('B','T','R','SR'))->nullable()->comment="'B','T','R','SR'";
			$table->string('kode_arsip')->nullable();
			$table->string('sifat')->nullable();
			$table->string('prihal')->nullable();
			$table->string('kota_persuratan')->nullable();
			$table->integer('lampiran')->nullable();
			$table->enum('satuan_lampiran', array('lembar','berkas'))->nullable()->comment="'lembar','berkas'";
			$table->text('yth', 65535)->nullable();
			$table->text('pembuka', 65535)->nullable();
			$table->string('nama_acara')->nullable();
			$table->dateTime('tgl_mulai_acara')->nullable();
			$table->dateTime('tgl_akhir_acara')->nullable();
			$table->string('tempat')->nullable();
			$table->text('penutup', 65535)->nullable();
			$table->text('penandatangan', 65535)->nullable();
			$table->text('tembusan', 65535)->nullable();
			$table->text('tamu_undangan', 65535)->nullable();
			$table->integer('nomor_tahunan')->nullable();
            $table->dateTime('tanggal_approve')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_surat_keluar_template_003');
	}

}

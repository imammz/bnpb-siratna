<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuratKeluarTemplateAllTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_surat_keluar_template_all', function(Blueprint $table)
		{
			$table->string('id')->nullable();
			$table->string('surat_keluar_id')->nullable();
			$table->string('surat_keluar_template_id')->nullable();
			$table->string('nomor_surat_keluar', 100)->nullable();
			$table->string('nama_kegiatan')->nullable();
			$table->text('konten', 65535)->nullable();
			$table->dateTime('tanggal_pelaksanaan')->nullable();
            $table->dateTime('tanggal_approve')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_surat_keluar_template_all');
	}

}

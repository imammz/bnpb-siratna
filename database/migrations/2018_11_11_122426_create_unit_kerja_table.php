<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnitKerjaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unit_kerja', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('kode_unit_kerja', 45);
            $table->string('nama_unit_kerja', 45);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('unit_kerja');
	}

}

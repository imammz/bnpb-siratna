<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDisposisiNotifTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_disposisi_notif', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('disposisi_id')->nullable();

            $table->string('id_user')->nullable();
			$table->integer('id_struktur_organisasi')->nullable();
            $table->integer('id_unit_kerja')->nullable();
			$table->string('url')->nullable();
			$table->string('nama')->nullable();
			$table->enum('status', array('read','unread','meneruskan','mendisposisi','terkirim','dibaca','selesai'))->nullable()->default('terkirim')->comment="'read','unread','meneruskan','mendisposisi','terkirim','dibaca','selesai'";
			$table->enum('kategori', array('disposisi','arahan','created'))->nullable()->comment="'disposisi','arahan','created'";
            $table->dateTime('tanggal_approve')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_disposisi_notif');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuratKeluarTemplate016Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_surat_keluar_template_016', function(Blueprint $table)
		{
			$table->string('id')->primary();
			$table->string('surat_keluar_id');
			$table->string('surat_keluar_template_id')->nullable();

			$table->string('keamanan')->nullable();
			$table->string('kode_arsip')->nullable();
			$table->string('nomor_surat')->nullable();
			$table->string('nomor_tahun')->nullable();
			$table->string('tentang')->nullable();
			$table->text('isi', 65535)->nullable();
			$table->text('penandatanganan', 65535)->nullable();
			$table->date('tanggal_surat')->nullable();
            $table->dateTime('tanggal_approve')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_surat_keluar_template_016');
	}

}

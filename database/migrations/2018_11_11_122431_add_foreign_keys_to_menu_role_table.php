<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMenuRoleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('menu_role', function(Blueprint $table)
		{
			$table->foreign('id_menu', 'fk_menu_roles_menu1')->references('id')->on('menu')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_role', 'fk_menu_roles_roles1')->references('id')->on('role')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('menu_role', function(Blueprint $table)
		{
			$table->dropForeign('fk_menu_roles_menu1');
			$table->dropForeign('fk_menu_roles_roles1');
		});
	}

}

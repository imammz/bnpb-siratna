<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuratKeluarLampiranTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_surat_keluar_lampiran', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('surat_keluar_id')->nullable();
			$table->string('nama_file')->nullable();
			$table->string('keterangan_file')->nullable();
			$table->string('created_by_username')->nullable();
			$table->string('created_by_divisi_id')->nullable();
            $table->text('catatan', 65535)->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_surat_keluar_lampiran');
	}

}

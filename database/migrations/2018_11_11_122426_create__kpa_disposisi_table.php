<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKpaDisposisiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_kpa_disposisi', function(Blueprint $table)
		{
			$table->string('id')->primary();



			$table->text('perihal', 65535)->nullable();
			$table->enum('status', array('active','inactive','done','created'))->nullable()->comment = "'active','inactive','done','created'";
			$table->string('created_by_username')->nullable();
			$table->string('created_by_nama')->nullable();
			$table->string('kepada_divisi_id')->nullable();
			$table->string('kepada_divisi_nama')->nullable();
			$table->enum('status_progress', array('open','progress','done'))->nullable()->default('open')->comment="'open','progress','done'";
            $table->string('nomor_agenda')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_kpa_disposisi');
	}

}

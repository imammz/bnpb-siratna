<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuratKeluarNotifTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_surat_keluar_notif', function(Blueprint $table)
		{
			$table->integer('id', true);

			$table->string('surat_keluar_id')->nullable();

			$table->text('url', 65535)->nullable();
			$table->string('nama')->nullable();
			$table->enum('status', array('proses','finish'))->nullable()->default('proses')->comment="'proses','finish'";
            $table->enum('kategori', array('created','revisi','verifikasi'))->nullable()->comment="'created','revisi','verifikasi'";
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_surat_keluar_notif');
	}

}

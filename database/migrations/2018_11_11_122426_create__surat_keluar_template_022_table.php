<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuratKeluarTemplate022Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_surat_keluar_template_022', function(Blueprint $table)
		{
			$table->string('id', 10)->nullable();
			$table->string('surat_keluar_id')->nullable();
			$table->string('surat_keluar_template_id')->nullable();

			$table->string('nomor_surat')->nullable();
			$table->string('nomor_surat_kedua')->nullable();
			$table->enum('tipe_surat', array('pejabat_negara','non_pejabat'))->nullable()->comment="'pejabat_negara','non_pejabat'";
			$table->enum('keamanan', array('B','T','R','SR'))->nullable()->comment="'B','T','R','SR'";
			$table->string('kode_arsip')->nullable();
			$table->string('sifat')->nullable();
			$table->string('prihal')->nullable();
			$table->text('tentang', 65535)->nullable();
			$table->string('instansi_pihak_pertama')->nullable();
			$table->string('instansi_pihak_kedua')->nullable();
			$table->string('logo_pihak_kedua')->nullable();
			$table->text('pihak_pertama', 65535)->nullable()->comment('json');
			$table->text('pihak_kedua', 65535)->nullable()->comment('json');
			$table->text('pembuka', 65535)->nullable();
			$table->text('isi', 65535)->nullable();
			$table->text('pasal', 65535)->nullable();
            $table->dateTime('tanggal_approve')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_surat_keluar_template_022');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStrukturOrganisasiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('struktur_organisasi', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->smallInteger('level')->nullable();
			$table->smallInteger('urut_level')->nullable();
            $table->integer('id_struktur_atasan_langsung');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('struktur_organisasi');
	}

}

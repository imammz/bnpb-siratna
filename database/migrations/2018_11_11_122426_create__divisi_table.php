<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDivisiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_divisi', function(Blueprint $table)
		{
			$table->string('id')->primary();
			$table->string('code')->nullable();
			$table->string('nama');
			$table->string('singkatan')->nullable();
			$table->integer('eselon')->nullable()->default(5);
			$table->enum('tipe', array('struktur','non_struktur'))->nullable()->comment="'struktur','non_struktur'";
			$table->string('referal')->nullable();
			$table->integer('divisi_left')->nullable();
			$table->integer('divisi_right')->nullable();
			$table->text('data', 65535)->nullable();
			$table->enum('status', array('active','inactive'))->nullable()->default('active')->comment="'active','inactive'";
			$table->integer('hak_akses_id')->nullable();
			$table->enum('kategori', array('utama','support'))->nullable()->default('utama')->comment="'utama','support'";
            $table->string('head_user_id')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_divisi');
	}

}

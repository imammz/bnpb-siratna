<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuratKeluarTemplate002Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_surat_keluar_template_002', function(Blueprint $table)
		{
			$table->string('id', 10)->primary();
			$table->string('surat_keluar_id')->nullable();
			$table->string('surat_keluar_template_id')->nullable();

			$table->string('nomor_surat')->nullable();
			$table->enum('keamanan', array('B','T','R','SR'))->comment="'B','T','R','SR'";
			$table->string('kode_arsip');
			$table->string('sifat');
			$table->string('prihal');
			$table->string('kota_persuratan')->nullable();
			$table->integer('lampiran')->nullable();
			$table->enum('satuan_lampiran', array('lembar','berkas'))->nullable()->comment="'lembar','berkas'";
			$table->text('yth', 65535)->nullable();
			$table->text('pembuka', 65535)->nullable();
			$table->string('nama_acara');
			$table->dateTime('tgl_mulai_acara');
			$table->dateTime('tgl_akhir_acara');
			$table->string('tempat');
			$table->text('penutup', 65535)->nullable();
			$table->text('penandatangan', 65535)->nullable()->comment('json');
			$table->text('tembusan', 65535)->nullable()->comment('json');
			$table->text('tamu_undangan', 65535)->nullable()->comment('json');
			$table->integer('nomor_tahunan')->nullable();
            $table->dateTime('tanggal_approve')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_surat_keluar_template_002');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePegawaiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pegawai', function(Blueprint $table)
		{
			$table->string('id')->primary();
			$table->string('nip', 24)->unique('nip_UNIQUE');
			$table->date('tanggal_lahir')->nullable();
			$table->string('tempat_lahir', 45)->nullable();
			$table->string('id_user')->index('fk_pegawai_users_idx');
			$table->integer('id_struktur_organisasi')->index('fk_pegawai_struktur_organisasi1_idx');
            $table->integer('id_unit_kerja')->index('fk_pegawai_unit_kerja1_idx');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pegawai');
	}

}

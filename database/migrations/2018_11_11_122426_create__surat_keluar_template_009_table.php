<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuratKeluarTemplate009Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_surat_keluar_template_009', function(Blueprint $table)
		{
			$table->string('id', 10)->primary();
			$table->string('surat_keluar_id')->nullable();
			$table->string('surat_keluar_template_id')->nullable();

			$table->string('nomor_surat')->nullable();
			$table->enum('keamanan', array('B','T','R','SR'))->nullable()->comment="'B','T','R','SR'";
			$table->string('kode_arsip')->nullable();
			$table->string('kota_persuratan')->nullable();
			$table->text('menimbang', 65535)->nullable();
			$table->text('dasar', 65535)->nullable();
			$table->text('kepada', 65535)->nullable();
			$table->text('daftar_kepada', 65535)->nullable();
			$table->text('untuk', 65535)->nullable();
			$table->date('tanggal_surat')->nullable();
			$table->text('penandatangan', 65535)->nullable();
			$table->integer('nomor_tahunan')->nullable();
            $table->dateTime('tanggal_approve')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_surat_keluar_template_009');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKpaDisposisiLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_kpa_disposisi_log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('disposisi_id');

			$table->text('data', 65535)->nullable();
			$table->text('referensi', 65535)->nullable();


			$table->string('created_by_username');
            $table->string('created_by_nama')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_kpa_disposisi_log');
	}

}

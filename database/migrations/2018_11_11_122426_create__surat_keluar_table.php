<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuratKeluarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_surat_keluar', function(Blueprint $table)
		{
			$table->string('id')->primary();



			$table->string('surat_keluar_template_nama')->nullable();
			$table->string('surat_keluar_template_id')->nullable();
			$table->string('surat_keluar_template_code')->nullable();
			$table->string('nomor_surat_keluar', 100)->nullable();
			$table->string('nama')->nullable();
			$table->enum('status', array('active','inactive','done','created'))->nullable()->comment="'active','inactive','done','created'";
			$table->string('created_by_username')->nullable();
			$table->string('created_by_nama')->nullable();
			$table->date('tanggal_pelaksanaan')->nullable();
			$table->string('status_progress')->default('proses');
			$table->string('prefix_nomor_surat');
			$table->integer('retensi_active')->nullable();
			$table->integer('retensi_inactive')->nullable();
            $table->string('retensi_keterangan')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_surat_keluar');
	}

}

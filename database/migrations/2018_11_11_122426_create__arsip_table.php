<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArsipTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_arsip', function(Blueprint $table)
		{
			$table->string('id')->primary();

			$table->enum('kategori', array('internal','eksternal'))->nullable()->comment = "'internal','eksternal'";
			$table->string('nama_arsip');
			$table->string('keterangan')->nullable();


            $table->string('id_user')->nullable();
			$table->integer('id_struktur_organisasi')->nullable();
            $table->integer('id_unit_kerja')->nullable();
			$table->string('created_by_username')->nullable();
			$table->string('created_by_nama')->nullable();
			$table->string('last_user_update')->nullable();
			$table->string('klasifikasi_keamanan')->nullable();
			$table->integer('jenjang_akses')->nullable()->default(5);
			$table->integer('retensi_active')->nullable();
			$table->integer('retensi_inactive')->nullable();
            $table->string('retensi_keterangan')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_arsip');
	}

}

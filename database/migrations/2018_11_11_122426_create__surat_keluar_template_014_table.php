<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuratKeluarTemplate014Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_surat_keluar_template_014', function(Blueprint $table)
		{
			$table->string('id', 10)->primary();
			$table->string('surat_keluar_id')->nullable();
			$table->string('surat_keluar_template_id')->nullable();

			$table->string('nomor_surat')->nullable();
			$table->enum('keamanan', array('B','T','R','SR'))->nullable()->comment="'B','T','R','SR'";
			$table->string('kode_arsip')->nullable();
			$table->string('sifat')->nullable();
			$table->string('prihal')->nullable();
			$table->string('nama_jabatan')->nullable();
			$table->text('peraturan', 65535)->nullable();
			$table->text('tentang', 65535)->nullable();
			$table->text('menimbang', 65535)->nullable();
			$table->text('mengingat', 65535)->nullable();
			$table->text('menetapkan', 65535)->nullable();
			$table->text('pasal', 65535)->nullable();
			$table->string('ditetapkan_di')->nullable();
			$table->date('tanggal_penetapan')->nullable();
			$table->text('penandatangan', 65535)->nullable();
			$table->string('mentri')->nullable();
			$table->string('nama_mentri')->nullable();
			$table->text('isi_lampiran', 65535)->nullable();
			$table->integer('nomor_tahunan')->nullable();
            $table->dateTime('tanggal_approve')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_surat_keluar_template_014');
	}

}

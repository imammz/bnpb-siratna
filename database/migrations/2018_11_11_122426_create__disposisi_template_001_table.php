<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDisposisiTemplate001Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_disposisi_template_001', function(Blueprint $table)
		{
			$table->string('id')->primary();
			$table->string('disposisi_id')->nullable();
			$table->string('disposisi_template_id')->nullable();
			$table->string('indeks')->nullable();
			$table->string('jenis_surat_masuk')->nullable();
			$table->string('rahasia')->nullable();
			$table->string('penting')->nullable();
			$table->string('biasa')->nullable();
			$table->dateTime('tanggal_penyelesaian')->nullable();
			$table->string('kode')->nullable();
			$table->dateTime('tanggal_surat_masuk')->nullable();
			$table->string('nomor_surat_masuk')->nullable();
			$table->string('asal')->nullable();
			$table->text('isi_ringkas', 65535)->nullable();
			$table->dateTime('tanggal_diterima')->nullable();
            $table->text('informasi', 65535)->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_disposisi_template_001');
	}

}

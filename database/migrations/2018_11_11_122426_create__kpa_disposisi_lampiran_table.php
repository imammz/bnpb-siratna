<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKpaDisposisiLampiranTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_kpa_disposisi_lampiran', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('disposisi_id')->nullable();
			$table->string('nama_file')->nullable();
			$table->string('created_by_username')->nullable();
            $table->string('created_by_divisi_id')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_kpa_disposisi_lampiran');
	}

}

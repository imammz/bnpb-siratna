<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRetensiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_retensi', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('tipe')->nullable();
			$table->string('nama')->nullable();
			$table->integer('retensi_active')->nullable();
			$table->integer('retensi_inactive')->nullable();
            $table->string('retensi_keterangan')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_retensi');
	}

}

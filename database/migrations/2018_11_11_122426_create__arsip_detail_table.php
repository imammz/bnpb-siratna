<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArsipDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_arsip_detail', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('arsip_id');
			$table->string('nama_file')->nullable();
            $table->string('created_by_username')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_arsip_detail');
	}

}

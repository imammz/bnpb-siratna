<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuratKeluarTemplate001Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_surat_keluar_template_001', function(Blueprint $table)
		{
			$table->string('id', 10)->primary();
			$table->string('surat_keluar_id');
			$table->string('surat_keluar_template_id')->nullable();

			$table->string('nomor_surat')->nullable();
			$table->enum('keamanan', array('B','T','R','SR'))->nullable()->comment="'B','T','R','SR'";
			$table->string('kode_arsip');
			$table->text('yth', 65535)->nullable();
			$table->string('dari')->nullable()->comment('kode_bidang');
			$table->text('prihal', 65535)->nullable();
			$table->date('tanggal_surat')->nullable();
			$table->text('isi', 65535)->nullable();
			$table->text('penandatangan', 65535)->nullable();
			$table->text('tembusan', 65535)->nullable();
			$table->integer('nomor_tahunan')->nullable();
            $table->dateTime('tanggal_approve')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_surat_keluar_template_001');
	}

}

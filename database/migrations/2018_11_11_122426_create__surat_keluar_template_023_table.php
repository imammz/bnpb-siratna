<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuratKeluarTemplate023Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_surat_keluar_template_023', function(Blueprint $table)
		{
			$table->string('id', 10)->nullable();
			$table->string('surat_keluar_id')->nullable();
			$table->string('surat_keluar_template_id')->nullable();

			$table->string('nomor_surat')->nullable();
			$table->enum('keamanan', array('B','T','R','SR'))->nullable()->comment="'B','T','R','SR'";
			$table->string('kode_arsip')->nullable();
			$table->string('sifat')->nullable();
			$table->string('prihal')->nullable();
			$table->date('tanggal_pembuatan')->nullable();
			$table->text('yth', 65535)->nullable();
			$table->text('isi', 65535)->nullable();
			$table->date('tanggal_terima')->nullable();
			$table->text('penerima', 65535)->nullable()->comment('json');
			$table->text('pengirim', 65535)->nullable()->comment('json');
			$table->dateTime('tanggal_approve')->nullable();
            $table->string('no_tlp', 15)->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_surat_keluar_template_023');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuratKeluarTemplate010Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_surat_keluar_template_010', function(Blueprint $table)
		{
			$table->string('id')->primary();
			$table->string('surat_keluar_id');
			$table->string('surat_keluar_template_id')->nullable();

			$table->string('keamanan')->nullable();
			$table->string('kode_arsip')->nullable();
			$table->string('nomor_surat')->nullable();
			$table->string('nomor_tahun')->nullable();
			$table->string('instruksi')->nullable();
			$table->string('nama_jabatan')->nullable();
			$table->text('isi', 65535)->nullable();
			$table->text('penerima_instruksi', 65535)->nullable();
			$table->text('arahan_instruksi', 65535)->nullable();
			$table->text('penandatangan', 65535)->nullable();
			$table->date('tanggal_surat')->nullable();
            $table->dateTime('tanggal_approve')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_surat_keluar_template_010');
	}

}

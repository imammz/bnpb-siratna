<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDisposisiCatatanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_disposisi_catatan', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nama')->nullable();
            $table->enum('kategori', array('surat_masuk','kpa'))->nullable()->comment="'surat_masuk','kpa'";
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_disposisi_catatan');
	}

}

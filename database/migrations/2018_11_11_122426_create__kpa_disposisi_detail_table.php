<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKpaDisposisiDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_kpa_disposisi_detail', function(Blueprint $table)
		{
			$table->string('id')->primary();

			$table->string('nomor_surat')->nullable();
			$table->dateTime('tanggal_surat')->nullable();
			$table->text('perihal', 65535)->nullable();
			$table->integer('nominal')->unsigned()->nullable();

			$table->dateTime('tanggal_diterima')->nullable();
			$table->string('kpa_disposisi_id')->nullable();
			$table->enum('status', array('active','inactive'))->nullable()->comment="'active','inactive'";
            $table->string('created_by_divisi_id')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_kpa_disposisi_detail');
	}

}

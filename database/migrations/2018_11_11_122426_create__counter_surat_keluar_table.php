<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCounterSuratKeluarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_counter_surat_keluar', function(Blueprint $table)
		{
			$table->string('nama_table');
			$table->date('tanggal')->nullable();
			$table->string('prefix')->nullable();
			$table->integer('jumlah');
			$table->integer('counter_hari')->nullable();
            $table->integer('counter')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_counter_surat_keluar');
	}

}

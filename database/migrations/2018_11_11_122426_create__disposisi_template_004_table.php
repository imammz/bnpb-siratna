<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDisposisiTemplate004Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_disposisi_template_004', function(Blueprint $table)
		{
			$table->string('id')->primary();
			$table->string('disposisi_id')->nullable();
			$table->string('disposisi_template_id')->nullable();
			$table->string('klasifikasi')->nullable();
			$table->string('pengirim')->nullable();
			$table->string('nomor_surat_masuk', 100)->nullable();
			$table->dateTime('tanggal_surat_masuk')->nullable();
			$table->dateTime('tanggal_diterima')->nullable();
			$table->string('nomor_agenda')->nullable();
			$table->string('retro')->nullable();
			$table->text('perihal', 65535)->nullable();
			$table->string('kepada_divisi_id')->nullable();
            $table->string('kepada_divisi_nama')->nullable();
            $table->string('kepada_id_user')->nullable();
			$table->integer('kepada_id_struktur_organisasi')->nullable();
            $table->integer('kepada_id_unit_kerja')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_disposisi_template_004');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgendaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_agenda', function(Blueprint $table)
		{
			$table->string('id')->primary();

			$table->string('nama_kegiatan');
			$table->string('tempat')->nullable();
			$table->dateTime('tanggal_mulai')->nullable();
			$table->dateTime('tanggal_akhir')->nullable();
			$table->text('keterangan', 65535)->nullable();
			$table->string('penanggung_jawab')->nullable();
			$table->enum('status', array('planned','onprogress','finished','canceled','hold','deleted'))->nullable()->comment = "'planned','onprogress','finished','canceled','hold','deleted'";
			$table->text('catatan', 65535)->nullable();
			$table->string('created_by_username')->nullable();
			$table->string('created_by_nama')->nullable();
			$table->string('update_agenda')->nullable();
            $table->string('created_by_id_user')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_agenda');
	}

}

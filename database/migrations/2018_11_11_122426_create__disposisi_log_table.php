<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDisposisiLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_disposisi_log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('disposisi_id');

			$table->text('data', 65535)->nullable();
			$table->text('referensi', 65535)->nullable();


			$table->string('created_by_username');
			$table->string('created_by_nama')->nullable();
            $table->enum('tipe', array('disposisi','catatan'))->nullable()->default('disposisi')->comment="'disposisi','catatan'";
            $table->string('id_user')->nullable();
			$table->integer('id_struktur_organisasi')->nullable();
            $table->integer('id_unit_kerja')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_disposisi_log');
	}

}

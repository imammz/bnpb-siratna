<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPegawaiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pegawai', function(Blueprint $table)
		{
			$table->foreign('id_struktur_organisasi', 'fk_pegawai_struktur_organisasi1')->references('id')->on('struktur_organisasi')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_unit_kerja', 'fk_pegawai_unit_kerja1')->references('id')->on('unit_kerja')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_user', 'fk_pegawai_users')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pegawai', function(Blueprint $table)
		{
			$table->dropForeign('fk_pegawai_struktur_organisasi1');
			$table->dropForeign('fk_pegawai_unit_kerja1');
			$table->dropForeign('fk_pegawai_users');
		});
	}

}

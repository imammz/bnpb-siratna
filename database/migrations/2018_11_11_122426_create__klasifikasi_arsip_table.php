<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKlasifikasiArsipTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_klasifikasi_arsip', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code')->nullable();
			$table->string('nama')->nullable();
            $table->integer('level')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_klasifikasi_arsip');
	}

}

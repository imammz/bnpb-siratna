<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCounterTahunanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_counter_tahunan', function(Blueprint $table)
		{
			$table->string('nama_table');
			$table->string('tahun', 100)->nullable();
			$table->string('prefix')->nullable();
            $table->integer('jumlah');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_counter_tahunan');
	}

}

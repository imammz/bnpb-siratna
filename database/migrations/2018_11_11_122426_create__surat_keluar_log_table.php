<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuratKeluarLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_surat_keluar_log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('surat_keluar_id')->nullable();

			$table->text('data', 65535)->nullable();
			$table->text('referensi', 65535)->nullable();

			$table->string('created_by_username')->nullable();
			$table->string('created_by_nama')->nullable();
            $table->string('divisi_id_penerima')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_surat_keluar_log');
	}

}

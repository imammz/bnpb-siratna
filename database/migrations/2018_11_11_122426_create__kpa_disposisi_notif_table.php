<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKpaDisposisiNotifTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_kpa_disposisi_notif', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('disposisi_id')->nullable();

			$table->string('url')->nullable();
			$table->string('nama')->nullable();
			$table->enum('status', array('read','unread'))->nullable()->default('unread')->comment="'read','unread'";
			$table->enum('kategori', array('disposisi','arahan','created'))->nullable()->comment="'disposisi','arahan','created'";
            $table->dateTime('tanggal_approve')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_kpa_disposisi_notif');
	}

}

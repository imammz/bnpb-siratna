<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\Uuid;

/**
 * Class User
 *
 * @property string $id
 * @property int $id_role
 * @property string $email
 * @property \Carbon\Carbon $email_verified_at
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Role $role
 * @property \Illuminate\Database\Eloquent\Collection $pegawais
 *
 * @package App\Models
 */
class User extends Authenticatable
{

    use HasApiTokens,Notifiable;
    use Uuid;

	public $incrementing = false;

	protected $casts = [
		'id_role' => 'int'
	];

	protected $dates = [
		'email_verified_at'
	];

	protected $hidden = [
		'password',
        'remember_token'
	];

	protected $fillable = [
		'id',
        'nama',
		'id_role',
		'email',
		'email_verified_at',
		'password',
        'remember_token',
        'username',
        'sta_aktif',
        'api_token'
	];

	public function role()
	{
		return $this->belongsTo(\App\Models\Role::class, 'id_role');
	}

	public function pegawai()
	{
		return $this->hasOne(\App\Models\Pegawai::class, 'id_user');
    }


}

<?php

namespace App\Http\Controllers;

use App\Models\Pegawai;
use App\Models\Role;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid as Uuid;
use Illuminate\Support\Facades\Validator;
use DB;
use DataTables;
use Auth;

class UsermanagementController extends Controller
{
    //
    public function index()
    {


        $data = [];
        $data['jmlPegawai'] = User::where('sta_aktif',1)->count();
        $data['jmlRole'] = Role::count();
        $data['jmlUnitKerja'] = UnitKerja::where('status_aktif',1)->count();

        $data['ka'] = UnitKerja::where('status_aktif',1)->where('eselon','=',0)->get();


        return view('usermanagement.index',$data);
    }

    public function tambahuser($kode) {
        $data = [];
        $data['id_unit_kerja'] = $kode;
        $data['unit'] = UnitKerja::where('kode',$kode)->first();
        $data['role'] = Role::where('id','>',2)->get();

        return view('usermanagement.tambahuser',$data);
    }

    public function tambahuserProses(Request $request) {

      $user =  User::create([
            'id' => Uuid::uuid4(),
            'nama' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'id_role' => $request->id_role,
            'password' => Hash::make($request->password),
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        Pegawai::create([
            'id' => Uuid::uuid4(),
            'nip' => $request->username,
            'tanggal_lahir' => $request->tanggal_lahir,
            'tempat_lahir' => $request->tempat_lahir,
            'id_user' => $user->id,
            'id_unit_kerja' => $request->id_unit_kerja,
            'sta_level' => $request->sta_level,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $request->session()->flash('pesan', 'Data Pengguna Berhasil Ditambah');

        return redirect('/usermanagement/tambahuser/'.$request->id_unit_kerja);


    }



    public function edituser($id,$kode,$edit=0) {
        $data = [];
        $data['id_unit_kerja'] = $kode;
        $data['unit'] = UnitKerja::where('kode',$kode)->first();
        $data['role'] = Role::where('id','>',2)->get();
        $data['pegawai'] = Pegawai::where('id_user',$id)->first();
        $data['edit'] = $edit;

        $unitkerja = UnitKerja::select('kode','prefix', 'nama_unit_kerja')->where('flag_tujuan_surat', 't')->orderBy('parent_kode')
            ->orderBy('kode')->get();
        $select_unit_kerja = [];
        foreach ($unitkerja as $row) {
            $select_unit_kerja += [$row->kode => '('.$row['prefix'].') '.$row['nama_unit_kerja']];
        }

        $data['select_unit'] = $select_unit_kerja;

        return view('usermanagement.edituser',$data);
    }

    public function edituserProses(Request $request) {


        $data = Pegawai::where('id',$request->id_pegawai);
        $data->update(['tanggal_lahir' => $request->tanggal_lahir
        ,'tempat_lahir' => $request->tempat_lahir
        ,'id_unit_kerja' => $request->id_unit_kerja
        ,'nip' => $request->nip
        ,'updated_at' => date('Y-m-d H:i:s')]);

        $data2 = User::where('id',$request->id_user);
        $data2->update([
        'email' => $request->email
        ,'nama' => $request->nama
        ,'updated_at' => date('Y-m-d H:i:s')]);


        $request->session()->flash('pesan', 'Data Pengguna Berhasil Diedit');

        return redirect('/usermanagement/edituser/'.$request->id_user.'/'.$request->id_unit_kerja.'/0');


    }

    public function hapususerProses(Request $request, $id_user,$id_unit_kerja) {



        $data2 = User::where('id',$id_user);
        $data2->update([
        'sta_aktif' => 0
        ,'updated_at' => date('Y-m-d H:i:s')]);

        $request->session()->flash('pesan', 'Data Pengguna Berhasil Dihapus');

        return redirect('/usermanagement/edituser/'.$id_user.'/'.$id_unit_kerja.'/1');


    }

    public function datapegawai()
    {
        $data = [];
        $data['pegawai'] = User::orderBy('id_role')->with(['pegawai'=>function($q){
                $q->orderBy('id_unit_kerja');
                $q->orderBy('sta_level');
        }]);
        return view('usermanagement.datapegawai', $data);
    }

    public function roleakses()
    {
        $data = [];
        return view('usermanagement.roleakses', $data);
    }

    public function unitkerja()
    {
        $data = [];
        return view('usermanagement.unitkerja', $data);
    }


    public function apiGetUnitKerja($kode_unit_kerja = null) {

        if($kode_unit_kerja == null) {
            $unikerja =  UnitKerja::orderBy('eselon','asc')
                                  ->orderBy('parent_kode','asc')
                                  ->orderBy('kode','asc')->get();

            return response()->json(['status'=>true,'msg'=>'Berhasil','data' => $unikerja], 200);
        }
        else {
            $unikerja =  UnitKerja::orderBy('eselon','asc')
            ->where('kode',$kode_unit_kerja)
            ->orderBy('parent_kode','asc')
            ->orderBy('kode','asc')->first();

            return response()->json(['status'=>true,'msg'=>'Berhasil','data' => $unikerja], 200);
        }

    }
    public function apiGetUnitKerjaByEselon($eselon = null) {

        if($eselon == null) {

            return response()->json(['status'=>true,'msg'=>'Eselon Harus Diisi'], 400);
        }
        else {
            $unikerja =  UnitKerja::orderBy('eselon','asc')
            ->where('eselon','=',$eselon-1)
            ->orderBy('parent_kode','asc')
            ->orderBy('kode','asc')->get();

            return response()->json(['status'=>true,'msg'=>'Berhasil','data' => $unikerja], 200);
        }

    }

    public function apiPostUnitKerja(Request $request) {

        $validator = Validator::make(request()->all(), [
            // 'keterangan' => 'required|max:200',
            'nama_unit_kerja' => 'required|min:3',
            'eselon' => 'required|numeric',
            'parent_kode' => 'required',
            'nama_jabatan' => 'required'
        ]);

        if ($validator->fails()) {
                return response()->json(['status'=>false,'msg'=>'Tidak memenuhi syarat rule validasi','error' => $validator->errors()], 400);
        } else {


            $kode = $this->_genKodeJabatan();

            $data = new UnitKerja;
            $data->pev_unit_kerja = $kode;
            $data->nama_unit_kerja = $request->nama_unit_kerja;
            $data->prefix = $request->prefix;
            $data->eselon = $request->eselon;
            $data->parent_kode = $request->parent_kode;
            $data->status_aktif = 1;
            $data->kode = $kode;
            $data->flag_tujuan_surat = true;
            $data->flag_sekretaris = false;
            $data->nama_jabatan = $request->nama_jabatan;
            $data->save();

            return response()->json(['status'=>true,'msg'=>'Unit Kerja Berhasil Ditambah','data' => $data], 200);

        }



    }


    public function apiPutUnitKerja(Request $request) {


        $validator = Validator::make(request()->all(), [
            'nama_unit_kerja' => 'required|min:3',
            'eselon' => 'required|numeric',
            'parent_kode' => 'required',
            'nama_jabatan' => 'required',
            'kode' => 'required'
        ]);

        if ($validator->fails()) {
                return response()->json(['status'=>false,'msg'=>'Tidak memenuhi syarat rule validasi','error' => $validator->errors()], 400);
        } else {


            $data = UnitKerja::where('kode',$request->kode)->first();
            $data->nama_unit_kerja = $request->nama_unit_kerja;
            $data->prefix = $request->prefix;
            $data->eselon = $request->eselon;
            $data->parent_kode = $request->parent_kode;
            $data->status_aktif = 1;
            $data->flag_tujuan_surat = true;
            $data->flag_sekretaris = false;
            $data->nama_jabatan = $request->nama_jabatan;
            $data->save();

            return response()->json(['status'=>true,'msg'=>'Unit Kerja Berhasil Diedit','data' => $data], 200);

        }



    }


    public function hapusUnitKerja($id_unit_kerja) {
         
        
        $data = Pegawai::where('id_unit_kerja',$id_unit_kerja);
        $data->update([
            'id_unit_kerja' => '0184'
            ,'updated_at' => date('Y-m-d H:i:s')]);   


        $data = UnitKerja::where('kode',$id_unit_kerja);
        $data->update([
            'status_aktif' => 0
            ,'updated_at' => date('Y-m-d H:i:s')]);  

        
        return redirect('to/usermanagement/');

    }


    private function _genKodeJabatan() {
        $q = DB::select("select right(concat('0000',cast(max(kode) as INTEGER)+1),4) as maks from unit_kerja where kode <> '9999';");
        $kode = $q[0];

        $kode = $kode->maks;

        return $kode;
    }

}

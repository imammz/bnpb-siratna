<?php

namespace App\Http\Controllers;

use App\Models\DisposisiLog;
use Illuminate\Http\Request;
use DB;
use DataTables;
use App\Models\Disposisi as Disposisi;
use Auth;
use Ramsey\Uuid\Uuid as Uuid;
use App\Models\UnitKerja;
use App\models\DisposisiKlasifikasi;
use App\models\DisposisiKeteranganLampiran;
use App\Models\DisposisiNotif;
use App\Models\Retensi;
use App\Models\Agenda;
use App\Models\AgendaDetail;
use App\Models\AgendaUser;
use Illuminate\Support\Facades\Validator;
use App\Models\Pegawai;
use App\Models\DisposisiLampiran;
use QrCode;
use PDF;
use Log;



class SuratMasukController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function tambah()
    {

        $id = Uuid::uuid4();

        $data = [];
        $data['gen'] = $id->toString();

        $unitkerja = UnitKerja::select('kode', 'nama_jabatan')->where('flag_tujuan_surat', 't')->orderBy('parent_kode')
                     ->orderBy('kode')->get();
        $select_unit_kerja = [];
        foreach ($unitkerja as $row) {
            $select_unit_kerja += [$row->kode => $row['nama_jabatan']];
        }

        $klasifikasi = DisposisiKlasifikasi::orderBy('id')->where('flag_aktif', 't')->get();
        $select_klasifikasi = [];
        foreach ($klasifikasi as $row) {
            $select_klasifikasi += [$row->id => $row['klasifikasi']];
        }

        $keteranggan_lampiran = DisposisiKeteranganLampiran::where('flag_aktif', 't')->orderBy('id')->get();
        $select_keteranggan_lampiran = [];
        foreach ($keteranggan_lampiran as $row) {
            $select_keteranggan_lampiran += [$row->id => $row['keterangan_lampiran']];
        }

        $jra = Retensi::orderby('id')->get();
        $select_jra = [];
        foreach ($jra as $row) {
            $select_jra += [$row->id => $row['nama'] . " ({$row['retensi_keterangan']})"];
        }



        $unitkerja_teruskan = UnitKerja::select('kode', 'nama_jabatan')->where('flag_tujuan_surat', 't')->orderBy('id')->get();
        $select_unit_kerja_teruskan = [];
        foreach ($unitkerja_teruskan as $row) {
            $select_unit_kerja_teruskan += [$row->kode => $row['nama_jabatan']];
        }


        $data['unitkerja'] = $select_unit_kerja;
        $data['unitkerja_teruskan'] = $select_unit_kerja_teruskan;
        $data['klasifikasi'] = $select_klasifikasi;
        $data['keteranggan_lampiran'] = $select_keteranggan_lampiran;
        $data['jra'] = $select_jra;




        return view('suratmasuk.tambah', $data);

    }


    public function tambahProses(Request $request)
    {
        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        $pegawai = Auth::user()->pegawai;
        $pev = $pegawai->unit_kerja->pev_unit_kerja;
        $pev = Auth::user()->pegawai->unit_kerja->pev_unit_kerja;
        $no_agenda = '';
        $counter = Disposisi::where('nomor_agenda', 'like', "%{$no_agenda}%")->count();
        //dd($counter);
        $counter = $counter + 1;
        $counter = sprintf('%08d', $counter);
        $no_agenda .= $counter.'/'.date('Y');



        $validator = Validator::make(request()->all(), [
            // 'keterangan' => 'required|max:200',
            'kepada' => 'required',
            'diteruskan_kepada' => 'required',
            'diteruskan_kepada' => 'required',
            'perihal' => 'required',
            'tgl_pelaksanaan' => 'required',
            'tgl_diterima' => 'required',
            'tgl_surat' => 'required',
            'nosurat' => 'required',
            'pengirim' => 'required',
            'kepada' => 'required',
            'file_attachment' => 'required',
//            'file_attachment.*' => 'mimes:jpg,jpeg,png,pdf,docx,doc,zip,xlsx,xls'
            'file_attachment.*' => 'mimes:jpg,jpeg,png,pdf,PDF,PNG,JPG,JPEG'
        ]);

        if ($validator->fails()) {
            return redirect('to/suratmasuk-tambah/')
                ->withErrors($validator->errors());
        } else {


            try {


                DB::transaction(function () use($request,$no_agenda) {

            $oleh_unitkerja_id = Auth::user()->pegawai->id_unit_kerja;
            $oleh_unitkerja = UnitKerja::where('kode', $oleh_unitkerja_id)->first();
            $kepada = UnitKerja::where('kode', $request->kepada)->first();
            $retensi = Retensi::where('id', $request->retensi)->first();
            $keterangan_lampiran = DisposisiKeteranganLampiran::where('id', $request->keterangan_lampiran)->first();
            $klasifikasi = DisposisiKlasifikasi::where('id', $request->klasifikasi)->first();

            $data = new Disposisi;
            $data->id = $request->gen;
            $data->oleh_unitkerja_id = $oleh_unitkerja_id;
            $data->oleh_unitkerja_nama = $oleh_unitkerja->nama_unit_kerja;
            $data->nama = 'Surat Masuk';
            $data->pengirim = $request->pengirim;
            $data->pengirim_nomor_surat = $request->nosurat;
            $data->status = 'active';
            $data->created_by_username = Auth::user()->username;
            $data->created_by_nama = Auth::user()->nama;
            $data->kepada_unitkerja_id = $request->kepada;
            $data->kepada_unitkerja_nama = $kepada->nama_unit_kerja;
            $data->status_progress = 'proses';
            $data->tanggal_pelaksanaan = $request->tgl_pelaksanaan;

            if(isset($retensi->retensi_active)) {
                $data->retensi_active = $retensi->retensi_active;
                $data->retensi_inactive = $retensi->retensi_inactive;
                $data->retensi_keterangan = $retensi->retensi_keterangan;
            }

            if(isset($keterangan_lampiran->keterangan_lampiran)) {
                $data->keterangan_lampiran = $keterangan_lampiran->keterangan_lampiran;
            }
            $data->oleh_id_user = Auth::user()->id;
            if(isset($klasifikasi->klasifikasi)) {
                $data->klasifikasi = $klasifikasi->klasifikasi;
            }

            $data->tanggal_surat_masuk = $request->tgl_surat;
            $data->tanggal_diterima = $request->tgl_diterima;
            $data->nomor_agenda = $no_agenda;
            $data->perihal = $request->perihal;
            $data->sumber = 2;
            $data->save();

            $diteruskan = $request->diteruskan_kepada;

            $agenda = new agenda;
            $agenda->nama_kegiatan = $data->perihal;
		    $agenda->tanggal_mulai = $data->tanggal_pelaksanaan;
		    $agenda->keterangan = 'Surat Dari : '.$data->pengirim;
		    $agenda->status = 'planned';
		    $agenda->created_by_username = Auth::user()->username;
		    $agenda->created_by_nama = Auth::user()->nama;
		    $agenda->created_by_id_user = Auth::user()->id;
            $agenda->id = Uuid::uuid4();
            $agenda->save();


            foreach ($diteruskan as $row) {

                $kepada = Pegawai::where('id_unit_kerja', $row)->where('sta_level', 1)->first();

                $data = new DisposisiNotif;
                $data->disposisi_id = $request->gen;
                $data->dari_id_unit_kerja = $oleh_unitkerja_id;
                $data->url = 'disposisi_template_004';
                $data->nama = $oleh_unitkerja->nama_unit_kerja;
                $data->status = 'meneruskan';
                $data->kategori = 'arahan';
                $data->dari_id_user = Auth::user()->id;
                $data->kepada_id_user = $kepada->user->id;
                $data->kepada_id_unit_kerja = $row;
                $data->save();

                $agendaUser = new AgendaUser;
                $agendaUser->agenda_id = $agenda->id;
                $agendaUser->id_user = $kepada->user->id;
                $agendaUser->save();

            }

            $this->uploadFiles($request,'utama',$agenda->id);

        });



            $request->session()->flash('pesan', 'Surat Masuk Berhasil Ditambah');

        }
        catch (Exception $e) {
            report($e);
            $request->session()->flash('pesan', 'Error - Surat Masuk Gagal Ditambah');
        }

        }

        return redirect('to/suratmasuk-tambah/');

    }

    public function uploadFiles($request,$kategori = 'utama',$agenda_id = null) {
        $destinationPath = public_path('/lampiran/suratmasuk');

        if($request->hasfile('file_attachment'))
        {
            foreach ($request->file('file_attachment') as $row) {

                if ($row->getClientOriginalName() != null) {
                    $file_name = md5(rand(1000, 9999) . $row->getClientOriginalName() . time())
                        . '.' . $row->getClientOriginalExtension();
                    try {
                        $row->move($destinationPath, $file_name);
                    }
                    catch (\Exception $e) {
                        Log::error('[AT upload files surat masuk: ' .date('Y-m-d H:i:s')  . '] : ' . $e->getMessage());
                    }
                }

                $oleh_unitkerja_id = Auth::user()->pegawai->id_unit_kerja;

                $data = new DisposisiLampiran;
                $data->disposisi_id = $request->gen;
                $data->nama_file = $file_name;
                $data->created_by_username = Auth::user()->username;
                $data->created_by_unitkerja_id = $oleh_unitkerja_id;
                $data->kategori = $kategori;
                $data->save();

                if($agenda_id != null) {
                    $agendaDetail = new AgendaDetail;
                    $agendaDetail->agenda_id = $agenda_id;
                    $agendaDetail->nama_file = $file_name;
                    $agendaDetail->created_by_username = Auth::user()->username;
                    $agendaDetail->save();


                }


                try {
                    $this->QRCode($request->gen);

                }
                catch (\Exception $e) {
                    Log::error('[Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $e->getMessage());
                }

            }
        }
    }


    public function daftar($flag = null)
    {

        $data = [];
        $data['jml_dibaca'] = Disposisi::where('status_progress', 'proses')
            ->where('status', 'active')->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
                $q->whereNotIn('status',['terkirim','meneruskan']);
            })->count();
        $data['jml_belum_dibaca'] = Disposisi::where('status_progress', 'proses')
            ->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
                $q->whereIn('status',['terkirim','meneruskan']);
            })
            ->where('status', 'active')->count();

        $data['jml_ditandai'] = Disposisi::where('status_progress', 'proses')
            ->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
                $q->orWhere('dari_id_user', Auth::user()->id);
            })
            ->wherehas('notif', function ($q) {
                $q->where('bookmarked', true);
            })
            ->where('status', 'active')->count();
        $data['flag'] = $flag;

        return view('suratmasuk.daftar', $data);


    }

    public function daftarData(Request $request,$flag = null)
    {

        $start = $request->get('start');
        $disposisi = Disposisi::where('status_progress', 'proses');
        $notif = [];

        if($flag=='tandai') {
            $disposisi->wherehas('notif', function ($q) {
                $q->where('bookmarked', true);
            })->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
                $q->orWhere('dari_id_user', Auth::user()->id);
            });
        }
        else {
            $disposisi->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
                $q->orWhere('dari_id_user', Auth::user()->id);
            });
        }


        if($flag=='sudah-dibaca') {
            $disposisi->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
                $q->whereNotIn('status',['terkirim','meneruskan']);
            });
        }
        else {
            $disposisi->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
                $q->orWhere('dari_id_user', Auth::user()->id);
            });
        }

        if($flag=='belum-dibaca') {
            $disposisi->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
                $q->whereIn('status',['terkirim','meneruskan']);
            });
        }
        else {
            $disposisi->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
                $q->orWhere('dari_id_user', Auth::user()->id);
            });
        }


        return Datatables::of(
            $disposisi->where('status', 'active')

                ->orderBy('created_at', 'desc')
        )->addIndexColumn()
            ->addColumn('no', function ($model) use ($start) {
                $no = $start + 1;
                return '<div style="text-align:right">' . $no . '</div>';
            })
            ->editColumn('pengirim', function ($d) {
                $notif = DisposisiNotif::where('disposisi_id', $d->id)
                    ->where(function ($query) {
                        $query->where('kepada_id_user', Auth::user()->id);
                        $query->orWhere('dari_id_user', Auth::user()->id);
                    })->first();
                $bold = ($notif->status == 'terkirim' OR $notif->status == 'meneruskan') ? 'font-weight-bold' : '';
                $marked = ($notif->bookmarked) ? 'text-danger font-weight-bold' : '';

                return  '<a  href="#"  data-toggle="modal"
                                    data-target="#modal-1" data-whatever="'.$d->id.'" class="text-small">' .'<div class="text-small ' . $bold . ' '.$marked. '">' . strtoupper($d->pengirim) . '</div>
                        </a>';
            }, 1)
            ->editColumn('pengirim_nomor_surat', function ($d) {
                $notif = DisposisiNotif::where('disposisi_id', $d->id)
                    ->where(function ($query) {
                        $query->where('kepada_id_user', Auth::user()->id);
                        $query->orWhere('dari_id_user', Auth::user()->id);
                    })
                    ->first();
                $bold = ($notif->status == 'terkirim' OR $notif->status == 'meneruskan') ? 'font-weight-bold' : '';
                $marked = ($notif->bookmarked) ? 'text-danger font-weight-bold' : '';

                return '<a  href="#"  data-toggle="modal"
                                    data-target="#modal-1" data-whatever="'.$d->id.'" class="text-small">' . strtoupper($d->pengirim_nomor_surat) . '</a>';
            }, 1)
            ->editColumn('perihal', function ($d) {
                $notif = DisposisiNotif::where('disposisi_id', $d->id)->where(function ($query) {
                    $query->where('kepada_id_user', Auth::user()->id);
                    $query->orWhere('dari_id_user', Auth::user()->id);
                })->first();

                //dd($notif);
                $bold = ($notif->status == 'terkirim' OR $notif->status == 'meneruskan') ? 'font-weight-bold' : '';
                $marked = ($notif->bookmarked) ? 'text-danger font-weight-bold' : '';

                return  '<a  href="#"  data-toggle="modal"
                                    data-target="#modal-1" data-whatever="'.$d->id.'" class="text-small">' .
                    '<div class="text-small ' . $bold .' '.$marked. '"> ' . $d->perihal . ' </div></a>';
            })
            ->addColumn('tgl_surat', function ($d) {
                return '<label class="badge badge-warning"><i class="fa fa-calendar-o"></i> '
                    . \Carbon\Carbon::parse($d->tanggal_surat_masuk)->format('d-M-Y')
                    . '</label>';
            })
            ->addColumn('dibaca', function ($d) {
                $notif = DisposisiNotif::where('disposisi_id', $d->id)->where(function ($query) {
                    $query->where('kepada_id_user', Auth::user()->id);
                    $query->orWhere('dari_id_user', Auth::user()->id);
                })->first();


                if(isset($notif->id)) {
                    if ($notif->status == 'terkirim' OR $notif->status == 'meneruskan') {
                        $dibaca =
                            '<a  href="#"  data-toggle="modal"
                                    data-target="#modal-1" data-whatever="'.$d->id.'" class="text-small">' .
                            '<div class="badge badge-pill badge-info"><i class="fa fa-envelope"></i> <span>belum</span></div></a>';

                    } else {
                        $dibaca =
                            '<a  href="#"  data-toggle="modal"
                                    data-target="#modal-1" data-whatever="'.$d->id.'" class="text-small">' .
                            '<div class="badge badge-pill badge-primary"><i class="fa fa-envelope-open"></i> <span>sudah</span> </div></a>';

                    }
                }
                else {
                    $dibaca =
                        '-';

                }

                return $dibaca;
            })

            ->addColumn('marked', function ($d) {
                $notif = DisposisiNotif::where('disposisi_id', $d->id)->where(function ($query) {
                    $query->where('kepada_id_user', Auth::user()->id);
                    $query->orWhere('dari_id_user', Auth::user()->id);
                })->first();

                if ($notif->bookmarked) {
                    $dibaca =
                        '<a  href="#" onclick="goto(\'suratmasuk-marked-batal/' . $d->id.'\')" class="text-small"> ' .
                        '<div class="badge badge-pill badge-danger"  data-toggle="tooltip" data-placement="left" title="Klik Untuk Batal Ditandai">
                        <i class="fa fa-bookmark"></i> </div></a>';

                } else {
                    $dibaca =
                        '<a  href="#" onclick="goto(\'suratmasuk-marked-proses/' . $d->id.'\')" class="text-small"> ' .
                        '<div class="badge badge-pill badge-secondary" data-toggle="tooltip" data-placement="left" title="Klik Untuk Ditandai"
                        data-toggle="tooltip" data-placement="left" title="Tambah Data Surat Masuk"> <i class="fa fa-bookmark-o"></i>  </div></a>';

                }
                return $dibaca;
            })
            ->rawColumns(['tgl_surat', 'no', 'pengirim', 'perihal', 'pengirim_nomor_surat', 'dibaca', 'marked'])

            ->make(true);

    }


    public function daftarDataSudah(Request $request,$flag = null)
    {

        $start = $request->get('start');
        $disposisi = Disposisi::where('status_progress', 'selesai');

        if($flag=='tandai') {
            $disposisi->wherehas('notif', function ($q) {
                $q->where('bookmarked', true);
            })->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
                $q->orWhere('dari_id_user', Auth::user()->id);
            });
        }
        else {
            $disposisi->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
                $q->orWhere('dari_id_user', Auth::user()->id);
            });
        }



        return Datatables::of(
            $disposisi->where('status', 'active')

                ->orderBy('created_at', 'desc')
        )->addIndexColumn()
            ->addColumn('no', function ($model) use ($start) {
                $no = $start + 1;
                return '<div style="text-align:right">' . $no . '</div>';
            })
            ->editColumn('pengirim', function ($d) {
                $notif = DisposisiNotif::where('disposisi_id', $d->id)
                    ->where(function ($query) {
                        $query->where('kepada_id_user', Auth::user()->id);
                        $query->orWhere('dari_id_user', Auth::user()->id);
                    })->first();
                $bold = ($notif->status == 'terkirim') ? 'font-weight-bold' : '';
                $marked = ($notif->bookmarked) ? 'text-danger font-weight-bold' : '';

                return  '<a  href="' . url('to/suratmasuk-daftar-proses/' . $d->id) . '" class="text-small">' .
                    '<div class="text-small ' . $bold . '">' . strtoupper($d->pengirim) . '</div></a>
                        ';
            }, 1)
            ->editColumn('pengirim_nomor_surat', function ($d) {
                $notif = DisposisiNotif::where('disposisi_id', $d->id)
                    ->where(function ($query) {
                        $query->where('kepada_id_user', Auth::user()->id);
                        $query->orWhere('dari_id_user', Auth::user()->id);
                    })
                    ->first();
                $bold = ($notif->status == 'terkirim') ? 'font-weight-bold' : '';
                $marked = ($notif->bookmarked) ? 'text-danger font-weight-bold' : '';

                return '<a class="' . $bold . ' text-small"" href="' . url('to/suratmasuk-daftar-proses/' . $d->id) . '">' . strtoupper($d->pengirim_nomor_surat) . '</a>
                        ';
            }, 1)
            ->editColumn('perihal', function ($d) {
                $notif = DisposisiNotif::where('disposisi_id', $d->id)->where(function ($query) {
                    $query->where('kepada_id_user', Auth::user()->id);
                    $query->orWhere('dari_id_user', Auth::user()->id);
                })->first();
                $bold = ($notif->status == 'terkirim') ? 'font-weight-bold' : '';
                $marked = ($notif->bookmarked) ? 'text-danger font-weight-bold' : '';

                return  '<a  href="' . url('to/suratmasuk-daftar-proses/' . $d->id) . '" class="text-small">' .
                    '<div class="text-small ' . $bold . '">' . $d->perihal . '</div></a>';
            })
            ->addColumn('tgl_surat', function ($d) {
                return '<label class="badge badge-warning"><i class="fa fa-calendar-o"></i> '
                    . \Carbon\Carbon::parse($d->tanggal_surat_masuk)->format('d-M-Y')
                    . '</label>';
            })

            ->addColumn('marked', function ($d) {
                $notif = DisposisiNotif::where('disposisi_id', $d->id)->where(function ($query) {
                    $query->where('kepada_id_user', Auth::user()->id);
                    $query->orWhere('dari_id_user', Auth::user()->id);
                })->first();

                if ($notif->bookmarked) {
                    $dibaca =
                        '<a  href="#" onclick="goto(\'suratmasuk-marked-proses/' . $d->id.'\')" class="text-small"> ' .
                        '<div class="badge badge-pill badge-danger"  data-toggle="tooltip" data-placement="left" title="Klik Untuk Batal Ditandai">
                        <i class="fa fa-bookmark"></i> </div></a>';

                } else {
                    $dibaca =
                        '<a  href="#" onclick="goto(\'suratmasuk-marked-batal/' . $d->id.'\')" class="text-small"> ' .
                        '<div class="badge badge-pill badge-secondary" data-toggle="tooltip" data-placement="left" title="Klik Untuk Ditandai"
                        data-toggle="tooltip" data-placement="left" title="Tambah Data Surat Masuk"> <i class="fa fa-bookmark-o"></i>  </div></a>';

                }
                return $dibaca;
            })

            ->addColumn('dibaca', function ($d) {
                $notif = DisposisiNotif::where('disposisi_id', $d->id)->where(function ($query) {
                    $query->where('kepada_id_user', Auth::user()->id);
                })->first();

                if(isset($notif->status)) {
                    if ($notif->status == 'terkirim') {
                        $dibaca =
                            '<a  href="' . url('to/suratmasuk-daftar-proses/' . $d->id) . '" class="text-small">' .
                            '<div class="badge badge-pill badge-info"><i class="fa fa-envelope"></i> <span>belum</span></div></a>';

                    } else {
                        $dibaca =
                            '<a  href="' . url('to/suratmasuk-daftar-proses/' . $d->id) . '" class="text-small"> ' .
                            '<div class="badge badge-pill badge-primary"><i class="fa fa-envelope-open"></i> <span>sudah</span> </div></a>';

                    }
                }
                else {
                    $dibaca =
                        '-';

                }

                return $dibaca;
            })

            ->rawColumns(['tgl_surat', 'no', 'pengirim', 'perihal', 'pengirim_nomor_surat',  'marked','dibaca'])
            ->make(true);

    }

    public function markedProses($id)
    {
        $notif = DisposisiNotif::where('disposisi_id', $id)->where(function ($query) {
            $query->where('kepada_id_user', Auth::user()->id);
            $query->orWhere('dari_id_user', Auth::user()->id);
        });
        //dd($notif->toSql());
        $notif->update(['bookmarked' => true]);

        //return back();

        return view('redirect',['url'=>'suratmasuk-daftar']);

    }

    public function markedBatal($id)
    {
        $notif = DisposisiNotif::where('disposisi_id', $id)->where(function ($query) {
            $query->where('kepada_id_user', Auth::user()->id);
            $query->orWhere('dari_id_user', Auth::user()->id);
        });

        $notif->update(['bookmarked' => false]);

        return view('redirect',['url'=>'suratmasuk-daftar']);
    }


    public function suratProses($id)
    {
        $this->QRCode($id);

        $notif = DisposisiNotif::where('disposisi_id', $id)->where('kepada_id_user', Auth::user()->id);
        $notif->update(['status' => 'dibaca']);

        $data = [];
        $data['suratmasuk'] = Disposisi::where('id', $id)->first();
        $data['files_images'] = DisposisiLampiran::where('disposisi_id',$id)->where('kategori','utama')
            ->whereNotIn(DB::raw('RIGHT(nama_file,3)'),['pdf','PDF'])->get();
        $data['files_pdf'] = DisposisiLampiran::where('disposisi_id',$id)->where('kategori','utama')
            ->whereIn(DB::raw('RIGHT(nama_file,3)'),['pdf','PDF'])->get();
        $data['files_lampiran_catatan'] = DisposisiLampiran::where('disposisi_id',$id)->where('kategori','support')->get();



        return response()->view('suratmasuk.detail', $data);
    }


    public function detailQRCode($id) {
        $surat = Disposisi::where('id',$id)->first();

        $data = [];
        $data['surat'] = $surat;

        return view('suratmasuk.detailcode',$data);

    }

    public function QRCode($id) {
        QRCode::format('png')->size(400)->merge(storage_path('qr.png'),.08,true)
            ->generate(url('suratmasuk-code/'.$id),public_path('/qrcode/'.$id.'.png'));
    }

    public function disposisi($id)
    {
        if(Auth::user()->pegawai->id_unit_kerja=='000001'
            or Auth::user()->pegawai->id_unit_kerja == '000003')  {

            $unitkerja = Pegawai::with(['unit_kerja' => function($q){
                $q->orderBy('parent_kode');
            }])
                ->where('id_user','<>',Auth::user()->id)
                ->where('id_unit_kerja','<>','000001')
                ->orderBy('id_unit_kerja')->orderBy('sta_level')->get();
        }
        else {
            $unitkerja = Pegawai::wherehas('unit_kerja',function($q){
                $q->where('parent_kode',Auth::user()->pegawai->id_unit_kerja);
                $q->orderBy('parent_kode');
            })->where('id_user','<>',Auth::user()->id)
                ->orderBy('id_unit_kerja')->orderBy('sta_level')->get();
        }



        $select_unit_kerja_kepada = [];
        foreach ($unitkerja as $row) {

            $getUnitKerja = '-';
            if(isset($row->unit_kerja->nama_jabatan)) {
                $getUnitKerja = $row->unit_kerja->nama_jabatan;
            }


            $level = ($row->sta_level==1)?'':'STAFF';
            $getUnitKerja = $level.' '.$getUnitKerja;
            $getUnitKerja = strtoupper($getUnitKerja);

            if(isset($row->unit_kerja->flag_sekretaris)) {
                if($row->unit_kerja->flag_sekretaris) {
                    $getUnitKerja = str_replace('KEPALA','',$getUnitKerja);
                }
            }


            $row->id_unit_kerja = str_replace(0, '', $row->id_unit_kerja);
            $select_unit_kerja_kepada += [$row->id_user => '['.$row->id_unit_kerja.'] ['.$getUnitKerja.' ] | '.
                $row->user->nama];
        }



        $select_unit_kerja_kepada += [0=>'--------------------------------------------------------------------------------------------------------------------------------------------------------'];

        if(Auth::user()->pegawai->unit_kerja->eselon == 0) {

        }

        $select_unit_kerja_kepada += [0=>'--------------------------------------------------------------------------------------------------------------------------------------------------------'];

        $data = [];
        $data['disposisi_id'] = $id;
        $data['unitkerja_kepada'] = $select_unit_kerja_kepada;
        $data['suratmasuk'] = Disposisi::where('id', $id)->first();

        return view('suratmasuk.disposisi', $data);

    }

    public function disposisiProses(Request $request) {
        $id = $request->disposisi_id;
        $catatan = [];
        $catatan['catatan'] = '';
        $catatan['kepada'] = '';

        $reff = [];

        foreach($request->kepada as $row) {
            $pegawai = Pegawai::where('id_user',$row)->first();
            $data = new DisposisiNotif;
            $data->disposisi_id = $id;
            $data->dari_id_unit_kerja = Auth::user()->pegawai->id_unit_kerja;
            $data->url = 'disposisi_template_004';
            $data->nama = Auth::user()->pegawai->unit_kerja->nama_unit_kerja;
            $data->status = 'terkirim';
            $data->kategori = 'disposisi';
            $data->dari_id_user = Auth::user()->id;
            $data->kepada_id_user = $row;
            $data->kepada_id_unit_kerja = $pegawai->id_unit_kerja;
            $data->save();

            $reff[] = ['divisiId'=>$pegawai->id_unit_kerja,'nama'=>Auth::user()->pegawai->unit_kerja->nama_unit_kerja,
                'tipe'=>'struktur'];

            $catatan['kepada'] .= '- '.$pegawai->unit_kerja->nama_unit_kerja.'('.$pegawai->user->nama.') <br/>';
        }



        if(isset($request->catatan_group)) {
            foreach($request->catatan_group as $row) {
                $catatan['catatan'] .= '- '.$row.' <br/>';
            }
        }

        $catatan['catatan'] .= '- '.$request->catatan.' <br/>';

        $data = new DisposisiLog;
        $data->disposisi_id = $id;
        $data->data = json_encode($catatan);
        $data->referensi = json_encode($reff);
        $data->dari_id_unit_kerja =  Auth::user()->pegawai->id_unit_kerja;
        $data->dari_nama_unit_kerja =  Auth::user()->pegawai->unit_kerja->nama_unit_kerja;
        $data->created_by_username =  Auth::user()->username;
        $data->created_by_nama =  Auth::user()->nama;
        $data->tipe =  'disposisi';
        $data->dari_id_user = Auth::user()->id;
        $data->save();


        return redirect(url('to/suratmasuk-daftar-proses/'.$id));


    }


    public function catatan($id)
    {
        $data = [];
        $data['suratmasuk'] = Disposisi::where('id', $id)->first();
        $data['disposisi_id'] = $id;

        return view('suratmasuk.catatan', $data);
    }

    public function catatanProses(Request $request) {
        $id = $request->disposisi_id;
        $catatan = [];
        $catatan['catatan'] = $request->catatan.' <br/>';

        $data = new DisposisiLog;
        $data->disposisi_id = $id;
        $data->data = json_encode($catatan);
        $data->dari_id_unit_kerja =  Auth::user()->pegawai->id_unit_kerja;
        $data->dari_nama_unit_kerja =  Auth::user()->pegawai->unit_kerja->nama_unit_kerja;
        $data->created_by_username =  Auth::user()->username;
        $data->created_by_nama =  Auth::user()->nama;
        $data->tipe =  'catatan';
        $data->dari_id_user = Auth::user()->id;
        $data->save();


        $this->uploadFiles($request,'support');

        return redirect(url('to/suratmasuk-daftar-proses/'.$id));

    }


    public function catatanSelesai($id)
    {
        $data = [];
        $data['suratmasuk'] = Disposisi::where('id', $id)->first();
        $data['disposisi_id'] = $id;

        return view('suratmasuk.catatan-selesai', $data);
    }

    public function catatanSelesaiProses(Request $request) {
        $id = $request->disposisi_id;
        $catatan = [];
        $catatan['catatan'] = $request->catatan.' <br/><br/>';

        $data = new DisposisiLog;
        $data->disposisi_id = $id;
        $data->data = json_encode($catatan);
        $data->dari_id_unit_kerja =  Auth::user()->pegawai->id_unit_kerja;
        $data->dari_nama_unit_kerja =  Auth::user()->pegawai->unit_kerja->nama_unit_kerja;
        $data->created_by_username =  Auth::user()->username;
        $data->created_by_nama =  Auth::user()->nama;
        $data->tipe =  'catatan';
        $data->dari_id_user = Auth::user()->id;
        $data->save();

        $data = Disposisi::where('id',$id);
        $data->update(['status_progress' => 'selesai']);

        return redirect(url('to/suratmasuk-daftar/'));
    }

    public function diteruskan($id) {

        $unitkerja = Pegawai::wherehas('unit_kerja',function($q){
            $q->where('parent_kode','<>',Auth::user()->pegawai->id_unit_kerja);
            $eselon = Auth::user()->pegawai->unit_kerja->eselon;
            $eselon = $eselon-1;
            $q->where('eselon','>',$eselon);
            $q->orderBy('eselon');
            $q->orderBy('id');
        })->where('id_user','<>',Auth::user()->id)
            ->orderBy('id_unit_kerja')->orderBy('sta_level')->get();


        $select_unit_kerja_kepada = [];
        foreach ($unitkerja as $row) {

            $getUnitKerja = '-';
            if(isset($row->unit_kerja->nama_jabatan)) {
                $getUnitKerja = $row->unit_kerja->nama_jabatan;
            }


            $level = ($row->sta_level==1)?'KEPALA':'STAFF';
            $getUnitKerja = $level.' '.$getUnitKerja;
            $getUnitKerja = strtoupper($getUnitKerja);

            if(isset($row->unit_kerja->flag_sekretaris)) {
                if($row->unit_kerja->flag_sekretaris) {
                    $getUnitKerja = str_replace('KEPALA','',$getUnitKerja);
                }
            }


            $row->id_unit_kerja = str_replace(0, '', $row->id_unit_kerja);
            $select_unit_kerja_kepada += [$row->id_user => '['.$row->id_unit_kerja.'] ['.$getUnitKerja.' ] | '.
                $row->user->nama];
        }



        $select_unit_kerja_kepada += [0=>'--------------------------------------------------------------------------------------------------------------------------------------------------------'];



        $data = [];
        $data['disposisi_id'] = $id;
        $data['unitkerja_kepada'] = $select_unit_kerja_kepada;
        $data['suratmasuk'] = Disposisi::where('id', $id)->first();

        return view('suratmasuk.diteruskan', $data);
    }


    public function diteruskanProses(Request $request) {



        $id = $request->disposisi_id;
        $reff = [];
        $catatan = [];
        $catatan['kepada'] = '';

        foreach($request->kepada as $row) {
            $pegawai = Pegawai::where('id_user',$row)->first();
            $data = new DisposisiNotif;
            $data->disposisi_id = $id;
            $data->dari_id_unit_kerja = Auth::user()->pegawai->id_unit_kerja;
            $data->url = 'disposisi_template_004';
            $data->nama = Auth::user()->pegawai->unit_kerja->nama_unit_kerja;
            $data->status = 'meneruskan';
            $data->kategori = 'arahan';
            $data->dari_id_user = Auth::user()->id;
            $data->kepada_id_user = $row;
            $data->kepada_id_unit_kerja = $pegawai->id_unit_kerja;
            $data->save();

            $reff[] = ['divisiId'=>$pegawai->id_unit_kerja,'nama'=>Auth::user()->pegawai->unit_kerja->nama_unit_kerja,
                'tipe'=>'struktur'];

            $catatan['kepada'] .= '- '.$pegawai->unit_kerja->nama_unit_kerja.'('.$pegawai->user->nama.') <br/>';
        }


        $catatan['catatan'] = '- '.$request->catatan.' <br/>';

        $data = new DisposisiLog;
        $data->disposisi_id = $id;
        $data->data = json_encode($catatan);
        $data->referensi = json_encode($reff);
        $data->dari_id_unit_kerja =  Auth::user()->pegawai->id_unit_kerja;
        $data->dari_nama_unit_kerja =  Auth::user()->pegawai->unit_kerja->nama_unit_kerja;
        $data->created_by_username =  Auth::user()->username;
        $data->created_by_nama =  Auth::user()->nama;
        $data->tipe =  'meneruskan';
        $data->dari_id_user = Auth::user()->id;
        $data->save();


        return redirect(url('to/suratmasuk-daftar-proses/'.$id));
    }


    public function pdfLembarDisposisi($id) {

        $data = [];
        $data['suratmasuk'] = Disposisi::where('id',$id)->first();
        $title = $id;
        $pdf = PDF::loadView('pdf.suratmasuk.lembar-disposisi', $data);
        $pdf->setPaper('Folio', 'portrait');
        return $pdf->stream('lembar-disposisi-'.$title.'-'.date('dmYHis').'.pdf');

    }
    public function pdfLembarDisposisiManual($id) {



        $data = [];
        $data['suratmasuk'] = Disposisi::where('id',$id)->first();
        $title = $id;
        $pdf = PDF::loadView('pdf.suratmasuk.lembar-disposisi-manual', $data);
        $pdf->setPaper('Folio', 'portrait');
        return $pdf->stream('lembar-disposisi-manual-'.$title.'-'.date('dmYHis').'.pdf');
    }

    public function pdfFileLampiranImages($id) {

        $disposisi =    DisposisiNotif::where('disposisi_id', $id)
            ->where(function ($query) {
                $query->where('kepada_id_user', Auth::user()->id);
                $query->orWhere('dari_id_user', Auth::user()->id);
            })->first();



        if($disposisi->disposisi->sumber==3) {
            return redirect(url($disposisi->url));
        }

        else  {
        //$files = DisposisiLampiran::where('disposisi_id',$id)->whereRaw('RIGHT(nama_file,3) in (jpg,pdf,jpe)')->get();
        $files = DisposisiLampiran::where('disposisi_id',$id)->whereNotIn(DB::raw('RIGHT(nama_file,3)'),['pdf','PDF'])->where('kategori','utama')
            ->get();


        $data = [];
        $data['files'] = $files;
//dd($data['files']);
        $title = $id;

        if(count($files)>0) {
            $title = $files[0];
            $title = $title->disposisi->perihal;
            $pdf = PDF::loadView('pdf.suratmasuk.lampiran-images', $data);
            return $pdf->stream('lampiran-scann-'.$title.'-'.date('dmYHis').'.pdf');
        }
        else {
            $files = DisposisiLampiran::where('disposisi_id',$id)->whereIn(DB::raw('RIGHT(nama_file,3)'),['pdf','PDF'])->where('kategori','utama')
            ->first();
            if(isset($files->nama_file)) {
                $nama_file = $files->nama_file;
            }
            else {
                $nama_file = '1';
            }
            return redirect(url('lampiran/suratmasuk/'.$nama_file));

        }


        }
    }


}

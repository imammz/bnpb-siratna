<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConfigController extends Controller
{

    public function cekSession() {
        if (Auth::check()) {
            return response()->json([
                'login' => TRUE
            ]);
        }
        else {
            return response()->json([
                'login' => FALSE
            ]);
        }
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\SuratKeluar;
use App\Models\SuratKeluarTemplate001;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\DisposisiLog;
use DB;
use DataTables;
use App\Models\Disposisi as Disposisi;
use Auth;
use Ramsey\Uuid\Uuid as Uuid;
use App\Models\UnitKerja;
use App\Models\DisposisiKlasifikasi;
use App\Models\DisposisiKeteranganLampiran;
use App\Models\DisposisiNotif;
use App\Models\Retensi;
use Illuminate\Support\Facades\Validator;
use App\Models\Pegawai;
use App\Models\DisposisiLampiran;
use QrCode;
use PDF;
use Log;
use Image;

class SuratKeluarController extends Controller
{
    //

    public function tambah() {

        $jenis = ['001'=>'Nota Dinas','003'=>'Undangan Internal','009'=>'Surat Perintah/Surat Tugas'];

        $data = [];
        $data['jenis'] = $jenis;

        $jra = Retensi::orderby('id')->get();
        $select_jra = [];
        foreach ($jra as $row) {
            $select_jra += [$row->id => $row['nama'] . " ({$row['retensi_keterangan']})"];
        }

        $data['jra'] = $select_jra;


        $unitkerja = Pegawai::where('id_user','=',Auth::user()->id)->where('id_unit_kerja','<>',null)
            ->orderBy('id_unit_kerja')->orderBy('sta_level')->get();


        $select_unit_kerja_kepada = [];
        foreach ($unitkerja as $row) {

            $getUnitKerja = '-';
            if(isset($row->unit_kerja->nama_unit_kerja)) {
                $getUnitKerja = $row->unit_kerja->nama_unit_kerja;
            }


            $level = ($row->sta_level==1)?'':'';
            $getUnitKerja = $level.' '.$getUnitKerja;
            $getUnitKerja = strtoupper($getUnitKerja);

            $row->id_unit_kerja = str_replace(0, '', $row->id_unit_kerja);
            $select_unit_kerja_kepada += [$row->unit_kerja->kode => '['.$row->id_unit_kerja.'] ['.$getUnitKerja.' ] | '.
                $row->user->nama];
        }
        $data['unitkerja'] = $select_unit_kerja_kepada;
        return view('suratkeluar.tambah',$data);
    }

    public function jenis($jenis = null,$ttd = null,$kode_klas = null) {

        session(['ttd'=>$ttd,'kode_klas'=>$kode_klas]);

        if($jenis=='001') {
            return \App::call('App\Http\Controllers\SuratKeluarController@jenis001');
        }
        elseif($jenis=='002') {
            return \App::call('App\Http\Controllers\SuratKeluarController@jenis002');
        }
        elseif($jenis=='003') {
            return \App::call('App\Http\Controllers\SuratKeluarController@jenis002');
        }
        else {
            return \App::call('App\Http\Controllers\SuratKeluarController@jenis001');
        }

    }


    public function jenis001() {

        $ttd = UnitKerja::where('kode',session('ttd'))->first();
        $kode_klas = session('kode_klas');

        $pegawai = Pegawai::where('id_unit_kerja',session('ttd'))->where('sta_level',1)->first();

        $unitkerja = Pegawai::where('id_user','<>',Auth::user()->id)->where('id_unit_kerja','<>',null)
            ->orderBy('id_unit_kerja')->orderBy('sta_level')->get();


        $select_unit_kerja_kepada = [];
        foreach ($unitkerja as $row) {

            $getUnitKerja = '-';
            if(isset($row->unit_kerja->nama_jabatan)) {
                $getUnitKerja = $row->unit_kerja->nama_jabatan;
            }




            if(isset($row->unit_kerja->flag_sekretaris)) {
                if($row->unit_kerja->flag_sekretaris) {
                    $getUnitKerja = str_replace('KEPALA','',$getUnitKerja);
                }
            }




            $row->id_unit_kerja = str_replace(0, '', $row->id_unit_kerja);
            $select_unit_kerja_kepada += [$row->unit_kerja->kode => '['.$row->id_unit_kerja.'] ['.$getUnitKerja.' ] | '.
                $row->user->nama];
        }



        $klasifikasi = DisposisiKlasifikasi::orderBy('id')->where('flag_aktif', 't')->get();
        $select_klasifikasi = [];
        foreach ($klasifikasi as $row) {
            $select_klasifikasi += [$row->id => $row['klasifikasi']];
        }

        $keteranggan_lampiran = DisposisiKeteranganLampiran::where('flag_aktif', 't')->orderBy('id')->get();
        $select_keteranggan_lampiran = [];
        foreach ($keteranggan_lampiran as $row) {
            $select_keteranggan_lampiran += [$row->id => $row['keterangan_lampiran']];
        }

        $jra = Retensi::orderby('id')->get();
        $select_jra = [];
        foreach ($jra as $row) {
            $select_jra += [$row->id => $row['nama'] . " ({$row['retensi_keterangan']})"];
        }


        $header = '';
            $nama_jenis = 'Nota Dinas';


            $pev = Auth::user()->pegawai->unit_kerja->prefix;
            $counter = Disposisi::where('oleh_unitkerja_id', '=', Auth::user()->pegawai->id_unit_kerja)->count();
            //dd($counter);
            $counter = $counter + 1;
            $counter = sprintf('%04d', $counter);
            $no_agenda = $counter.'/'. $ttd->prefix. '/'.$kode_klas. '/' . date('Y') . '';


            $body = '

(isi redaksi nota dinas)
...............................
...............................


';




        $select_unit_kerja_tembusan = [];
        foreach ($unitkerja as $row) {

            $getUnitKerja = '-';
            if(isset($row->unit_kerja->nama_jabatan)) {
                $getUnitKerja = $row->unit_kerja->nama_jabatan;
            }



            $row->id_unit_kerja = str_replace(0, '', $row->id_unit_kerja);
            $select_unit_kerja_tembusan += [$row->unit_kerja->kode => '['.$row->id_unit_kerja.'] ['.$getUnitKerja.' ] | '.
                $row->user->nama];
        }



        $select_unit_kerja_tembusan += [0=>'--------------------------------------------------------------------------------------------------------------------------------------------------------'];

        if(Auth::user()->pegawai->unit_kerja->eselon == 0) {

        }

        $select_unit_kerja_tembusan += [0=>'--------------------------------------------------------------------------------------------------------------------------------------------------------'];


        $id = Uuid::uuid4();
        $data = [];
        $data['gen'] = $id->toString();
        $data['nama_jenis'] = $nama_jenis;
        $data['unitkerja'] = $select_unit_kerja_kepada;
        $data['klasifikasi'] = $select_klasifikasi;
        $data['keteranggan_lampiran'] = $select_keteranggan_lampiran;
        $data['jra'] = $select_jra;
        $data['no_agenda'] = $no_agenda;
        $data['keamanan'] = ['1'=>'Biasa/Terbuka','2'=>'Terbatas','3'=>'Rahasia','4'=>'Sangat Rahasia'];

        $data['body'] = $body;
        $data['header'] = $header;
        $data['jenis'] = 'NOTA DINAS';
        $data['unitkerja_teruskan'] = $select_unit_kerja_tembusan;


        $data['ttd_nama'] = $pegawai->user->nama;
        $data['ttd_jabatan'] = $ttd->nama_unit_kerja;



        return view('suratkeluar.tambah-jenis',$data);

    }



    public function jenis002() {

        $ttd = UnitKerja::where('kode',session('ttd'))->first();
        $kode_klas = session('kode_klas');

        $pegawai = Pegawai::where('id_unit_kerja',session('ttd'))->where('sta_level',1)->first();

        $unitkerja = Pegawai::where('id_user','<>',Auth::user()->id)->where('id_unit_kerja','<>',null)
            ->orderBy('id_unit_kerja')->orderBy('sta_level')->get();


        $select_unit_kerja_kepada = [];
        foreach ($unitkerja as $row) {

            $getUnitKerja = '-';
            if(isset($row->unit_kerja->nama_jabatan)) {
                $getUnitKerja = $row->unit_kerja->nama_jabatan;
            }




            if(isset($row->unit_kerja->flag_sekretaris)) {
                if($row->unit_kerja->flag_sekretaris) {
                    $getUnitKerja = str_replace('KEPALA','',$getUnitKerja);
                }
            }




            $row->id_unit_kerja = str_replace(0, '', $row->id_unit_kerja);
            $select_unit_kerja_kepada += [$row->unit_kerja->kode => '['.$row->id_unit_kerja.'] ['.$getUnitKerja.' ] | '.
                $row->user->nama];
        }



        $klasifikasi = DisposisiKlasifikasi::orderBy('id')->where('flag_aktif', 't')->get();
        $select_klasifikasi = [];
        foreach ($klasifikasi as $row) {
            $select_klasifikasi += [$row->id => $row['klasifikasi']];
        }

        $keteranggan_lampiran = DisposisiKeteranganLampiran::where('flag_aktif', 't')->orderBy('id')->get();
        $select_keteranggan_lampiran = [];
        foreach ($keteranggan_lampiran as $row) {
            $select_keteranggan_lampiran += [$row->id => $row['keterangan_lampiran']];
        }

        $jra = Retensi::orderby('id')->get();
        $select_jra = [];
        foreach ($jra as $row) {
            $select_jra += [$row->id => $row['nama'] . " ({$row['retensi_keterangan']})"];
        }


        $header = '';
            $nama_jenis = 'Undangan';


            $pev = Auth::user()->pegawai->unit_kerja->prefix;
            $counter = Disposisi::where('oleh_unitkerja_id', '=', Auth::user()->pegawai->id_unit_kerja)->count();
            //dd($counter);
            $counter = $counter + 1;
            $counter = sprintf('%04d', $counter);
            $no_agenda = $counter.'/'. $ttd->prefix. '/'.$kode_klas. '/' . date('Y') . '';


            $body = '
            <table align="center" border="0" cellpadding="1" style="width: 100%;">
            <tbody>
                <tr>
                    <td height="90" valign="top">
    
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                    </td>
                </tr>
                <tr>
                        <td colspan="2" height="120">
                        <table border="0" cellpadding="3" style="width:700;">
                            <tbody>
    
                                <tr height="10">
                                        <td width="93"><span>Nomor</span></td>
                                    <td width="8"><span>:</span></td>
                                    <td style=" text-align: justify;"><span>Und.         '.$no_agenda.'   </span></td>
                                    <th style="font-style: normal;"> '.date('d-F-Y').' </th>
                                </tr>
                                <tr style="height= -20px;">
                                    <td><span>Sifat</span></td>
                                    <td><span>:</span></td>
                                    <td style=" text-align: justify;"><span style=" text-align: justify ;"> (Biasa/Penting/Rahasia/Segera) </span></td>
                                </tr>
                                <tr style="height= -20px;">
                                    <td><span>Lampiran</span></td>
                                    <td><span>:</span></td>
                                    <td style=" text-align: justify;"><span style=" text-align: justify ;">  (jumlah lembar) </span></td>
                                </tr>
                                <tr style="height= -20px;">
                                    <td><span>Hal</span></td>
                                    <td><span>:</span></td>
                                    <td style=" text-align: justify;"><span style=" text-align: justify ;"> Undangan</span></td>
                                </tr>
    
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr height="50">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Yth. (Tujuan Surat)</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Di (.......) </td>
                                    <td></td>
                                    <tr>
                            </tbody>
                        </table>
                    </td>
                    <td valign="top">
                        <div align="right">
                        </div>
                    </td>
                    </tr>
                    <tr>
                        <td width="302"></td>
                        <td width="343"></td>
                        <td width="339"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3" height="320" valign="top">
                            <table>
                                <tbody>
                                    <td height="10" valign="top">
                                </tbody>
                            </table>
                            <div align="justify">
                                <table border="0" cellpadding="2" style="width:700;">
                                    <tbody>
                                        <tr height=" 30 ">
                                            <td style=" text-align: justify;"><span> &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; (Isi pembukaan surat undangan) yang akan dilaksanakan pada:</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table>
                                    <tbody>
                                        <tr height="30">
    
                                        </tr>
                                    </tbody>
                                </table>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td width="50"></td>
                                            <td>Pada hari, tanggal</td>
                                            <td>:</td>
                                            <td> (Tanggal Acara) </td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td>Waktu</td>
                                            <td>:</td>
                                            <td> (waktu mulai acara) s.d. (waktu selesai acara)</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td>Tempat</td>
                                            <td>:</td>
                                            <td> (Nama Lokasi) </td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td>Acara</td>
                                            <td>:</td>
                                            <td> (nama acara) </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table border="0" cellpadding="2" style="width:700;">
                                    <tbody>
                                        <tr height=" 30 ">
                                            <td style=" text-align: justify;"><span> &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; Demikian disampaikan atas perhatian dan kehadiran Bapak/Ibu, kami ucapkan terima kasih.</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                                </td>
                    </tr>
                    <tr>
                        <td style="width: 840;"></td>
                        <td colspan="3" height="130 " valign="top">
                            <div align="center">
                                <span align="left"> (Nama Jabatan) </span></div>
                        </td>
    
                    </tr>
                    <tr>
                        <td style="width: 840;"></td>
                        <td width="70">
                            <div align="center">
                                <span> (Nama Pejabat) </span></div>
                        </td>
                    </tr>
            </tbody>
</table>

';




        $select_unit_kerja_tembusan = [];
        foreach ($unitkerja as $row) {

            $getUnitKerja = '-';
            if(isset($row->unit_kerja->nama_jabatan)) {
                $getUnitKerja = $row->unit_kerja->nama_jabatan;
            }



            $row->id_unit_kerja = str_replace(0, '', $row->id_unit_kerja);
            $select_unit_kerja_tembusan += [$row->unit_kerja->kode => '['.$row->id_unit_kerja.'] ['.$getUnitKerja.' ] | '.
                $row->user->nama];
        }



        $select_unit_kerja_tembusan += [0=>'--------------------------------------------------------------------------------------------------------------------------------------------------------'];

        if(Auth::user()->pegawai->unit_kerja->eselon == 0) {

        }

        $select_unit_kerja_tembusan += [0=>'--------------------------------------------------------------------------------------------------------------------------------------------------------'];


        $id = Uuid::uuid4();
        $data = [];
        $data['gen'] = $id->toString();
        $data['nama_jenis'] = $nama_jenis;
        $data['unitkerja'] = $select_unit_kerja_kepada;
        $data['klasifikasi'] = $select_klasifikasi;
        $data['keteranggan_lampiran'] = $select_keteranggan_lampiran;
        $data['jra'] = $select_jra;
        $data['no_agenda'] = $no_agenda;
        $data['keamanan'] = ['1'=>'Biasa/Terbuka','2'=>'Terbatas','3'=>'Rahasia','4'=>'Sangat Rahasia'];

        $data['body'] = $body;
        $data['header'] = $header;
        $data['jenis'] = 'UNDANGAN';
        $data['unitkerja_teruskan'] = $select_unit_kerja_tembusan;


        $data['ttd_nama'] = $pegawai->user->nama;
        $data['ttd_jabatan'] = $ttd->nama_unit_kerja;



        return view('suratkeluar.tambah-jenis-002',$data);

    }


    public function tambahProses(Request $request) {

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        $kdTemplates = '001';

        $pegawai = Auth::user()->pegawai;
        $pev = $pegawai->unit_kerja->pev_unit_kerja;
        $pev = Auth::user()->pegawai->unit_kerja->pev_unit_kerja;
        $no_agenda = 'SM/' . $pev . '/' . date('Y') . '/';
        $counter = Disposisi::where('nomor_agenda', 'like', "%{$no_agenda}%")->count();
        //dd($counter);
        $counter = $counter + 1;
        $counter = sprintf('%08d', $counter);
        $no_agenda .= $counter;

        $validator = Validator::make(request()->all(), [
            // 'keterangan' => 'required|max:200',
            //'kepada' => 'required',
            //'pengirim' => 'required',
            //'nosurat' => 'required',
            'perihal' => 'required',
            'tgl_surat' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('suratkeluar-tambah/')
                ->withErrors($validator->errors());
        } else {


            $oleh_unitkerja_id = Auth::user()->pegawai->id_unit_kerja;
            $oleh_unitkerja = UnitKerja::where('kode', $oleh_unitkerja_id)->first();
            $kepada = UnitKerja::where('kode', $request->kepada)->first();
            $retensi = Retensi::where('id', $request->retensi)->first();
            $klasifikasi = DisposisiKlasifikasi::where('id', $request->klasifikasi)->first();

            $suratKeluarID = Uuid::uuid4();

            $data = new Disposisi;
            $data->id = $request->gen;
            $data->oleh_unitkerja_id = $oleh_unitkerja_id;
            $data->oleh_unitkerja_nama = $oleh_unitkerja->nama_unit_kerja;
            $data->nama = 'Surat Keluar';
            $data->pengirim = $request->pengirim;
            $data->pengirim_nomor_surat = $request->nosurat;
            $data->status = 'active';
            $data->created_by_username = Auth::user()->username;
            $data->created_by_nama = Auth::user()->nama;
            $data->kepada_unitkerja_id = $request->kepada;
            $data->kepada_unitkerja_nama = $kepada->nama_unit_kerja;
            $data->status_progress = 'proses';

            if(isset($retensi->retensi_active)) {
                $data->retensi_active = $retensi->retensi_active;
            }
            if(isset($retensi->retensi_inactive)) {

            $data->retensi_inactive = $retensi->retensi_inactive;
            }
            if (isset($retensi->retensi_keterangan)) {
                $data->retensi_keterangan = $retensi->retensi_keterangan;
            }
            
            $data->oleh_id_user = Auth::user()->id;

            if(isset($klasifikasi->klasifikasi)) {
                $data->klasifikasi = $klasifikasi->klasifikasi;
            }
            
            $data->tanggal_surat_masuk = $request->tgl_surat;
            $data->tanggal_diterima = $request->tgl_diterima;
            $data->nomor_agenda = $no_agenda;
            $data->perihal = $request->perihal;
            $data->sumber = 3;
            $data->save();

            $diteruskan = $request->tembusan_kepada;

            $kepada = Pegawai::where('id_unit_kerja', $request->kepada)->where('sta_level', 1)->first();

            $data = new DisposisiNotif;
            $data->disposisi_id = $request->gen;
            $data->dari_id_unit_kerja = $oleh_unitkerja_id;
            $data->url = 'suratkeluar-templates-pdf/'.$kdTemplates.'/'.$suratKeluarID;
            $data->nama = $oleh_unitkerja->nama_unit_kerja;
            $data->status = 'meneruskan';
            $data->kategori = 'tembusan';
            $data->dari_id_user = Auth::user()->id;
            $data->kepada_id_user = $kepada->user->id;
            $data->kepada_id_unit_kerja = $request->kepada;
            $data->save();


            foreach ($diteruskan as $row) {

                $kepada = Pegawai::where('id_unit_kerja', $row)->where('sta_level', 1)->first();

                $data = new DisposisiNotif;
                $data->disposisi_id = $request->gen;
                $data->dari_id_unit_kerja = $oleh_unitkerja_id;
                $data->url = 'suratkeluar-templates-pdf/'.$kdTemplates.'/'.$suratKeluarID;
                $data->nama = $oleh_unitkerja->nama_unit_kerja;
                $data->status = 'meneruskan';
                $data->kategori = 'tembusan';
                $data->dari_id_user = Auth::user()->id;
                $data->kepada_id_user = $kepada->user->id;
                $data->kepada_id_unit_kerja = $row;
                $data->save();
            }

$data = new SuratKeluar;
$data->id = $suratKeluarID;
$data->created_by_username =  Auth::user()->username;
$data->created_at = date('Y-m-d H:i:s');
$data->created_by_nama =  Auth::user()->nama;
$data->nama = $request->perihal;
$data->nomor_surat_keluar = $request->nosurat;
$data->prefix_nomor_surat = $request->nosurat;

if(isset($retensi->retensi_active)) {
    $data->retensi_active = $retensi->retensi_active;

}

if (isset($retensi->retensi_inactive)) {
    $data->retensi_inactive = $retensi->retensi_inactive;
}

if (isset($retensi->retensi_keterangan)) {
    $data->retensi_keterangan = $retensi->retensi_keterangan;
}

$data->status = 'created';
$data->status_progress = 'proses';
$data->surat_keluar_template_code = $kdTemplates;
$data->surat_keluar_template_id = $kdTemplates;
$data->surat_keluar_template_nama = 'NOTA DINAS';
$data->save();


$keamanan = 'B';

switch ($request->klasifikasi) {
    case 1 :
        $keamanan = 'B';
        break;
    case 2 :
        $keamanan = 'T';
        break;
    case 3 :
        $keamanan = 'R';
        break;
    case 4 :
        $keamanan = 'SR';
        break;

}


$data2 = new SuratKeluarTemplate001;
$data2->id = Uuid::uuid4();
$data2->surat_keluar_id = $suratKeluarID;
$data2->surat_keluar_template_id = $data->surat_keluar_template_id;
$data2->nomor_surat = $request->nosurat;
$data2->keamanan = $keamanan;
$data2->kode_arsip = $request->nosurat;
$data2->yth = $request->yth;
$data2->dari = $request->dari;
$data2->prihal = $request->perihal;
$data2->tanggal_surat = $request->tgl_surat;
$data2->isi =$request->content_surat;
$data2->penandatangan = json_encode(['nama'=>$request->penandatangan_nama,'nip'=>$request->penandatangan_nip,'jabatan'=>$request->penandatangan_jabatan]);
$data2->tembusan = json_encode($request->tembusan_kepada);
$data2->nomor_tahunan = date('Y');
$data2->created_at = date('Y-m-d H:i:s');
$data2->save();


$request->session()->flash('pesan', 'Nota Dinas Berhasil Ditambah');

        }

        return redirect('to/suratkeluar-daftar/');

    }




    public function tambahProses002(Request $request) {

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        $kdTemplates = '002';

        $pegawai = Auth::user()->pegawai;
        $pev = $pegawai->unit_kerja->pev_unit_kerja;
        $pev = Auth::user()->pegawai->unit_kerja->pev_unit_kerja;
        $no_agenda = 'SM/' . $pev . '/' . date('Y') . '/';
        $counter = Disposisi::where('nomor_agenda', 'like', "%{$no_agenda}%")->count();
        //dd($counter);
        $counter = $counter + 1;
        $counter = sprintf('%08d', $counter);
        $no_agenda .= $counter;

        $validator = Validator::make(request()->all(), [
            // 'keterangan' => 'required|max:200',
            //'kepada' => 'required',
            //'pengirim' => 'required',
            //'nosurat' => 'required',
            'perihal' => 'required',
            'tgl_surat' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('suratkeluar-tambah/')
                ->withErrors($validator->errors());
        } else {


            $oleh_unitkerja_id = Auth::user()->pegawai->id_unit_kerja;
            $oleh_unitkerja = UnitKerja::where('kode', $oleh_unitkerja_id)->first();
          
            $kepada = [];
          
            $retensi = Retensi::where('id', $request->retensi)->first();
            $klasifikasi = DisposisiKlasifikasi::where('id', $request->klasifikasi)->first();

            $suratKeluarID = Uuid::uuid4();

            $data = new Disposisi;
            $data->id = $request->gen;
            $data->oleh_unitkerja_id = $oleh_unitkerja_id;
            $data->oleh_unitkerja_nama = $oleh_unitkerja->nama_unit_kerja;
            $data->nama = 'Surat Keluar';
            $data->pengirim = $request->pengirim;
            $data->pengirim_nomor_surat = $request->nosurat;
            $data->status = 'active';
            $data->created_by_username = Auth::user()->username;
            $data->created_by_nama = Auth::user()->nama;
            $data->kepada_unitkerja_id = $oleh_unitkerja_id;
            $data->kepada_unitkerja_nama = $oleh_unitkerja->nama_unit_kerja;
            $data->status_progress = 'proses';

            if(isset($retensi->retensi_active)) {
                $data->retensi_active = $retensi->retensi_active;
            }
            if(isset($retensi->retensi_inactive)) {

            $data->retensi_inactive = $retensi->retensi_inactive;
            }
            if (isset($retensi->retensi_keterangan)) {
                $data->retensi_keterangan = $retensi->retensi_keterangan;
            }
            
            $data->oleh_id_user = Auth::user()->id;

            if(isset($klasifikasi->klasifikasi)) {
                $data->klasifikasi = $klasifikasi->klasifikasi;
            }
            
            $data->tanggal_surat_masuk = $request->tgl_surat;
            $data->tanggal_diterima = $request->tgl_diterima;
            $data->nomor_agenda = $no_agenda;
            $data->perihal = $request->perihal;
            $data->sumber = 3;
            $data->save();

            $diteruskan = $request->tembusan_kepada;

        


            foreach ($diteruskan as $row) {

                $kepada = Pegawai::where('id_unit_kerja', $row)->where('sta_level', 1)->first();

                $data = new DisposisiNotif;
                $data->disposisi_id = $request->gen;
                $data->dari_id_unit_kerja = $oleh_unitkerja_id;
                $data->url = 'suratkeluar-templates-pdf/'.$kdTemplates.'/'.$suratKeluarID;
                $data->nama = $oleh_unitkerja->nama_unit_kerja;
                $data->status = 'meneruskan';
                $data->kategori = 'tembusan';
                $data->dari_id_user = Auth::user()->id;
                $data->kepada_id_user = $kepada->user->id;
                $data->kepada_id_unit_kerja = $row;
                $data->save();
            }

$data = new SuratKeluar;
$data->id = $suratKeluarID;
$data->created_by_username =  Auth::user()->username;
$data->created_at = date('Y-m-d H:i:s');
$data->created_by_nama =  Auth::user()->nama;
$data->nama = $request->perihal;
$data->nomor_surat_keluar = $request->nosurat;
$data->prefix_nomor_surat = $request->nosurat;

if(isset($retensi->retensi_active)) {
    $data->retensi_active = $retensi->retensi_active;

}

if (isset($retensi->retensi_inactive)) {
    $data->retensi_inactive = $retensi->retensi_inactive;
}

if (isset($retensi->retensi_keterangan)) {
    $data->retensi_keterangan = $retensi->retensi_keterangan;
}

$data->status = 'created';
$data->status_progress = 'proses';
$data->surat_keluar_template_code = $kdTemplates;
$data->surat_keluar_template_id = $kdTemplates;
$data->surat_keluar_template_nama = 'UNDANGAN';
$data->save();


$keamanan = 'B';

switch ($request->klasifikasi) {
    case 1 :
        $keamanan = 'B';
        break;
    case 2 :
        $keamanan = 'T';
        break;
    case 3 :
        $keamanan = 'R';
        break;
    case 4 :
        $keamanan = 'SR';
        break;

}


$data2 = new SuratKeluarTemplate001;
$data2->id = Uuid::uuid4();
$data2->surat_keluar_id = $suratKeluarID;
$data2->surat_keluar_template_id = $data->surat_keluar_template_id;
$data2->nomor_surat = $request->nosurat;
$data2->keamanan = $keamanan;
$data2->kode_arsip = $request->nosurat;
$data2->yth = $request->yth;
$data2->dari = $request->dari;
$data2->prihal = $request->perihal;
$data2->tanggal_surat = $request->tgl_surat;
$data2->isi =$request->content_surat;
$data2->penandatangan = json_encode(['nama'=>$request->penandatangan_nama,'nip'=>$request->penandatangan_nip,'jabatan'=>$request->penandatangan_jabatan]);
$data2->tembusan = json_encode($request->tembusan_kepada);
$data2->nomor_tahunan = date('Y');
$data2->created_at = date('Y-m-d H:i:s');
$data2->save();


$request->session()->flash('pesan', 'Undangan Berhasil Ditambah');

        }

        return redirect('to/suratkeluar-daftar/');

    }

    public function daftar() {
        $data = [];
        return view('suratkeluar.daftar', $data);
    }



    public function daftarData(Request $request,$flag = null)
    {

        $start = $request->get('start');
        $disposisi = SuratKeluar::wherehas('byUsername', function ($q) {

            $pegawai_uk = Pegawai::where('id_unit_kerja',Auth::user()->pegawai->id_unit_kerja)->get();
            $user_uk = [];
            foreach($pegawai_uk as $row) {
                $user_uk[] = $row->id_user;
            }

            $q->whereIn('id', $user_uk);
        });

        $notif = [];


        return Datatables::of(
            $disposisi  ->orderBy('created_at', 'desc')
        )->addIndexColumn()
            ->addColumn('no', function ($model) use ($start) {
                $no = $start + 1;
                return '<div style="text-align:right">' . $no . '</div>';
            }) ->editColumn('nomor_surat_keluar', function ($d) {

                return '<a  href="#"  data-toggle="modal"
                                    data-target="#modal-1" data-whatever="'.$d->id.'" data-jenis="'.$d->surat_keluar_template_code.'" class="text-small"><b>' . strtoupper($d->nomor_surat_keluar) . '</b></a>';
            }, 1)
            ->rawColumns(['nomor_surat_keluar'])

            ->make(true);

    }


    public function templatesDetil($code = null ,$id = null) {

    }

    public function templatesPdf($code = null ,$id = null) {

        return \App::call('App\Http\Controllers\SuratKeluarController@pdf'.$code,["id"=>$id]);

    }


    public function pdf001($id) {

        $title = 'NOTA DINAS';

        $data = [];
        $data['id'] = $id;
        $data['surat'] = SuratKeluar::where('id',$id)->first();
        $surat = $data['surat'];
        $data['ttd'] = (isset($surat->notaDinas->penandatangan))?json_decode($surat->notaDinas->penandatangan):[];
        $tembusan_kode = (isset($surat->notaDinas->tembusan))?json_decode($surat->notaDinas->tembusan):[];
        $tembusan = [];

        foreach($tembusan_kode as $row) {
            $uk = UnitKerja::where('kode',$row)->orderBy('id')->first();
            if(isset($uk->nama_unit_kerja)) {
                $tembusan[] = $uk->nama_unit_kerja;
            }

        }

        $data['tembusan'] = $tembusan;

        $pdf = PDF::loadView('suratkeluar.pdf.001', $data);
        return $pdf->stream('surat-001-'.$title.'-'.date('dmYHis').'.pdf');
    }


    public function pdf002($id) {

        $title = 'UNDANGAN';

        $data = [];
        $data['id'] = $id;
        $data['surat'] = SuratKeluar::where('id',$id)->first();
        $surat = $data['surat'];
        $data['ttd'] = json_decode($surat->notaDinas->penandatangan);
        $tembusan_kode = json_decode($surat->notaDinas->tembusan);
        $tembusan = [];

        foreach($tembusan_kode as $row) {
            $uk = UnitKerja::where('kode',$row)->orderBy('id')->first();
            if(isset($uk->nama_unit_kerja)) {
                $tembusan[] = $uk->nama_unit_kerja;
            }

        }

        $data['tembusan'] = $tembusan;

        $pdf = PDF::loadView('suratkeluar.pdf.002', $data);
        return $pdf->stream('surat-002-'.$title.'-'.date('dmYHis').'.pdf');
    }

}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\Menu;
use App\Models\MenuRole;
use App\Models\Role;

class UserController extends Controller
{
    public $successStatus = 200;

    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('nApp')->accessToken;
            return response()->json(['status'=>true,'success' => $success], $this->successStatus);
        }
        else{
            return response()->json(['status'=>false,'error'=>'Unauthorised'], 401);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['id_role'] = 2;
        $user = User::create($input);
        $success['token'] =  $user->createToken('nApp')->accessToken;
        $success['name'] =  $user->name;

        return response()->json(['status'=>true,'success'=>$success], $this->successStatus);
    }

    public function details()
    {
        $user = Auth::user();
        $menurole = MenuRole::where('id_role',$user->id_role)->get();
        $menu = [];
        foreach($menurole as $row) {
            $menu[] = $row->menu;
        }
        return response()->json(['status'=>true,'success' => $user,'menu'=>$menu], $this->successStatus);
    }

    public function detailsEmail()
    {
        $user = Auth::user();
        return response()->json(['status'=>true,'email' => $user->email], $this->successStatus);
    }

}

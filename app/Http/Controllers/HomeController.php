<?php

namespace App\Http\Controllers;

//require_once('../vendor/binarystash/pdf-watermarker/pdfwatermarker/pdfwatermarker.php');
//require_once('../vendor/binarystash/pdf-watermarker/pdfwatermarker/pdfwatermark.php');

use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use App\Models\Disposisi as Disposisi;
use Ramsey\Uuid\Uuid as Uuid;
use App\Models\UnitKerja;
use App\models\DisposisiKlasifikasi;
use App\models\DisposisiKeteranganLampiran;
use App\Models\DisposisiNotif;
use App\Models\Agenda;
use App\Models\AgendaUser;
use App\Models\SuratKeluarNotif;
use Ilovepdf\Ilovepdf;
//use PDFWatermark;
//use PDFWatermarker;
//use setasign\Fpdi\Fpdi;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];

        return view('main', $data);
    }


    public function to($url = null,$p1= null,$p2= null,$p3= null)
    {
        if($url=='home-page') {
            return redirect('/');
        }
        elseif($url=='home') {
            return redirect('/');
        }
        else {


        $data = [];
        $data['url'] = $url;
        if($p1!=null) {
            $data['url'] = $url.'/'.$p1;
        }
        if($p2!=null) {
            $data['url'] = $url.'/'.$p1.'/'.$p2;
        }
        if($p3!=null) {
            $data['url'] = $url.'/'.$p1.'/'.$p2.'/'.$p3;
        }
        return view('main', $data);
        }
    }

    public function home() {



        $data = [];

        $data['jml_belum_dibaca'] = Disposisi::where('status_progress', 'proses')
            ->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
                $q->whereIn('status',['terkirim','meneruskan']);
            })
            ->where('status', 'active')->count();

        $data['belum_dibaca'] = Disposisi::where('status_progress', 'proses')
            ->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
                $q->whereIn('status',['terkirim','meneruskan']);
            })
            ->where('status', 'active')->get();


        session(['notif_surat_masuk'=>$data['belum_dibaca'],'jml_surat_masuk'=>$data['jml_belum_dibaca']]);


        $data['suratmasukMarked'] = Disposisi::where('status_progress', 'proses')
            ->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
                $q->orWhere('dari_id_user', Auth::user()->id);
            })
            ->wherehas('notif', function ($q) {
                $q->where('bookmarked', true);
            })
            ->where('status', 'active')->count();

        $data['suratmasuk_terbaru'] = Disposisi::where('status_progress', 'proses')
            ->where('status', 'active')
            ->wherehas('notif', function ($q) {
                $q->where('kepada_id_user', Auth::user()->id);
            })
            ->orderBy('created_at', 'desc')->limit(5)->get();



        $data['suratkeluarMarked'] = SuratKeluarNotif::where('kepada_id_user', Auth::user()->id)->count();
        $data['agendaUser'] = AgendaUser::where('id_user', Auth::user()->id)->get();
        $data['TopAgendaUser'] = DB::select("select * from _agenda A inner join _agenda_user B  on A.id = B.agenda_id
                                            where A.tanggal_mulai > '".date('Y-m-d H:i:s')."' and B.id_user ='".Auth::user()->id."'
                                           order by A.tanggal_mulai asc  limit 5  ");

        return view('home', $data);

    }


    public function testpdf()
    {



        try {



            $ilovepdf = new Ilovepdf('project_public_a522578cc6ec9858331ec6210f7ba1d6__ha5z7cb78eb26ac10d14bdbb713c0212c8a2', 'secret_key_c48d8c917f7de77bae66aaaa024b7e9b_wAghb4eed38619cb81975a7e6d938a543e5f4');

            $myTaskWatermark = $ilovepdf->newTask('watermark');

            $file1 = $myTaskWatermark->addFile(getcwd() . '\file1.pdf');
//            $myTaskWatermark->setText('siska.bnpb.go.id');
            $myTaskWatermark->setImage(getcwd() . '\logo.png');
            $myTaskWatermark->setLayer('below');
            $myTaskWatermark->setVerticalPosition('middle');
            $myTaskWatermark->execute();
            $myTaskWatermark->download();

        } catch (\Ilovepdf\Exceptions\StartException $e) {
            echo "An error occured on start: " . $e->getMessage() . " ";
            // Authentication errors
        } catch (\Ilovepdf\Exceptions\AuthException $e) {
            echo "An error occured on auth: " . $e->getMessage() . " ";
           // echo implode(', ', $e->getErrors());
            // Uploading files errors
        } catch (\Ilovepdf\Exceptions\UploadException $e) {
            echo "An error occured on upload: " . $e->getMessage() . " ";
           // echo implode(', ', $e->getErrors());
            // Processing files errors
        } catch (\Ilovepdf\Exceptions\ProcessException $e) {
            echo "An error occured on process: " . $e->getMessage() . " ";
           // echo implode(', ', $e->getErrors());
            // Downloading files errors
        } catch (\Ilovepdf\Exceptions\DownloadException $e) {
            echo "An error occured on process: " . $e->getMessage() . " ";
           // echo implode(', ', $e->getErrors());
            // Other errors (as connexion errors and other)
        } catch (\Exception $e) {
            echo "An error occured: " . $e->getMessage();
        }
    }


    public function testpdf2()
    {

        $watermark = new PDFWatermark(getcwd() . '\logo.png');
        $watermark->setPosition('center');
        //$watermark->setAsBackground();
        $watermarker = new PDFWatermarker(getcwd() . '\file1.pdf', getcwd() . '\result.pdf', $watermark);
        $watermarker->savePdf();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KpaController extends Controller
{

    public function suratmasuk() {


       return view('kpa.suratmasuk');
    }

    public function suratmasukTambah() {

        return view('kpa.suratmasuk-tambah');
     }

    public function disposisi() {

        return view('kpa.disposisi');
    }

    public function disposisiTambah() {

        return view('kpa.disposisi-tambah');
    }

    public function disposisiDetil() {

        return view('kpa.disposisi-detil');
    }


}

<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StrukturOrganisasi
 * 
 * @property int $id
 * @property int $level
 * @property int $urut_level
 * @property int $id_struktur_atasan_langsung
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pegawais
 *
 * @package App\Models
 */
class StrukturOrganisasi extends Eloquent
{
	protected $table = 'struktur_organisasi';

	protected $casts = [
		'level' => 'int',
		'urut_level' => 'int',
		'id_struktur_atasan_langsung' => 'int'
	];

	protected $fillable = [
		'level',
		'urut_level',
		'id_struktur_atasan_langsung'
	];

	public function pegawais()
	{
		return $this->hasMany(\App\Models\Pegawai::class, 'id_struktur_organisasi');
	}
}

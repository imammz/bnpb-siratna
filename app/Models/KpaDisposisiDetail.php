<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpaDisposisiDetail
 * 
 * @property string $id
 * @property string $nomor_surat
 * @property \Carbon\Carbon $tanggal_surat
 * @property string $perihal
 * @property int $nominal
 * @property \Carbon\Carbon $tanggal_diterima
 * @property string $kpa_disposisi_id
 * @property string $status
 * @property string $created_by_divisi_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class KpaDisposisiDetail extends Eloquent
{
	protected $table = '_kpa_disposisi_detail';
	public $incrementing = false;

	protected $casts = [
		'nominal' => 'int'
	];

	protected $dates = [
		'tanggal_surat',
		'tanggal_diterima'
	];

	protected $fillable = [
		'nomor_surat',
		'tanggal_surat',
		'perihal',
		'nominal',
		'tanggal_diterima',
		'kpa_disposisi_id',
		'status',
		'created_by_divisi_id'
	];
}

<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CounterSuratKeluar
 * 
 * @property string $nama_table
 * @property \Carbon\Carbon $tanggal
 * @property string $prefix
 * @property int $jumlah
 * @property int $counter_hari
 * @property int $counter
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class CounterSuratKeluar extends Eloquent
{
	protected $table = '_counter_surat_keluar';
	public $incrementing = false;

	protected $casts = [
		'jumlah' => 'int',
		'counter_hari' => 'int',
		'counter' => 'int'
	];

	protected $dates = [
		'tanggal'
	];

	protected $fillable = [
		'nama_table',
		'tanggal',
		'prefix',
		'jumlah',
		'counter_hari',
		'counter'
	];
}

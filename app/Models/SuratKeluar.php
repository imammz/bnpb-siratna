<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluar
 *
 * @property string $id
 * @property string $surat_keluar_template_nama
 * @property string $surat_keluar_template_id
 * @property string $surat_keluar_template_code
 * @property string $nomor_surat_keluar
 * @property string $nama
 * @property string $status
 * @property string $created_by_username
 * @property string $created_by_nama
 * @property \Carbon\Carbon $tanggal_pelaksanaan
 * @property string $status_progress
 * @property string $prefix_nomor_surat
 * @property int $retensi_active
 * @property int $retensi_inactive
 * @property string $retensi_keterangan
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluar extends Eloquent
{
	protected $table = '_surat_keluar';
	public $incrementing = false;

	protected $casts = [
		'retensi_active' => 'int',
		'retensi_inactive' => 'int'
	];

	protected $dates = [
		'tanggal_pelaksanaan'
	];

	protected $fillable = [
		'surat_keluar_template_nama',
		'surat_keluar_template_id',
		'surat_keluar_template_code',
		'nomor_surat_keluar',
		'nama',
		'status',
		'created_by_username',
		'created_by_nama',
		'tanggal_pelaksanaan',
		'status_progress',
		'prefix_nomor_surat',
		'retensi_active',
		'retensi_inactive',
		'retensi_keterangan'
    ];


    public function byUsername() {
        return $this->belongsTo('App\user','created_by_username','username');
    }

    public function notaDinas() {
            return $this->hasOne('App\Models\SuratKeluarTemplate001','surat_keluar_id','id');
    }
}

<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarLampiran
 *
 * @property int $id
 * @property string $surat_keluar_id
 * @property string $nama_file
 * @property string $keterangan_file
 * @property string $created_by_username
 * @property string $created_by_divisi_id
 * @property string $catatan
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarLampiran extends Eloquent
{
	protected $table = '_surat_keluar_lampiran';

	protected $fillable = [
		'surat_keluar_id',
		'nama_file',
		'keterangan_file',
		'created_by_username',
		'created_by_divisi_id',
		'catatan'
    ];

    public function byUsername() {
        return $this->belongsTo('App\user','created_by_username','username');
    }
}

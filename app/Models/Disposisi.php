<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Disposisi
 *
 * @property string $id
 * @property string $disposisi_template_nama
 * @property string $disposisi_template_id
 * @property string $nama
 * @property string $pengirim
 * @property string $pengirim_nomor_surat
 * @property string $status
 * @property string $created_by_username
 * @property string $created_by_nama
 * @property string $kepada_divisi_id
 * @property string $kepada_divisi_nama
 * @property string $status_progress
 * @property \Carbon\Carbon $tanggal_pelaksanaan
 * @property int $retensi_active
 * @property int $retensi_inactive
 * @property string $retensi_keterangan
 * @property string $keterangan_lampiran
 * @property string $id_user
 * @property int $id_struktur_organisasi
 * @property int $id_unit_kerja
 * @property string $kepada_id_user
 * @property int $kepada_id_struktur_organisasi
 * @property int $kepada_id_unit_kerja
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Disposisi extends Eloquent
{
    protected $table = 'disposisi';
    public $incrementing = false;

    protected $casts = [
        'retensi_active' => 'int',
        'retensi_inactive' => 'int',
        'id_struktur_organisasi' => 'int',
        'id_unit_kerja' => 'int',
        'kepada_id_struktur_organisasi' => 'int',
        'kepada_id_unit_kerja' => 'int'
    ];

    protected $dates = [
        'tanggal_pelaksanaan'
    ];

    protected $fillable = [
        'disposisi_template_nama',
        'disposisi_template_id',
        'nama',
        'pengirim',
        'pengirim_nomor_surat',
        'status',
        'created_by_username',
        'created_by_nama',
        'kepada_unitkerja_id',
        'kepada_unitkerja_nama',
        'oleh_unitkerja_id',
        'oleh_unitkerja_nama',
        'status_progress',
        'tanggal_pelaksanaan',
        'retensi_active',
        'retensi_inactive',
        'retensi_keterangan',
        'keterangan_lampiran',
        'oleh_id_user',
        'klasifikasi',
        'tanggal_surat_masuk',
        'tanggal_diterima',
        'nomor_agenda',
        'perihal',
    ];

    public function byUsername() {
        return $this->belongsTo('App\user','created_by_username','username');
    }

    public function temp4()
    {
        return $this->hasOne('App\Models\DisposisiTemplate004', 'id', 'disposisi_template_id');
    }

    public function temp3()
    {
        return $this->hasOne('App\Models\DisposisiTemplate003', 'id', 'disposisi_template_id');
    }

    public function temp2()
    {
        return $this->hasOne('App\Models\DisposisiTemplate002', 'id', 'disposisi_template_id');
    }

    public function temp1()
    {
        return $this->hasOne('App\Models\DisposisiTemplate001', 'id', 'disposisi_template_id');
    }


    public function lampiran()
    {
        return $this->hasMany('App\Models\DisposisiLampiran', 'disposisi_id', 'id');
    }

    public function log()
    {
        return $this->hasMany('App\Models\DisposisiLog', 'disposisi_id', 'id');
    }


    public function logDisposisi()
    {
        return $this->hasMany('App\Models\DisposisiLog', 'disposisi_id', 'id')->whereIn('tipe',['disposisi','meneruskan'])->orderBy('created_at','asc');
    }

    public function logDiteruskan()
    {
        return $this->hasMany('App\Models\DisposisiLog', 'disposisi_id', 'id')->where('tipe','meneruskan')->orderBy('created_at','asc');
    }

    public function logCatatan()
    {
        return $this->hasMany('App\Models\DisposisiLog', 'disposisi_id', 'id')->where('tipe','catatan')->orderBy('created_at','asc');
    }

    public function notif()
    {
        return $this->hasMany('App\Models\DisposisiNotif', 'disposisi_id', 'id');
    }





}

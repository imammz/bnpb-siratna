<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarTemplate004
 * 
 * @property string $id
 * @property string $surat_keluar_id
 * @property string $surat_keluar_template_id
 * @property string $nomor_surat
 * @property string $keamanan
 * @property string $kode_arsip
 * @property string $sifat
 * @property string $prihal
 * @property string $kota_persuratan
 * @property int $lampiran
 * @property string $satuan_lampiran
 * @property \Carbon\Carbon $tanggal_surat
 * @property string $yth
 * @property string $di
 * @property string $isi
 * @property string $penandatangan
 * @property string $tembusan
 * @property int $nomor_tahunan
 * @property \Carbon\Carbon $tanggal_approve
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarTemplate004 extends Eloquent
{
	protected $table = '_surat_keluar_template_004';
	public $incrementing = false;

	protected $casts = [
		'lampiran' => 'int',
		'nomor_tahunan' => 'int'
	];

	protected $dates = [
		'tanggal_surat',
		'tanggal_approve'
	];

	protected $fillable = [
		'surat_keluar_id',
		'surat_keluar_template_id',
		'nomor_surat',
		'keamanan',
		'kode_arsip',
		'sifat',
		'prihal',
		'kota_persuratan',
		'lampiran',
		'satuan_lampiran',
		'tanggal_surat',
		'yth',
		'di',
		'isi',
		'penandatangan',
		'tembusan',
		'nomor_tahunan',
		'tanggal_approve'
	];
}

<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarTemplate023
 * 
 * @property string $id
 * @property string $surat_keluar_id
 * @property string $surat_keluar_template_id
 * @property string $nomor_surat
 * @property string $keamanan
 * @property string $kode_arsip
 * @property string $sifat
 * @property string $prihal
 * @property \Carbon\Carbon $tanggal_pembuatan
 * @property string $yth
 * @property string $isi
 * @property \Carbon\Carbon $tanggal_terima
 * @property string $penerima
 * @property string $pengirim
 * @property \Carbon\Carbon $tanggal_approve
 * @property string $no_tlp
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarTemplate023 extends Eloquent
{
	protected $table = '_surat_keluar_template_023';
	public $incrementing = false;

	protected $dates = [
		'tanggal_pembuatan',
		'tanggal_terima',
		'tanggal_approve'
	];

	protected $fillable = [
		'id',
		'surat_keluar_id',
		'surat_keluar_template_id',
		'nomor_surat',
		'keamanan',
		'kode_arsip',
		'sifat',
		'prihal',
		'tanggal_pembuatan',
		'yth',
		'isi',
		'tanggal_terima',
		'penerima',
		'pengirim',
		'tanggal_approve',
		'no_tlp'
	];
}

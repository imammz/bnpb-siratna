<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Arsip
 * 
 * @property string $id
 * @property string $kategori
 * @property string $nama_arsip
 * @property string $keterangan
 * @property string $id_user
 * @property int $id_struktur_organisasi
 * @property int $id_unit_kerja
 * @property string $created_by_username
 * @property string $created_by_nama
 * @property string $last_user_update
 * @property string $klasifikasi_keamanan
 * @property int $jenjang_akses
 * @property int $retensi_active
 * @property int $retensi_inactive
 * @property string $retensi_keterangan
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Arsip extends Eloquent
{
	protected $table = '_arsip';
	public $incrementing = false;

	protected $casts = [
		'id_struktur_organisasi' => 'int',
		'id_unit_kerja' => 'int',
		'jenjang_akses' => 'int',
		'retensi_active' => 'int',
		'retensi_inactive' => 'int'
	];

	protected $fillable = [
		'kategori',
		'nama_arsip',
		'keterangan',
		'id_user',
		'id_struktur_organisasi',
		'id_unit_kerja',
		'created_by_username',
		'created_by_nama',
		'last_user_update',
		'klasifikasi_keamanan',
		'jenjang_akses',
		'retensi_active',
		'retensi_inactive',
		'retensi_keterangan'
	];
}

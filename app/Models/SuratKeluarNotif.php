<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarNotif
 *
 * @property int $id
 * @property string $surat_keluar_id
 * @property string $url
 * @property string $nama
 * @property string $status
 * @property string $kategori
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarNotif extends Eloquent
{
	protected $table = '_surat_keluar_notif';

	protected $fillable = [
		'surat_keluar_id',
		'url',
		'nama',
		'status',
        'kategori',
        'dari_id_user',
        'updated_at',
        'kepada_id_user',
        'kepada_id_unit_kerja',
        'bookmarked'
	];
}

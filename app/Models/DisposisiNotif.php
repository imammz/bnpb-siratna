<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DisposisiNotif
 *
 * @property int $id
 * @property string $disposisi_id
 * @property string $id_user
 * @property int $id_struktur_organisasi
 * @property int $id_unit_kerja
 * @property string $url
 * @property string $nama
 * @property string $status
 * @property string $kategori
 * @property \Carbon\Carbon $tanggal_approve
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class DisposisiNotif extends Eloquent
{
	protected $table = 'disposisi_notif';

	protected $casts = [
		'id_struktur_organisasi' => 'int'
	];

	protected $dates = [
		'tanggal_approve'
	];

	protected $fillable = [
		'disposisi_id',
		'dari_id_user',
		'dari_id_unit_kerja',
		'kepada_id_user',
		'kepada_id_unit_kerja',
		'url',
		'nama',
		'status',
		'kategori',
        'tanggal_approve',
        'bookmarked'
    ];

    public function disposisi() {
        return $this->belongsTo('App\Models\Disposisi','disposisi_id','id');
    }


    public function kepadaUnitKerja() {
        return $this->belongsTo('App\Models\UnitKerja','kepada_id_unit_kerja','kode');
    }

    public function dariUnitKerja() {
        return $this->belongsTo('App\Models\UnitKerja','dari_id_unit_kerja','kode');
    }
}

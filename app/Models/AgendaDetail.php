<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AgendaDetail
 *
 * @property int $id
 * @property string $agenda_id
 * @property string $nama_file
 * @property string $created_by_username
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class AgendaDetail extends Eloquent
{
	protected $table = '_agenda_detail';

	protected $fillable = [
		'agenda_id',
		'nama_file',
		'created_by_username'
    ];


    public function agenda() {
        return $this->belongsTo('App\Models\Agenda','agenda_id','id');
    }
}

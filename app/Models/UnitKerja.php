<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UnitKerja
 *
 * @property int $id
 * @property string $kode_unit_kerja
 * @property string $nama_unit_kerja
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $pegawais
 *
 * @package App\Models
 */
class UnitKerja extends Eloquent
{
	protected $table = 'unit_kerja';

	protected $fillable = [
		'kode',
        'pev_unit_kerja',
        'nama_unit_kerja',
        'created_at',
        'updated_at',
        'nama_jabatan',
        'eselon',
        'parent_kode',
        'status_aktif',
        'kode',
        'flag_tujuan_surat',
        'flag_sekretaris'

	];

	public function pegawai()
	{
		return $this->hasMany(\App\Models\Pegawai::class, 'id_unit_kerja','kode');
    }



    public function child()
    {
        return $this->hasMany(\App\Models\UnitKerja::class, 'parent_kode','kode')
        ->where('status_aktif','=',1)->orderBy('kode');
    }

    public function parent()
    {
        return $this->belongsTo(\App\Models\UnitKerja::class, 'kode','parent_kode')
        ->where('status_aktif','=',1)->orderBy('kode');
    }

    public function kepala()
    {
        return $this->pegawai()->where('sta_level',1)->wherehas('user',function($q){
            $q->where('sta_aktif','=',1);
        });
    }

    public function staff()
    {
        return $this->pegawai()->where('sta_level',2)->wherehas('user',function($q){
            $q->where('sta_aktif','=',1);
        });
    }


    public function notifKepada()
	{
		return $this->hasMany('App\Models\DisposisiNotif', 'kepada_id_unit_kerja','kode');
    }

    public function notifDari()
	{
		return $this->hasMany('App\Models\DisposisiNotif', 'dari_id_unit_kerja','kode');
    }



}

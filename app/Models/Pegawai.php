<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Pegawai
 *
 * @property string $id
 * @property string $nip
 * @property \Carbon\Carbon $tanggal_lahir
 * @property string $tempat_lahir
 * @property string $id_user
 * @property int $id_struktur_organisasi
 * @property int $id_unit_kerja
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\StrukturOrganisasi $struktur_organisasi
 * @property \App\Models\UnitKerja $unit_kerja
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Pegawai extends Eloquent
{
	protected $table = 'pegawai';
	public $incrementing = false;

	protected $casts = [
		'id_struktur_organisasi' => 'int',
	];

	protected $dates = [
		'tanggal_lahir'
	];

	protected $fillable = [
	    'id',
		'nip',
		'tanggal_lahir',
		'tempat_lahir',
		'id_user',
		'id_struktur_organisasi',
		'id_unit_kerja'
	];

	public function struktur_organisasi()
	{
		return $this->belongsTo(\App\Models\StrukturOrganisasi::class, 'id_struktur_organisasi');
	}

	public function unit_kerja()
	{
		return $this->belongsTo(\App\Models\UnitKerja::class, 'id_unit_kerja','kode');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'id_user');
	}


}

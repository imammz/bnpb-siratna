<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Counter
 * 
 * @property string $nama_table
 * @property string $prefix
 * @property int $jumlah
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Counter extends Eloquent
{
	protected $table = '_counter';
	public $incrementing = false;

	protected $casts = [
		'jumlah' => 'int'
	];

	protected $fillable = [
		'nama_table',
		'prefix',
		'jumlah'
	];
}

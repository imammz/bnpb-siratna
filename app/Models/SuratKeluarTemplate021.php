<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarTemplate021
 * 
 * @property string $id
 * @property string $surat_keluar_id
 * @property string $surat_keluar_template_id
 * @property string $nomor_surat
 * @property string $nomor_surat_kedua
 * @property string $tipe_surat
 * @property string $keamanan
 * @property string $kode_arsip
 * @property string $sifat
 * @property string $prihal
 * @property string $tentang
 * @property string $instansi_pihak_pertama
 * @property string $instansi_pihak_kedua
 * @property string $logo_pihak_kedua
 * @property string $pihak_pertama
 * @property string $pihak_kedua
 * @property string $pembuka
 * @property string $isi
 * @property string $pasal
 * @property \Carbon\Carbon $tanggal_approve
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarTemplate021 extends Eloquent
{
	protected $table = '_surat_keluar_template_021';
	public $incrementing = false;

	protected $dates = [
		'tanggal_approve'
	];

	protected $fillable = [
		'id',
		'surat_keluar_id',
		'surat_keluar_template_id',
		'nomor_surat',
		'nomor_surat_kedua',
		'tipe_surat',
		'keamanan',
		'kode_arsip',
		'sifat',
		'prihal',
		'tentang',
		'instansi_pihak_pertama',
		'instansi_pihak_kedua',
		'logo_pihak_kedua',
		'pihak_pertama',
		'pihak_kedua',
		'pembuka',
		'isi',
		'pasal',
		'tanggal_approve'
	];
}

<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Retensi
 *
 * @property int $id
 * @property string $tipe
 * @property string $nama
 * @property int $retensi_active
 * @property int $retensi_inactive
 * @property string $retensi_keterangan
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Retensi extends Eloquent
{
	protected $table = '_retensi';

	protected $casts = [
		'retensi_active' => 'int',
		'retensi_inactive' => 'int'
	];

	protected $fillable = [
		'tipe',
		'nama',
		'retensi_active',
		'retensi_inactive',
		'retensi_keterangan'
	];
}

<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarTemplateAll
 * 
 * @property string $id
 * @property string $surat_keluar_id
 * @property string $surat_keluar_template_id
 * @property string $nomor_surat_keluar
 * @property string $nama_kegiatan
 * @property string $konten
 * @property \Carbon\Carbon $tanggal_pelaksanaan
 * @property \Carbon\Carbon $tanggal_approve
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarTemplateAll extends Eloquent
{
	protected $table = '_surat_keluar_template_all';
	public $incrementing = false;

	protected $dates = [
		'tanggal_pelaksanaan',
		'tanggal_approve'
	];

	protected $fillable = [
		'id',
		'surat_keluar_id',
		'surat_keluar_template_id',
		'nomor_surat_keluar',
		'nama_kegiatan',
		'konten',
		'tanggal_pelaksanaan',
		'tanggal_approve'
	];
}

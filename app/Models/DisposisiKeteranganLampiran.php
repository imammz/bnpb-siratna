<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class DisposisiKeteranganLampiran extends Model
{
    protected $table = 'disposisi_keterangan_lampiran';
    public $incrementing = true;
}

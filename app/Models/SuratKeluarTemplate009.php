<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarTemplate009
 * 
 * @property string $id
 * @property string $surat_keluar_id
 * @property string $surat_keluar_template_id
 * @property string $nomor_surat
 * @property string $keamanan
 * @property string $kode_arsip
 * @property string $kota_persuratan
 * @property string $menimbang
 * @property string $dasar
 * @property string $kepada
 * @property string $daftar_kepada
 * @property string $untuk
 * @property \Carbon\Carbon $tanggal_surat
 * @property string $penandatangan
 * @property int $nomor_tahunan
 * @property \Carbon\Carbon $tanggal_approve
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarTemplate009 extends Eloquent
{
	protected $table = '_surat_keluar_template_009';
	public $incrementing = false;

	protected $casts = [
		'nomor_tahunan' => 'int'
	];

	protected $dates = [
		'tanggal_surat',
		'tanggal_approve'
	];

	protected $fillable = [
		'surat_keluar_id',
		'surat_keluar_template_id',
		'nomor_surat',
		'keamanan',
		'kode_arsip',
		'kota_persuratan',
		'menimbang',
		'dasar',
		'kepada',
		'daftar_kepada',
		'untuk',
		'tanggal_surat',
		'penandatangan',
		'nomor_tahunan',
		'tanggal_approve'
	];
}

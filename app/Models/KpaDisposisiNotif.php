<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpaDisposisiNotif
 * 
 * @property int $id
 * @property string $disposisi_id
 * @property string $url
 * @property string $nama
 * @property string $status
 * @property string $kategori
 * @property \Carbon\Carbon $tanggal_approve
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class KpaDisposisiNotif extends Eloquent
{
	protected $table = '_kpa_disposisi_notif';

	protected $dates = [
		'tanggal_approve'
	];

	protected $fillable = [
		'disposisi_id',
		'url',
		'nama',
		'status',
		'kategori',
		'tanggal_approve'
	];
}

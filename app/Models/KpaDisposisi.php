<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpaDisposisi
 *
 * @property string $id
 * @property string $perihal
 * @property string $status
 * @property string $created_by_username
 * @property string $created_by_nama
 * @property string $kepada_divisi_id
 * @property string $kepada_divisi_nama
 * @property string $status_progress
 * @property string $nomor_agenda
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class KpaDisposisi extends Eloquent
{
	protected $table = '_kpa_disposisi';
	public $incrementing = false;

	protected $fillable = [
		'perihal',
		'status',
		'created_by_username',
		'created_by_nama',
		'kepada_divisi_id',
		'kepada_divisi_nama',
		'status_progress',
		'nomor_agenda'
    ];


    public function byUsername() {
        return $this->belongsTo('App\user','created_by_username','username');
    }
}

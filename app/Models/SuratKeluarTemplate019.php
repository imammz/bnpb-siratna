<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarTemplate019
 * 
 * @property string $id
 * @property string $surat_keluar_id
 * @property string $surat_keluar_template_id
 * @property string $keamanan
 * @property string $kode_arsip
 * @property string $nomor_surat
 * @property string $nomor_tahun
 * @property string $tentang
 * @property string $isi
 * @property string $penandatanganan
 * @property \Carbon\Carbon $tanggal_surat
 * @property \Carbon\Carbon $tanggal_approve
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarTemplate019 extends Eloquent
{
	protected $table = '_surat_keluar_template_019';
	public $incrementing = false;

	protected $dates = [
		'tanggal_surat',
		'tanggal_approve'
	];

	protected $fillable = [
		'surat_keluar_id',
		'surat_keluar_template_id',
		'keamanan',
		'kode_arsip',
		'nomor_surat',
		'nomor_tahun',
		'tentang',
		'isi',
		'penandatanganan',
		'tanggal_surat',
		'tanggal_approve'
	];
}

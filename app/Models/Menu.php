<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Menu
 * 
 * @property int $id
 * @property string $nama_menu
 * @property string $id_parent_menu
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $roles
 *
 * @package App\Models
 */
class Menu extends Eloquent
{
	protected $table = 'menu';

	protected $fillable = [
		'nama_menu',
		'id_parent_menu'
	];

	public function roles()
	{
		return $this->belongsToMany(\App\Models\Role::class, 'menu_role', 'id_menu', 'id_role')
					->withTimestamps();
	}
}

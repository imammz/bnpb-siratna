<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpaDisposisiLampiran
 *
 * @property int $id
 * @property string $disposisi_id
 * @property string $nama_file
 * @property string $created_by_username
 * @property string $created_by_divisi_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class KpaDisposisiLampiran extends Eloquent
{
	protected $table = '_kpa_disposisi_lampiran';

	protected $fillable = [
		'disposisi_id',
		'nama_file',
		'created_by_username',
		'created_by_divisi_id'
    ];

    public function byUsername() {
        return $this->belongsTo('App\user','created_by_username','username');
    }
}

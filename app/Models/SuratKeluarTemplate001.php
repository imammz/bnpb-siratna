<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarTemplate001
 * 
 * @property string $id
 * @property string $surat_keluar_id
 * @property string $surat_keluar_template_id
 * @property string $nomor_surat
 * @property string $keamanan
 * @property string $kode_arsip
 * @property string $yth
 * @property string $dari
 * @property string $prihal
 * @property \Carbon\Carbon $tanggal_surat
 * @property string $isi
 * @property string $penandatangan
 * @property string $tembusan
 * @property int $nomor_tahunan
 * @property \Carbon\Carbon $tanggal_approve
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarTemplate001 extends Eloquent
{
	protected $table = '_surat_keluar_template_001';
	public $incrementing = false;

	protected $casts = [
		'nomor_tahunan' => 'int'
	];

	protected $dates = [
		'tanggal_surat',
		'tanggal_approve'
	];

	protected $fillable = [
		'surat_keluar_id',
		'surat_keluar_template_id',
		'nomor_surat',
		'keamanan',
		'kode_arsip',
		'yth',
		'dari',
		'prihal',
		'tanggal_surat',
		'isi',
		'penandatangan',
		'tembusan',
		'nomor_tahunan',
		'tanggal_approve'
	];


	public function suratkeluar() {
        return $this->belongsTo('App\models\SuratKeluar','surat_keluar_id','id');
    }






}

<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DisposisiTemplate001
 *
 * @property string $id
 * @property string $disposisi_id
 * @property string $disposisi_template_id
 * @property string $indeks
 * @property string $jenis_surat_masuk
 * @property string $rahasia
 * @property string $penting
 * @property string $biasa
 * @property \Carbon\Carbon $tanggal_penyelesaian
 * @property string $kode
 * @property \Carbon\Carbon $tanggal_surat_masuk
 * @property string $nomor_surat_masuk
 * @property string $asal
 * @property string $isi_ringkas
 * @property \Carbon\Carbon $tanggal_diterima
 * @property string $informasi
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class DisposisiTemplate001 extends Eloquent
{
	protected $table = 'disposisi_template_001';
	public $incrementing = false;

	protected $dates = [
		'tanggal_penyelesaian',
		'tanggal_surat_masuk',
		'tanggal_diterima'
	];

	protected $fillable = [
		'disposisi_id',
		'disposisi_template_id',
		'indeks',
		'jenis_surat_masuk',
		'rahasia',
		'penting',
		'biasa',
		'tanggal_penyelesaian',
		'kode',
		'tanggal_surat_masuk',
		'nomor_surat_masuk',
		'asal',
		'isi_ringkas',
		'tanggal_diterima',
		'informasi'
    ];

    public function disposisi() {
        return $this->belongsTo('App\Models\Disposisi','disposisi_id');
    }
}

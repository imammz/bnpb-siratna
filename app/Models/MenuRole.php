<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MenuRole
 * 
 * @property int $id_menu
 * @property int $id_role
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Menu $menu
 * @property \App\Models\Role $role
 *
 * @package App\Models
 */
class MenuRole extends Eloquent
{
	protected $table = 'menu_role';
	public $incrementing = false;

	protected $casts = [
		'id_menu' => 'int',
		'id_role' => 'int'
	];

	public function menu()
	{
		return $this->belongsTo(\App\Models\Menu::class, 'id_menu');
	}

	public function role()
	{
		return $this->belongsTo(\App\Models\Role::class, 'id_role');
	}
}

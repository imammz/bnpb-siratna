<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Agenda
 *
 * @property string $id
 * @property string $nama_kegiatan
 * @property string $tempat
 * @property \Carbon\Carbon $tanggal_mulai
 * @property \Carbon\Carbon $tanggal_akhir
 * @property string $keterangan
 * @property string $penanggung_jawab
 * @property string $status
 * @property string $catatan
 * @property string $created_by_username
 * @property string $created_by_nama
 * @property string $update_agenda
 * @property string $created_by_id_user
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Agenda extends Eloquent
{
	protected $table = '_agenda';
	public $incrementing = false;

	protected $dates = [
		'tanggal_mulai',
		'tanggal_akhir'
	];

	protected $fillable = [
		'nama_kegiatan',
		'tempat',
		'tanggal_mulai',
		'tanggal_akhir',
		'keterangan',
		'penanggung_jawab',
		'status',
		'catatan',
		'created_by_username',
		'created_by_nama',
		'update_agenda',
		'created_by_id_user'
    ];


    public function daftarUser() {
        return $this->hasMany('App\Models\AgendaUser','agenda_id','id');
    }

    public function detail() {
        return $this->hasMany('App\Models\AgendaDetail','agenda_id','id');
    }
}

<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarLog
 *
 * @property int $id
 * @property string $surat_keluar_id
 * @property string $data
 * @property string $referensi
 * @property string $created_by_username
 * @property string $created_by_nama
 * @property string $divisi_id_penerima
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarLog extends Eloquent
{
	protected $table = '_surat_keluar_log';

	protected $fillable = [
		'surat_keluar_id',
		'data',
		'referensi',
		'created_by_username',
		'created_by_nama',
		'divisi_id_penerima'
    ];

    public function byUsername() {
        return $this->belongsTo('App\user','created_by_username','username');
    }
}

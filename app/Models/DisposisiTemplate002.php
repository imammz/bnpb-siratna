<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DisposisiTemplate002
 *
 * @property string $id
 * @property string $disposisi_id
 * @property string $disposisi_template_id
 * @property string $klasifikasi
 * @property string $pengirim
 * @property string $nomor_surat_masuk
 * @property \Carbon\Carbon $tanggal_surat_masuk
 * @property \Carbon\Carbon $tanggal_diterima
 * @property int $nomor_agenda
 * @property string $retro
 * @property string $perihal
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class DisposisiTemplate002 extends Eloquent
{
	protected $table = 'disposisi_template_002';
	public $incrementing = false;

	protected $casts = [
		'nomor_agenda' => 'int'
	];

	protected $dates = [
		'tanggal_surat_masuk',
		'tanggal_diterima'
	];

	protected $fillable = [
		'disposisi_id',
		'disposisi_template_id',
		'klasifikasi',
		'pengirim',
		'nomor_surat_masuk',
		'tanggal_surat_masuk',
		'tanggal_diterima',
		'nomor_agenda',
		'retro',
		'perihal'
    ];

    public function disposisi() {
        return $this->belongsTo('App\Models\Disposisi','disposisi_id');
    }
}

<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarTemplate015
 * 
 * @property string $id
 * @property string $surat_keluar_id
 * @property string $surat_keluar_template_id
 * @property string $nomor_surat
 * @property string $keamanan
 * @property string $kode_arsip
 * @property string $sifat
 * @property string $prihal
 * @property string $nama_jabatan
 * @property string $peraturan
 * @property string $tentang
 * @property string $menimbang
 * @property string $mengingat
 * @property string $menetapkan
 * @property string $pasal
 * @property string $ditetapkan_di
 * @property \Carbon\Carbon $tanggal_penetapan
 * @property string $penandatangan
 * @property string $mentri
 * @property string $nama_mentri
 * @property string $isi_lampiran
 * @property int $nomor_tahunan
 * @property \Carbon\Carbon $tanggal_approve
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarTemplate015 extends Eloquent
{
	protected $table = '_surat_keluar_template_015';
	public $incrementing = false;

	protected $casts = [
		'nomor_tahunan' => 'int'
	];

	protected $dates = [
		'tanggal_penetapan',
		'tanggal_approve'
	];

	protected $fillable = [
		'id',
		'surat_keluar_id',
		'surat_keluar_template_id',
		'nomor_surat',
		'keamanan',
		'kode_arsip',
		'sifat',
		'prihal',
		'nama_jabatan',
		'peraturan',
		'tentang',
		'menimbang',
		'mengingat',
		'menetapkan',
		'pasal',
		'ditetapkan_di',
		'tanggal_penetapan',
		'penandatangan',
		'mentri',
		'nama_mentri',
		'isi_lampiran',
		'nomor_tahunan',
		'tanggal_approve'
	];
}

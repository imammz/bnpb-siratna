<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarTemplate003
 * 
 * @property string $id
 * @property string $surat_keluar_id
 * @property string $surat_keluar_template_id
 * @property string $nomor_surat
 * @property string $keamanan
 * @property string $kode_arsip
 * @property string $sifat
 * @property string $prihal
 * @property string $kota_persuratan
 * @property int $lampiran
 * @property string $satuan_lampiran
 * @property string $yth
 * @property string $pembuka
 * @property string $nama_acara
 * @property \Carbon\Carbon $tgl_mulai_acara
 * @property \Carbon\Carbon $tgl_akhir_acara
 * @property string $tempat
 * @property string $penutup
 * @property string $penandatangan
 * @property string $tembusan
 * @property string $tamu_undangan
 * @property int $nomor_tahunan
 * @property \Carbon\Carbon $tanggal_approve
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarTemplate003 extends Eloquent
{
	protected $table = '_surat_keluar_template_003';
	public $incrementing = false;

	protected $casts = [
		'lampiran' => 'int',
		'nomor_tahunan' => 'int'
	];

	protected $dates = [
		'tgl_mulai_acara',
		'tgl_akhir_acara',
		'tanggal_approve'
	];

	protected $fillable = [
		'surat_keluar_id',
		'surat_keluar_template_id',
		'nomor_surat',
		'keamanan',
		'kode_arsip',
		'sifat',
		'prihal',
		'kota_persuratan',
		'lampiran',
		'satuan_lampiran',
		'yth',
		'pembuka',
		'nama_acara',
		'tgl_mulai_acara',
		'tgl_akhir_acara',
		'tempat',
		'penutup',
		'penandatangan',
		'tembusan',
		'tamu_undangan',
		'nomor_tahunan',
		'tanggal_approve'
	];
}

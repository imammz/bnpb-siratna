<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DisposisiCatatan
 *
 * @property int $id
 * @property string $nama
 * @property string $kategori
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class DisposisiCatatan extends Eloquent
{
	protected $table = 'disposisi_catatan';

	protected $fillable = [
		'nama',
		'kategori'
	];
}

<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Role
 * 
 * @property int $id
 * @property string $nama_role
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $menus
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Role extends Eloquent
{
	protected $table = 'role';

	protected $fillable = [
		'nama_role'
	];

	public function menus()
	{
		return $this->belongsToMany(\App\Models\Menu::class, 'menu_role', 'id_role', 'id_menu')
					->withTimestamps();
	}

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'id_role');
	}
}

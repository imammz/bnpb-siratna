<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarTemplate
 * 
 * @property string $id
 * @property string $code
 * @property string $nama
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarTemplate extends Eloquent
{
	protected $table = '_surat_keluar_template';
	public $incrementing = false;

	protected $fillable = [
		'code',
		'nama'
	];
}

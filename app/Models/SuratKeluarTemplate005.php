<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarTemplate005
 * 
 * @property string $id
 * @property string $surat_keluar_id
 * @property string $surat_keluar_template_id
 * @property string $judul
 * @property string $isi
 * @property string $ttd_jabatan
 * @property string $ttd_nama
 * @property int $nomor_tahunan
 * @property string $kode_jabatan
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarTemplate005 extends Eloquent
{
	protected $table = '_surat_keluar_template_005';
	public $incrementing = false;

	protected $casts = [
		'nomor_tahunan' => 'int'
	];

	protected $fillable = [
		'surat_keluar_id',
		'surat_keluar_template_id',
		'judul',
		'isi',
		'ttd_jabatan',
		'ttd_nama',
		'nomor_tahunan',
		'kode_jabatan'
	];
}

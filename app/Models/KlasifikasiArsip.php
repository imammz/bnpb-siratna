<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KlasifikasiArsip
 * 
 * @property int $id
 * @property string $code
 * @property string $nama
 * @property int $level
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class KlasifikasiArsip extends Eloquent
{
	protected $table = '_klasifikasi_arsip';

	protected $casts = [
		'level' => 'int'
	];

	protected $fillable = [
		'code',
		'nama',
		'level'
	];
}

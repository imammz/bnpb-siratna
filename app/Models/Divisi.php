<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Divisi
 * 
 * @property string $id
 * @property string $code
 * @property string $nama
 * @property string $singkatan
 * @property int $eselon
 * @property string $tipe
 * @property string $referal
 * @property int $divisi_left
 * @property int $divisi_right
 * @property string $data
 * @property string $status
 * @property int $hak_akses_id
 * @property string $kategori
 * @property string $head_user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Divisi extends Eloquent
{
	protected $table = '_divisi';
	public $incrementing = false;

	protected $casts = [
		'eselon' => 'int',
		'divisi_left' => 'int',
		'divisi_right' => 'int',
		'hak_akses_id' => 'int'
	];

	protected $fillable = [
		'code',
		'nama',
		'singkatan',
		'eselon',
		'tipe',
		'referal',
		'divisi_left',
		'divisi_right',
		'data',
		'status',
		'hak_akses_id',
		'kategori',
		'head_user_id'
	];
}

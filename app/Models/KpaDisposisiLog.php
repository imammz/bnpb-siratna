<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpaDisposisiLog
 *
 * @property int $id
 * @property string $disposisi_id
 * @property string $data
 * @property string $referensi
 * @property string $created_by_username
 * @property string $created_by_nama
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class KpaDisposisiLog extends Eloquent
{
	protected $table = '_kpa_disposisi_log';

	protected $fillable = [
		'disposisi_id',
		'data',
		'referensi',
		'created_by_username',
		'created_by_nama'
    ];

    public function byUsername() {
        return $this->belongsTo('App\user','created_by_username','username');
    }
}

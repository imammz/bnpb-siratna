<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuratKeluarTemplate007
 * 
 * @property string $id
 * @property string $surat_keluar_id
 * @property string $surat_keluar_template_id
 * @property string $nomor_surat
 * @property string $keamanan
 * @property string $kode_arsip
 * @property string $sifat
 * @property string $prihal
 * @property string $isi
 * @property string $dibuat_di
 * @property string $nama_pihak_pertama
 * @property string $nama_pihak_kedua
 * @property string $penandatangan
 * @property int $nomor_tahunan
 * @property \Carbon\Carbon $tanggal_approve
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SuratKeluarTemplate007 extends Eloquent
{
	protected $table = '_surat_keluar_template_007';
	public $incrementing = false;

	protected $casts = [
		'nomor_tahunan' => 'int'
	];

	protected $dates = [
		'tanggal_approve'
	];

	protected $fillable = [
		'surat_keluar_id',
		'surat_keluar_template_id',
		'nomor_surat',
		'keamanan',
		'kode_arsip',
		'sifat',
		'prihal',
		'isi',
		'dibuat_di',
		'nama_pihak_pertama',
		'nama_pihak_kedua',
		'penandatangan',
		'nomor_tahunan',
		'tanggal_approve'
	];
}

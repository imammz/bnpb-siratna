<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgendaUser extends Model
{
    protected $table = '_agenda_user';
	public $incrementing = true;

	protected $fillable = [
		'agenda_id',
		'id_user',
		'created_by_username'
    ];


    public function user() {
        return $this->belongsTo('App\user','id_user','id');
    }

    public function byUsername() {
        return $this->belongsTo('App\user','created_by_username','username');
    }

    public function agenda() {
        return $this->belongsTo('App\Models\Agenda','agenda_id','id')->orderBy('tanggal_mulai','desc');
    }

}

<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ArsipDetail
 * 
 * @property int $id
 * @property string $arsip_id
 * @property string $nama_file
 * @property string $created_by_username
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class ArsipDetail extends Eloquent
{
	protected $table = '_arsip_detail';

	protected $fillable = [
		'arsip_id',
		'nama_file',
		'created_by_username'
	];
}

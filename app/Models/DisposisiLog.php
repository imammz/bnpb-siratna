<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 11 Nov 2018 16:04:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DisposisiLog
 *
 * @property int $id
 * @property string $disposisi_id
 * @property string $data
 * @property string $referensi
 * @property string $created_by_username
 * @property string $created_by_nama
 * @property string $tipe
 * @property string $id_user
 * @property int $id_struktur_organisasi
 * @property int $id_unit_kerja
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class DisposisiLog extends Eloquent
{
	protected $table = 'disposisi_log';

	protected $casts = [
		'id_struktur_organisasi' => 'int',
		'id_unit_kerja' => 'int'
	];

	protected $fillable = [
		'disposisi_id',
		'data',
		'referensi',
		'created_by_username',
		'created_by_nama',
		'tipe',
		'dari_id_user',
        'dari_id_unit_kerja',
        'dari_nama_unit_kerja',
    ];

    public function disposisi() {
        return $this->belongsTo('App\Models\Disposisi','disposisi_id','id');
    }
    public function byUsername() {
        return $this->belongsTo('App\user','created_by_username','username');
    }
}
